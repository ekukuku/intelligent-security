package com.campfire.intelligentsecurity.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.bean.camera.AuthStruct;
import com.github.drinkjava2.jdialects.TableModelUtils;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Order(2)
public class AppConfig {
    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    @Autowired
    Environment environment;

    @Bean
    public AuthStruct struct() {
        AuthStruct authStruct = new AuthStruct();
        authStruct.setAk(environment.getProperty("local.camera.ak"));
        authStruct.setSk(environment.getProperty("local.camera.sk"));
        authStruct.setUrl(environment.getProperty("local.camera.host"));
        authStruct.setTokenurl(StrUtil.format("{}{}", authStruct.getUrl(), environment.getProperty("local.camera.tokenpath")));
        authStruct.setControlurl(StrUtil.format("{}{}", authStruct.getUrl(), environment.getProperty("local.camera.controlpath")));
        authStruct.setPlayurl(StrUtil.format("{}{}", authStruct.getUrl(), environment.getProperty("local.camera.playpath")));
        authStruct.setStatusurl(StrUtil.format("{}{}", authStruct.getUrl(), environment.getProperty("local.camera.statuspath")));
        authStruct.setSnapurl(StrUtil.format("{}{}", authStruct.getUrl(), environment.getProperty("local.camera.snapurl")));
        LOG.info("云台控制配置加载完成:\n{}", JSONUtil.toJsonPrettyStr(authStruct));
        return authStruct;
    }

    @Bean
    public void CreateDDL() {

        Map<String, Object> setting = new HashMap<String, Object>();
        setting.put(TableModelUtils.OPT_EXCLUDE_TABLES, Arrays.asList("Dbsample")); // 排除个别表名
        setting.put(TableModelUtils.OPT_PACKAGE_NAME, "com.campfire.intelligentsecurity.bean");// 包名
        setting.put(TableModelUtils.OPT_IMPORTS, "import java.sql.Timestamp;\n"); // 追加新的imports
        setting.put(TableModelUtils.OPT_REMOVE_DEFAULT_IMPORTS, true); // 是否去除自带的imports
        setting.put(TableModelUtils.OPT_CLASS_ANNOTATION, true);
        setting.put(TableModelUtils.OPT_CLASS_DEFINITION, "public class $Class extends ActiveRecord<$Class> {");// 类定义模板
        setting.put(TableModelUtils.OPT_FIELD_FLAGS, true); // 全局静态属性字段标记
        setting.put(TableModelUtils.OPT_FIELD_FLAGS_STATIC, true); // 全局静态属性字段标记
        setting.put(TableModelUtils.OPT_FIELD_FLAGS_STYLE, "upper"); // 全局静态属性字段标记可以有upper,lower,normal,camel几种格式
        setting.put(TableModelUtils.OPT_FIELDS, true); // 是否生成JavaBean属性
        setting.put(TableModelUtils.OPT_GETTER_SETTERS, true); // 是否生成getter setter
        setting.put(TableModelUtils.OPT_PUBLIC_FIELD, false); // JavaBean属性是否定义成public
        setting.put(TableModelUtils.OPT_LINK_STYLE, true); // getter/setter是否生成为链式风格
        //TableModelUtils.db2JavaSrcFiles(dbContext.getDataSource(), dbContext.getDialect(), "c:/temp", setting);
    }
}
