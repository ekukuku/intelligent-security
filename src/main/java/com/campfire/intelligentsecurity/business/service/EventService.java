package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.campfire.intelligentsecurity.business.bean.*;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import com.github.drinkjava2.jsqlbox.sqlitem.SampleItem;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jdbpro.SqlOption.IGNORE_NULL;
import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class EventService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object menuList() {
        return builder("select a.id, a.eventName, b.typeName, a.icon, a.algorithmId, a.remark, a.enable, a.createTime,a.algorithm,a.algorithmKey,a.status " +
                "from t_sys_event a left join t_sys_event_type b on a.eventType = b.id " +
                "where 1=1 "," order by a.status desc,a.algorithmId ");
    }

    public Object eventlist() {
        return builder("select id,algorithmKey,algorithmMsg,eventName,status,enable from t_sys_event_name where 1=1 "," order by algorithmKey");
    }

    public Object saveOrUpdateEvent(TSysEventName eventName) {
        if (eventName.getId() != null) {
            String sfmc=dbContext.qryString("select algorithmMsg from t_sys_event_name where id=?",par(eventName.getId()));
            eventName.update();
            dbContext.exe(" update bus_operationplan_algorithm set algorithm_id = ? where algorithm_id =?",par(eventName.getAlgorithmMsg(),sfmc));
        } else {
            eventName.insert();
        }
        return true;
    }

    public Object saveOrUpdate(TSysEvent sysEvent) {
        if (sysEvent.getId() != null) {
            String sf=dbContext.qryString("select algorithm from t_sys_event where id=? ",par(sysEvent.getId()));
            sysEvent.update();
            dbContext.exe("update t_sys_event_name set algorithmKey=? where algorithmKey=?",par(sysEvent.getAlgorithm(),sf));
        } else {
            sysEvent.insert();
        }
        return true;
    }

    public Object loadInfoEvent(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select id,algorithmKey,algorithmMsg,eventName,status,enable from t_sys_event_name where id=?", par(id));
        } else {
            return null;
        }
    }

    public Object loadInfo(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select id, eventName, eventType, icon, algorithmId, remark, enable,algorithm,algorithmKey,status from t_sys_event where id=?", par(id));
        } else {
            return null;
        }
    }

    public List loadEventType() {
        return dbContext.qryMapList("select id, typeName from t_sys_event_type order by id");
    }

    public List keyList() {
        return dbContext.qryMapList("select algorithm id, eventName name from t_sys_event order by id");
    }

    public Object remove(String ids) {
        return dbContext.exe("delete from t_sys_event where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object removeEvent(String ids) {
        return dbContext.exe("delete from t_sys_event_name where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object chengeState(String id, String state) {
        return dbContext.exe("update t_sys_event set enable = ? where id = ?", par(state, id));
    }

    public Object changeStatus(String id, String state) {
        return dbContext.exe("update t_sys_event set status = ? where id = ?", par(state, id));
    }

    public Object changeStateEvent(String id, String state) {
        return dbContext.exe("update t_sys_event_name set enable = ? where id = ?", par(state, id));
    }

    public Object changeStatusEvent(String id, String state) {
        return dbContext.exe("update t_sys_event_name set status = ? where id = ?", par(state, id));
    }


    public Object loadEvent() {
        List<BusEvent> list = dbContext.qryEntityList(BusEvent.class, "select id,typeName from t_sys_event_type where enable=1 order by id");
        List<EventList> eventList = null;
        for (BusEvent event : list) {
            eventList = dbContext.qryEntityList(EventList.class, "select t1.algorithmMsg algorithm,t1.eventName eventName,t1.enable enable from t_sys_event_name t1\n" +
                    "left join t_sys_event t2 on t2.algorithm=t1.algorithmKey\n" +
                    "where t2.enable=1 and t2.eventType=? and t1.status=1 order by  t2.algorithm ", par(event.getId()));
            event.setList(eventList);
        }
        System.out.println(JSONObject.toJSONString(list));
        return list;
    }

    public Object loadCameraAlgorithm(String cameraNum) {
        List<BusEvent> list = dbContext.qryEntityList(BusEvent.class, "select id,typeName from t_sys_event_type where enable=1 order by id");
        List<String> alist = dbContext.qryList("select algorithm from camera_algorithm where camera_num=?", par(cameraNum));
        List<EventList> eventList = null;
        for (BusEvent event : list) {
            eventList = dbContext.qryEntityList(EventList.class, "select t1.algorithmMsg algorithm,t1.eventName eventName,t1.enable enable from t_sys_event_name t1\n" +
                    "left join t_sys_event t2 on t2.algorithm=t1.algorithmKey\n" +
                    "where t2.enable=1 and t2.eventType=? and t1.status=1 order by  t2.algorithm ", par(event.getId()));
            if (alist!=null&&alist.size()>0){
                for (EventList ee:eventList){
                    if (alist.contains(ee.getAlgorithm())){
                        ee.setEnable("1");
                    }else{
                        ee.setEnable("0");
                    }
                }
            }
            event.setList(eventList);
        }
        return list;
    }

    public Object loadPalnAlgorithm(String planid) {
        List<BusEvent> list = dbContext.qryEntityList(BusEvent.class, "select id,typeName from t_sys_event_type where enable=1 order by id");
        List<String> alist = dbContext.qryList("select algorithm_id from bus_operationplan_algorithm where operation_id=?", par(planid));
        List<EventList> eventList = null;
        for (BusEvent event : list) {
            eventList = dbContext.qryEntityList(EventList.class, "select t1.algorithmMsg algorithm,t1.eventName eventName,t1.enable enable from t_sys_event_name t1\n" +
                    "left join t_sys_event t2 on t2.algorithm=t1.algorithmKey\n" +
                    "where t2.enable=1 and t2.eventType=? and t1.status=1 order by  t2.algorithm ", par(event.getId()));
            if (alist!=null&&alist.size()>0){
                for (EventList ee:eventList){
                    if (alist.contains(ee.getAlgorithm())){
                        ee.setEnable("1");
                    }else{
                        ee.setEnable("0");
                    }
                }
            }
            event.setList(eventList);
        }
        return list;
    }

    public Object loadEventName(String id) {
        return dbContext.qryString("select ifnull(eventName,'未知违章类型') from t_sys_event_name where algorithmMsg=?",par(id));
    }

}
