package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusUnit;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class BusUnitService extends BaseService {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    @Autowired
    HttpSession session;

    public Object List() {
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT");
        sb.append(" bs.id, ");
        sb.append(" bs.create_by AS createBy, ");
        sb.append(" bs.create_time AS createTime, ");
        sb.append(" bs.update_by AS updateBy, ");
        sb.append(" bs.update_time AS updateTime, ");
        sb.append(" bs.del_flag AS delFlag, ");
        sb.append(" bs.remark, ");
        sb.append(" bs.enterprise_name AS enterpriseName, ");
        sb.append(" bs.credit_code AS creditCode, ");
        sb.append(" bs.legal_name AS legalName, ");
        sb.append(" bs.legal_phone AS legalPhone, ");
        sb.append(" bs.access_status AS accessStatus, ");
        sb.append(" bs.acceptance_unit AS acceptanceUnit, ");
        sb.append(" bs.safety_points AS safetyPoints, ");
        sb.append(" bs.negative_state AS negativeState, ");
        sb.append(" bs.black_list AS blackList,");
        sb.append(" ifnull(c1.control_value, 1) black_flag,");
        sb.append(" ifnull(c2.control_value, 1) nelist_flag ");
        sb.append(" FROM");
        sb.append(" bus_unit bs ");
        sb.append(" left join t_control_record c1 " +
                "                   on bs.id = c1.relation_id and c1.relation_type = 1 and c1.control_type = 0 and c1.latest = 1 " +
                "         left join t_control_record c2 " +
                "                   on bs.id = c2.relation_id and c2.relation_type = 1 and c2.control_type = 1 and c2.latest = 1 ");

        sb.append("where 1=1 ");
        return builder(sb.toString());
    }

    public Object saveOrUpdate(BusUnit busUnit) {
        if (StrUtil.isNotEmpty(busUnit.getId())) {
            busUnit.setUpdateBy(session.getAttribute("usercode").toString());
            busUnit.update();
        } else {
            busUnit.setId(getUUID());
            busUnit.setDelFlag("0");
            busUnit.setCreateBy(session.getAttribute("usercode").toString());
            busUnit.insert();
        }
        return true;
    }

    public Object remove(String id) {
        return dbContext.exe("delete from bus_unit where id in('" + id.replaceAll(",", "','") + "')");
    }

    public Object load(String id) {
        if (StrUtil.isNotEmpty(id)) {
            StringBuffer sb = new StringBuffer();
            sb.append(" SELECT");
            sb.append(" id, ");
            sb.append(" create_by AS createBy, ");
            sb.append(" create_time AS createTime, ");
            sb.append(" update_by AS updateBy, ");
            sb.append(" update_time AS updateTime, ");
            sb.append(" del_flag AS delFlag, ");
            sb.append(" remark, ");
            sb.append(" enterprise_name AS enterpriseName, ");
            sb.append(" credit_code AS creditCode, ");
            sb.append(" legal_name AS legalName, ");
            sb.append(" legal_phone AS legalPhone, ");
            sb.append(" access_status AS accessStatus, ");
            sb.append(" acceptance_unit AS acceptanceUnit, ");
            sb.append(" safety_points AS safetyPoints, ");
            sb.append(" negative_state AS negativeState, ");
            sb.append(" black_list AS blackList");
            sb.append(" FROM");
            sb.append(" bus_unit where 1=1 and id = ? ");
            return dbContext.qryMap(sb.toString(), par(id));
        } else {
            return null;
        }
    }
}
