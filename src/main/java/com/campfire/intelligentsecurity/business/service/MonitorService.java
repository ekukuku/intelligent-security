package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.campfire.intelligentsecurity.business.bean.camera.AuthStruct;
import com.campfire.intelligentsecurity.sys.bean.TreeNode;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class MonitorService extends BaseService {


    @Autowired
    CameraControlService cameraControlService;

//    public Object tree() {
//        List<TreeNode> list = dbContext.qryEntityList(TreeNode.class, "select  b.LABEL title,b.VALUE id,'9999' parentId,'true' disabled,'true' spread from  t_sys_dict b " +
//                "where b.TYPE = 'camera_type'");
//        for (TreeNode treeNode : list
//        ) {
//            treeNode.setChildren(dbContext.qryMapList("select '" + treeNode.getId() + "' parentId,a.camera_name title, a.camera_num id,a.stream from bus_camera " +
//                    "a where camera_type=?", par(treeNode.getId())));
//        }
//        return list;
//    }

    public Object tree() {
        List<TreeNode> pList=dbContext.qryEntityList(TreeNode.class,"SELECT s1.remark title,(@i:=@i+1) AS id,'true' spread,'true' disabled from (select DISTINCT remark from bus_camera) AS s1,(SELECT @i:=0) AS itable");
        for (TreeNode pnode:pList){
            List<TreeNode> list = dbContext.qryEntityList(TreeNode.class, "SELECT s1.yyly id,t.LABEL title,'false' spread,'true' disabled,'"+pnode.getId()+"' parentId from (SELECT DISTINCT yyly from bus_camera where remark=?) as s1\n" +
                    "left JOIN t_sys_dict t on t.`VALUE`=s1.yyly where t.TYPE = 'camera_yyly'",par(pnode.getTitle()));
            for (TreeNode treeNode : list
            ) {
                List<TreeNode> list2 = dbContext.qryEntityList(TreeNode.class, "select distinct substation title,substation id,'" + treeNode.getId() + "' parentId,'true' disabled,'false' spread from  bus_camera " +
                        "where yyly = ? and remark=?", par(treeNode.getId(),pnode.getTitle()));
                for (TreeNode treeNode2 : list2) {
                    treeNode2.setChildren(dbContext.qryMapList("select '" + treeNode2.getId() + "' parentId,a.camera_name title, a.id id,a.camera_num,a.stream from bus_camera " +
                            "a where substation=? and yyly=? and remark=?", par(treeNode2.getId()),par(treeNode.getId()),par(pnode.getTitle())));
                }
                treeNode.setChildren(list2);
            }
            pnode.setChildren(list);
        }
        return pList;
    }


    public Object loadStreamUrl(String camera_num) {
        String stream = dbContext.qryString("select stream from bus_camera where camera_num = ?", par(camera_num));
        if (StrUtil.isBlank(stream)) {
            stream = cameraControlService.play(camera_num);
        }
        return stream;
    }
}
