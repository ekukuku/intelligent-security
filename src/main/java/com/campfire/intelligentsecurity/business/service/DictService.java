package com.campfire.intelligentsecurity.business.service;

import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.github.drinkjava2.jsqlbox.DB.notNull;
import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class DictService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    /**
     * 数据字典工作性质
     * @return
     * dict_gzxz 工作性质
     * dict_dydj 电压等级
     * dict_zyfxdj 作业风险等级
     * dict_dwfxdj 电网风险等级
     * dict_sf 是否
     * dict_sex 性别
     * dict_zylb 专业类别
     */
    public Object loadCodeBook(String type) {
        return dbContext.qryMapList("select value as id,label as name from t_sys_dict where type=? and del_flag='0' order by sort",par(type));
    }

    /**
     * 作业人员
     * @return
     */
    public Object loadZyry(String unit) {
        return dbContext.qryMapList("select id as value,pname name from bus_person where DEL_FLAG=0 and unit in ('" + unit.replaceAll(",", "','") + "')");
    }

    /**
     * 作业设备
     * @return
     */
    public Object loadCamera() {
        return dbContext.qryMapList("select camera_num value,camera_name name from bus_camera where del_flag=0 ");
    }

    public Object loadSf() {
        return dbContext.qryMapList("select id as value,name from violation_algorithm where DEL_FLAG=0");
    }

    /**
     * 作业单位
     * @return
     */
    public Object loadDw() {
        return dbContext.qryMapList("select id,enterprise_name as name from bus_unit where del_flag='0'");
    }
    /**
     * 作业单位
     * @return
     */
    public Object loadDw2() {
        return dbContext.qryMapList("select id as value,enterprise_name as name from bus_unit where del_flag='0'");
    }
    public Object loadCodeBook2(String type) {
        return dbContext.qryMapList("select value,label as name from t_sys_dict where type=? and del_flag='0' order by sort",par(type));
    }
}
