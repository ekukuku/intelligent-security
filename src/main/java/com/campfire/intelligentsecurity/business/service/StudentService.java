package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.TTestStudent;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class StudentService extends BaseService {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object studentList() {
        return builder("select * from t_test_student where 1=1 ");
    }

    public Object saveOrUpdate(TTestStudent student) {
        if (student.getId() != null) {
            student.update();
        } else {
            student.insert();
        }
        return true;
    }

    public Object remove(String id) {
        return dbContext.exe("delete from t_test_student where id in('" + id.replaceAll(",", "','") + "')");
    }

    public Object load(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select * from t_test_student where id=?", par(id));
        } else {
            return null;
        }
    }
}
