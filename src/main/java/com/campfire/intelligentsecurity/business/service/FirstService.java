package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusCamera;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.github.drinkjava2.jsqlbox.DB.noBlank;
import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class FirstService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object planStatistics() {
        return dbContext.qryMap ("select count(1) total_num, count(if(STATUS='1',1,null)) cur_num, " +
                "count(if(OPERATIONAL_RISK_LEVEL>0,1,null)) risk_all, count(if(OPERATIONAL_RISK_LEVEL=1,1,null)) risk_1, " +
                "count(if(OPERATIONAL_RISK_LEVEL=2,1,null)) risk_2, count(if(OPERATIONAL_RISK_LEVEL=3,1,null)) risk_3, " +
                "count(if(OPERATIONAL_RISK_LEVEL=4,1,null)) risk_4, count(if(OPERATIONAL_RISK_LEVEL=5,1,null)) risk_5 " +
                "from bus_operationplan");
    }

    public Object personStatistics(){
        return dbContext.qryMap ("select count(1) total_person, count(if(major_type=1,1,null)) person_type_1, " +
                "count(if(major_type=2,1,null)) person_type_2, count(if(major_type=3,1,null)) person_type_3 " +
                "from bus_person");
    }

    public Object loadAdmittanceInfo(){
        return dbContext.qryMap ("select count(1) admittance_all, " +
                "count(if(a.STATUS='1' and a.START_TIME<NOW() and a.END_TIME>NOW(),1,null)) admittance_today " +
                "from bus_operationplan a " +
                "left join bus_operationplan_person b on a.ID = operation_id");
    }

    public Object loadJqwzInfo(){
        return dbContext.qryMapList("select DATE_FORMAT(a.createTime, '%m-%d') createTime, ifnull(b.eventName,'未知违章类型') as eventName " +
                " from t_event_record a" +
                " left join t_sys_event_name b on a.algorithmId = b.algorithmMsg " +
                "where state = 1 and date_sub(curdate(), interval 1 month) <= a.createTime order by a.createTime desc");
    }

    public Object loadTeamInfo(){
        return dbContext.qryMap ("select count(1) team_num_all, count(if(access_status=0,1,null)) team_num_cur from bus_unit");
    }

    public Object loadTeamJobInfo(){
        return dbContext.qryMap("select count(1) job_num_all, count(distinct CONSTRUCTION_UNIT) job_num_team, " +
                "count(if(OPERATIONAL_RISK_LEVEL>=3,1,null)) job_num_risk3 " +
                "from bus_operationplan " +
                "where STATUS='1' and START_TIME<NOW() and END_TIME>NOW()");
    }

    public Object loadBlackListInfo(){
        return dbContext.qryMap("select count(if(relation_type=0 and control_type=0 and control_value=0,1,null)) bl_p_all, " +
                "count(if(relation_type=0 and control_type=0 and control_value=0 and latest=1,1,null)) bl_p_cur," +
                "count(if(relation_type=0 and control_type=1 and control_value=0,1,null)) fm_p_all, " +
                "count(if(relation_type=0 and control_type=1 and control_value=0 and latest=1,1,null)) fm_p_cur, " +
                "count(if(relation_type=1 and control_type=0 and control_value=0,1,null)) bl_u_all, " +
                "count(if(relation_type=1 and control_type=0 and control_value=0 and latest=1,1,null)) bl_u_cur, " +
                "count(if(relation_type=1 and control_type=1 and control_value=0,1,null)) fm_u_all, " +
                "count(if(relation_type=1 and control_type=1 and control_value=0 and latest=1,1,null)) fm_u_cur " +
                "from t_control_record");
    }

    public Object loadCamInfo(){
        return dbContext.qryMap("select count(if(camera_type=1,1,null)) cam_num_bkq, count(if(camera_type=2,1,null)) cam_num_gd from bus_camera where del_flag = 0");
    }

    public Object loadWzInfo(){
        return dbContext.qryMap("select count(if(state=0,1,null)) wz_dqr, count(if(state=1,1,null)) wz_yqr, count(if(state=2,1,null)) wz_yxj from t_event_record where createTime >= curdate()");
    }
}
