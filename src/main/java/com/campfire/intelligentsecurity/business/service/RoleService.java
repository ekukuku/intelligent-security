package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.TSysRole;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class RoleService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object roleList() {
        return builder("SELECT id, roleName, roleCode, enable, details, createTime FROM t_sys_role where 1=1 ");
    }

    public Object loadInfo(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select id, roleName, roleCode, enable, details, createTime from t_sys_role where id=?", par(id));
        } else {
            return null;
        }
    }

    public Object menuList(){
        /*
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        List<TSysMenu> menuList = dbContext.qryEntityList(TSysMenu.class, "select id, title, parent from t_sys_menu where state = 1 order by sort,id");
        List<TSysMenu> menus = menuList.stream().filter(tSysMenu -> tSysMenu.getParent() == -1).collect(Collectors.toList());
        for (TSysMenu menu : menus) {
            menu.setChildren(menuList.stream().filter(tSysMenu -> tSysMenu.getParent().equals(menu.getId())).collect(Collectors.toList()));
        }
        status.put("code", 200);
        status.put("message", "操作成功");
        result.put("status", status);
        result.put("data", menus);
        return result;
         */
        Map<String, Object> result = new HashMap<String, Object>();
        List<TSysMenu> menuList = dbContext.qryEntityList(TSysMenu.class, "select * from t_sys_menu where 1 = 1 order by sort");
        result.put("code", 0);
        result.put("data", menuList);
        return result;
    }

    public Object saveOrUpdate(TSysRole sysRole) {
        if (sysRole.getId() != null) {
            sysRole.update();
        } else {
            sysRole.insert();
        }
        return true;
    }

    public Object remove(String ids) {
        return dbContext.exe("delete from t_sys_role where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object chengeState(String id, String state) {
        return dbContext.exe("update t_sys_role set enable = ? where id = ?", par(state, id));
    }

    @Transactional
    public Object changeUsers(String roleid, String users) {
        dbContext.exe("delete from t_role_user where roleid = ?", par(roleid));
        if(users!=null&&!"".equals(users)){
            String[] userids = users.split(",");
            if(userids!=null&&userids.length>0){
                for(String userid:userids){
                    dbContext.exe("insert into t_role_user(roleid, userid) values(?,?)", par(roleid, userid));
                }
            }
        }
        return "success";
    }

    public Object userList() {
        return builder("SELECT a.id, a.username, a.usercode, b.lastLogin " +
                "FROM t_sys_user a " +
                "left join ( " +
                "select max(createTime) lastLogin, userid from t_sys_login_log group by userid " +
                ") b on a.id = b.userid " +
                "where 1=1 ");
    }

    public List curUserList(String roleid) {
        return dbContext.qryMapList("select a.id, a.usercode, a.username from t_sys_user a left join t_role_user b on a.id = b.userid where b.roleid=? ", par(roleid));
    }

    public List curMenuList(String roleid) {
        return dbContext.qryMapList("select id, menuid from t_role_menu where roleid=? ", par(roleid));
    }

    @Transactional
    public Object changeMenu(String roleid, String menu) {
        dbContext.exe("delete from t_role_menu where roleid = ?", par(roleid));
        if(menu!=null&&!"".equals(menu)){
            String[] menuids = menu.split(",");
            if(menuids!=null&&menuids.length>0){
                for(String menuid:menuids){
                    dbContext.exe("insert into t_role_menu(roleid, menuid) values(?,?)", par(roleid, menuid));
                }
            }
        }
        return "success";
    }
}
