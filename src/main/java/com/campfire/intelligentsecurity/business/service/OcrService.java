package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.bean.TicketInfo;
import com.campfire.intelligentsecurity.sys.bean.ListPager;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.par;


@Service
public class OcrService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    @Autowired
    Environment environment;

    //    public Object ticketList() {
//        return builder("select id, jobContent, jobLocation, startTime, endTime, createTime, result, name, url,master,workers,operationplan from t_ticket_info where 1=1 ORDER BY createTime DESC");
//    }
    public Object ticketList() {
        ListPager builder = builder("select id, jobContent, jobLocation, startTime, endTime, createTime, result, name, url,master,workers,operationplan from t_ticket_info where 1=1 ORDER BY createTime DESC");
        for (Map<String, Object> datum : (List<Map<String, Object>>) builder.getData()) {
            Map<String, Object> stringObjectMap = judgeAccess(datum);
        }
        return builder;
    }

    public Object ticket(String id) {
        if (StrUtil.isNotEmpty(id)) {
//            return dbContext.qryMap("select id, jobContent, jobLocation, startTime, endTime, createTime, result, name, url,master,workers,operationplan from t_ticket_info where operationplan=? ORDER BY createTime DESC", par(id));
            Map<String, Object> map = dbContext.qryMap("select id, jobContent, jobLocation, startTime, endTime, createTime, result, name, url,master,workers,operationplan from t_ticket_info where operationplan=? ORDER BY createTime DESC", par(id));
            return judgeAccess(map);
        } else {
            return null;
        }
    }

    public Map<String, Object> getCharge(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select OPERATION_CHARGE_PERSON from bus_operationplan where ID=?", par(id));
        } else {
            return null;
        }
    }


    public Object saveOrUpdate(TicketInfo ticketInfo) {
        if (StrUtil.isNotEmpty(ticketInfo.getId())) {
            ticketInfo.update();
        } else {
            ticketInfo.insert();
        }

        return true;
    }

    public Object remove(String id) {
        return dbContext.exe("delete from t_ticket_info where id in('" + id.replaceAll(",", "','") + "')");
    }

    public JSONObject ocr(String pic) {
        try {
            String param = "{\"pic\":\"" + pic + "\"}";
            String result = HttpUtil.post(environment.getProperty("ocr.api"), param);
            JSONObject resultJbj = JSONUtil.parseObj(result);
            return resultJbj;
        } catch (Exception e) {
            LOG.info("OCR服务异常");
        }
        return null;
    }

    public Map<String, Object> judgeAccess(Map<String, Object> map) {
        String master = map.get("master").toString();
        if (StrUtil.isNotEmpty(master)) {
            Map<String, Object> access = dbContext.qryMap("select access_status from bus_person where pname=?", par(master));
            if (access.size() > 0) {
                String access_status = access.get("access_status").toString();
                if (!access_status.equals("0")) {
                    map.put("master", master + "*");
                }
            } else {
                map.put("master", master + "*");
            }
        }
        String workers = map.get("workers").toString();
        if (StrUtil.isNotEmpty(workers)) {
            StringBuilder w = new StringBuilder();
            for (String s : workers.split("、")) {
                Map<String, Object> access = dbContext.qryMap("select access_status from bus_person where pname=?", par(s));

                if (access.size() > 0) {
                    String access_status = access.get("access_status").toString();
                    if (!access_status.equals("0")) {
                        s += "*";
                    }
                } else {
                    s += "*";
                }
                w.append(s).append("、");
            }
            map.put("workers", w.substring(0, w.length() - 1));
        }
        return map;
    }

    public Map<String, Object> getPersonIdByName(String name) {
        if (StrUtil.isNotEmpty(name)) {
            return dbContext.qryMap("select id from bus_person where pname=?", par(name));
        } else {
            return null;
        }
    }
}
