package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONString;
import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.bean.BusPerson;
import com.campfire.intelligentsecurity.business.bean.TEventVideo;
import com.campfire.intelligentsecurity.business.bean.TPersonRecord;
import com.campfire.intelligentsecurity.business.bean.TpersonOffline;
import com.campfire.intelligentsecurity.business.bean.api.*;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.campfire.intelligentsecurity.sys.service.WebSocketServer;
import com.campfire.intelligentsecurity.sys.service.WzMsgPushServer;
import com.github.drinkjava2.jdialects.id.UUID32Generator;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.noBlank;
import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class ApiService extends BaseService {


    @Autowired
    Environment environment;

    @Autowired
    CameraControlService service;

    public Object api(ApiStruct apiStruct) {
        if (apiStruct != null) {
            dbContext.exe("insert into t_sys_log(type,ipAddr,content) values(?,?,?)", par(1, request.getRemoteHost(), apiStruct.getData()));
            switch (apiStruct.apiType()) {
                case RLSBXXJS:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.RLSBXXJS.getName());
                    RLSBXXJSStruct rlsbxxjsStruct = (RLSBXXJSStruct) apiStruct.getDataBean();
                    return rlsbxxHandle(rlsbxxjsStruct);
                case WZXXJS:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.WZXXJS.getName());
                    WZXXStruct wzxxStruct = (WZXXStruct) apiStruct.getDataBean();
                    return wzxxHandle(wzxxStruct);
                case RLXXJS:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.RLXXJS.getName());
                    RLXXStruct rlxxStruct = (RLXXStruct) apiStruct.getDataBean();
                    return rlxxHandle(rlxxStruct);
                case RYCRJS:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.RYCRJS.getName());
                    List<RYCRJSStruct> structDataList = apiStruct.getDataList();
                    return rycrHandle(structDataList);
                case SPSBWC:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.SPSBWC.getName());
                    JKWCStruct jkwcStruct = (JKWCStruct) apiStruct.getDataBean();
                    return jkwcHandle(jkwcStruct);
                case BAD:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.BAD.getName());
                    return null;
                case GETSTEAM:
                    LOG.info("收到远程调用消息：{}", ApiStruct.ApiType.GETSTEAM.getName());
                    GETSTEAMStruct steamStruct= (GETSTEAMStruct) apiStruct.getDataBean();
                    return steamHandle(steamStruct);
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    //违章消息处理
    public Object wzxxHandle(WZXXStruct wzxxStruct) {
        if (wzxxStruct != null) {
            if (StrUtil.isEmpty(wzxxStruct.getCAMERA_ID()) || StrUtil.isEmpty(wzxxStruct.getALGORITHM_ID())
                    || StrUtil.isEmpty(wzxxStruct.getVIOLATION_PIC()) || StrUtil.isEmpty(wzxxStruct.getVIOLATION_VIDEO()) || StrUtil.isEmpty(wzxxStruct.getVIOLATION_TIME())) {
                return "param error";
            }
            if (!isDate(wzxxStruct.getVIOLATION_TIME())) {
                return "time format error,please use yyyy-MM-dd HH:mm:ss";
            }
            // 根据监控设备id及违章时间获取工作计划id
            String planId = getPlanIdByCamera(wzxxStruct.getCAMERA_ID(), wzxxStruct.getVIOLATION_TIME());
            if (StrUtil.isEmpty(planId)) {
                planId="-1";
            }
            String eventid = "100";
            /*
            TODO 启用算法逻辑
            String eventid = dbContext.qryString("select b.id from bus_operationplan_algorithm a left join t_sys_event b on a.algorithm_id = b.algorithmId " +
                    "where operation_id = ? and algorithm_id = ?", par(planId, wzxxStruct.getALGORITHM_ID()));
            if (StrUtil.isEmpty(eventid)) {
                return "该算法未启用或不存在。";
            }
             */
           /* if ("头".equals(wzxxStruct.getALGORITHM_ID())||"人体".equals(wzxxStruct.getALGORITHM_ID())||"已戴安全帽".equals(wzxxStruct.getALGORITHM_ID())) {
                LOG.info("=======该算法未启用或不存在: "+wzxxStruct.getALGORITHM_ID());
                return "The algorithm not enable or does not exist";
            }*/
            if (wzxxStruct.getCAMERA_ID().contains("offline")){
                long cc=dbContext.qryLongValue("select count(1) from t_offline_info where FIND_IN_SET(?,sf)",par(wzxxStruct.getALGORITHM_ID()));
                if (cc>0){
                    String key=wzxxStruct.getCAMERA_ID();
                    int flag = key.indexOf("_");
                    String str1=key.substring(0, flag);
                    String offlineId=key.substring(str1.length()+1, key.length());
                    dbContext.exe("INSERT INTO t_event_offline(offline, eventid, algorithmId, " +
                            "imagePath, videoPath, state, createTime)" +
                            " VALUES (?, ?, ?, ?, ?, ?, ?)",par(offlineId,eventid,wzxxStruct.getALGORITHM_ID(),wzxxStruct.getVIOLATION_PIC(), wzxxStruct.getVIOLATION_VIDEO(),0,wzxxStruct.getVIOLATION_TIME()));
                    LOG.debug("=========离线视频违章事件入库成功！");
                    dbContext.exe("INSERT INTO t_event_record(planid, eventid, algorithmId, imagePath, videoPath, createTime) " +
                            "VALUES (?, ?, ?, ?, ?, ?)", par(planId, eventid, wzxxStruct.getALGORITHM_ID(), wzxxStruct.getVIOLATION_PIC(), wzxxStruct.getVIOLATION_VIDEO(), wzxxStruct.getVIOLATION_TIME()));
                }
            }else{
                if (!planId.equals("-1")){
                    long cc0=dbContext.qryLongValue("select count(1) from camera_algorithm where algorithm=? ",par(wzxxStruct.getALGORITHM_ID()));
                    if (cc0>0){
                        dbContext.exe("INSERT INTO t_event_record(planid, eventid, algorithmId, imagePath, videoPath, createTime) " +
                                "VALUES (?, ?, ?, ?, ?, ?)", par(planId, eventid, wzxxStruct.getALGORITHM_ID(), wzxxStruct.getVIOLATION_PIC(), wzxxStruct.getVIOLATION_VIDEO(), wzxxStruct.getVIOLATION_TIME()));
                        WebSocketServer.sendMessage("收到新的违章消息", "违章推送", 3, 3);
                        JSONObject json=new JSONObject();
                        json.put("type","wz");
                        json.put("id",wzxxStruct.getALGORITHM_ID());
                        json.put("imagePath",wzxxStruct.getVIOLATION_PIC());
                        json.put("videoPath",wzxxStruct.getVIOLATION_VIDEO());
                        if (StrUtil.isNotEmpty(wzxxStruct.getALGORITHM_ID())){
                            String eventName = dbContext.qryString("select eventName from t_sys_event where algorithmKey=? ", par(wzxxStruct.getALGORITHM_ID()));
                            json.put("eventName",eventName);
                        }else{
                            json.put("eventName","未知违章事件");
                        }
                        json.put("createTime",wzxxStruct.getVIOLATION_TIME());
                        if (!json.isEmpty()){
                            WzMsgPushServer.push(planId,json.toString());
                        }
                    }else{
                        long cc=dbContext.qryLongValue("select count(1) from bus_operationplan_algorithm where operation_id=? and algorithm_id=?",par(planId,wzxxStruct.getALGORITHM_ID()));
                        if (cc>0){
                            dbContext.exe("INSERT INTO t_event_record(planid, eventid, algorithmId, imagePath, videoPath, createTime) " +
                                    "VALUES (?, ?, ?, ?, ?, ?)", par(planId, eventid, wzxxStruct.getALGORITHM_ID(), wzxxStruct.getVIOLATION_PIC(), wzxxStruct.getVIOLATION_VIDEO(), wzxxStruct.getVIOLATION_TIME()));
                            WebSocketServer.sendMessage("收到新的违章消息", "违章推送", 3, 3);
                            JSONObject json=new JSONObject();
                            json.put("type","wz");
                            json.put("id",wzxxStruct.getALGORITHM_ID());
                            json.put("imagePath",wzxxStruct.getVIOLATION_PIC());
                            json.put("videoPath",wzxxStruct.getVIOLATION_VIDEO());
                            if (StrUtil.isNotEmpty(wzxxStruct.getALGORITHM_ID())){
                                String eventName = dbContext.qryString("select eventName from t_sys_event where algorithmKey=? ", par(wzxxStruct.getALGORITHM_ID()));
                                json.put("eventName",eventName);
                            }else{
                                json.put("eventName","未知违章事件");
                            }
                            json.put("createTime",wzxxStruct.getVIOLATION_TIME());
                            if (!json.isEmpty()){
                                WzMsgPushServer.push(planId,json.toString());
                            }
                        }
                    }
                }else{
                    dbContext.exe("INSERT INTO t_event_record(planid, eventid, algorithmId, imagePath, videoPath, createTime) " +
                            "VALUES (?, ?, ?, ?, ?, ?)", par(planId, eventid, wzxxStruct.getALGORITHM_ID(), wzxxStruct.getVIOLATION_PIC(), wzxxStruct.getVIOLATION_VIDEO(), wzxxStruct.getVIOLATION_TIME()));
                    WebSocketServer.sendMessage("收到新的违章消息", "违章推送", 3, 3);
                    JSONObject json=new JSONObject();
                    json.put("type","wz");
                    json.put("id",wzxxStruct.getALGORITHM_ID());
                    json.put("imagePath",wzxxStruct.getVIOLATION_PIC());
                    json.put("videoPath",wzxxStruct.getVIOLATION_VIDEO());
                    if (StrUtil.isNotEmpty(wzxxStruct.getALGORITHM_ID())){
                        String eventName = dbContext.qryString("select eventName from t_sys_event where algorithmKey=? ", par(wzxxStruct.getALGORITHM_ID()));
                        json.put("eventName",eventName);
                    }else{
                        json.put("eventName","未知违章事件");
                    }
                    json.put("createTime",wzxxStruct.getVIOLATION_TIME());
                    if (!json.isEmpty()){
                        WzMsgPushServer.push(planId,json.toString());
                    }
                }
            }
            return "success";
        }
        return "faild";
    }

    //人脸识别消息处理
    public Object rlsbxxHandle(RLSBXXJSStruct rlsbxxjsStruct) {
        if (rlsbxxjsStruct != null) {
            if (StrUtil.isEmpty(rlsbxxjsStruct.getCAMERA_ID()) || StrUtil.isEmpty(rlsbxxjsStruct.getUSER_PIC()) || StrUtil.isEmpty(rlsbxxjsStruct.getFIND_TIME())) {
                return "param error";
            }
            if (!isDate(rlsbxxjsStruct.getFIND_TIME())) {
                return "The algorithm not enable or does not exist";
            }
            if (rlsbxxjsStruct.getCAMERA_ID().contains("offline")){
                String key=rlsbxxjsStruct.getCAMERA_ID();
                int flag = key.indexOf("_");
                String str1=key.substring(0, flag);
                String offlineId=key.substring(str1.length()+1, key.length());
                TpersonOffline tpersonOffline=null;
                if (StrUtil.isEmpty(rlsbxxjsStruct.getFACE_ID())){
                    dbContext.exe("INSERT INTO t_person_offline(offline,faceImage, createTime) VALUES (?, ?, ?)",
                            par(offlineId,rlsbxxjsStruct.getUSER_PIC(),DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp()));
                    LOG.debug("=========离线视频人脸识别入库成功！");
                }else{
                    try {
                        String userid = null, name=null,phone=null,unit=null,driver_point=null;
                        tpersonOffline = dbContext.entityFindOneBySQL(TpersonOffline.class,
                                "select * from t_person_offline where offline = ? and userid = ?",
                                par(offlineId, rlsbxxjsStruct.getFACE_ID()));
                        Map<String, Object> userinfo = dbContext.qryMap("select a.id,ifnull(a.pname, '陌生人') as name,a.phone,ifnull(a.unit,'') unit,ifnull(a.driver_point,'') driver_point from bus_person a" +
                                " where id = ?", par(rlsbxxjsStruct.getFACE_ID()));
                        userid = (String) userinfo.get("id");
                        name= (String) userinfo.get("name");
                        phone= (String) userinfo.get("phone");
                        unit= (String) userinfo.get("unit");
                        driver_point= (String) userinfo.get("driver_point");
                        if (tpersonOffline!=null){
                            tpersonOffline.setFaceImage(rlsbxxjsStruct.getUSER_PIC());
                            tpersonOffline.setCreateTime(DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp());
                            tpersonOffline.update();
                            LOG.debug("=========离线视频人脸识别入库成功！");
                        }else{
                            tpersonOffline = new TpersonOffline();
                            tpersonOffline.setOffline(offlineId);
                            tpersonOffline.setUserid(userid);
                            tpersonOffline.setFaceid(null);
                            tpersonOffline.setName(name);
                            tpersonOffline.setPhone(phone);
                            tpersonOffline.setUnit(unit);
                            tpersonOffline.setDriverPoint(driver_point);
                            tpersonOffline.setFaceImage(rlsbxxjsStruct.getUSER_PIC());
                            tpersonOffline.setCreateTime(DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp());
                            tpersonOffline.insert();
                            LOG.debug("=========离线视频人脸识别入库成功！");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }else{
                String planId = getPlanIdByCamera(rlsbxxjsStruct.getCAMERA_ID(), rlsbxxjsStruct.getFIND_TIME());
                if (StrUtil.isEmpty(planId)) {
                    return "The current time period of the device is not bound to a work plan";
                }
                String userid = null, kind = null,name=null,phone=null,unit=null,driver_point=null;
                if (StrUtil.isEmpty(rlsbxxjsStruct.getFACE_ID())) {
                    rlsbxxjsStruct.setFACE_ID(null);
                    kind = "0";
                } else {
                    Map<String, Object> userinfo = dbContext.qryMap("select a.id, b.operation_id,ifnull(a.pname, '陌生人') as name,a.phone,ifnull(a.unit,'') unit,ifnull(a.driver_point,'') driver_point from bus_person a" +
                            " left join(" +
                            "select operation_id, person_id from bus_operationplan_person where operation_id = ? " +
                            "UNION all " +
                            "select id operation_id, OPERATION_CHARGE_PERSON_ID person_id from bus_operationplan where id = ? " +
                            "UNION all " +
                            "select id operation_id, SAFETY_CHARGE_PERSON_ID person_id from bus_operationplan where id = ? " +
                            ")b on a.id = b.person_id " +
                            " where id = ?", par(planId, planId, planId, rlsbxxjsStruct.getFACE_ID()));
                    userid = (String) userinfo.get("id");
                    kind = StrUtil.isEmpty((String) userinfo.get("operation_id")) ? "0" : "1";
                    name= (String) userinfo.get("name");
                    phone= (String) userinfo.get("phone");
                    unit= (String) userinfo.get("unit");
                    driver_point= (String) userinfo.get("driver_point");
                }
                TPersonRecord personRecord = null;
                try {
                    personRecord = dbContext.entityLoadBySql(TPersonRecord.class,
                            "select * from t_person_record where planid = ? and userid = ?",
                            par(planId, rlsbxxjsStruct.getFACE_ID()));
                } catch (Exception e) {

                }
                if (personRecord != null) { // 存在则更新
                    personRecord.setFaceImage(rlsbxxjsStruct.getUSER_PIC());
                    personRecord.setCreateTime(DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp());
                    personRecord.update();
                } else { // 不存在则插入
                    personRecord = new TPersonRecord();
                    personRecord.setPlanid(planId);
                    personRecord.setUserid(userid);
                    personRecord.setUserKind(Byte.valueOf(kind));
                    personRecord.setFaceImage(rlsbxxjsStruct.getUSER_PIC());
                    personRecord.setCreateTime(DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp());
                    personRecord.insert();
                }
                WebSocketServer.sendMessage("侦测到新的人脸信息", "人脸识别", 3, 3);

                JSONObject json=new JSONObject();
                json.put("type","ry");
                json.put("userKind",kind);
                json.put("faceId",rlsbxxjsStruct.getFACE_ID());
                json.put("faceImage",rlsbxxjsStruct.getUSER_PIC());
                json.put("createTime",DateUtil.parse(rlsbxxjsStruct.getFIND_TIME()).toTimestamp());
                json.put("name",StrUtil.isNotEmpty(name)? name:"陌生人");
                json.put("phone",StrUtil.isNotEmpty(phone)? phone:"");
                json.put("unit",StrUtil.isNotEmpty(unit)? unit:"");
                json.put("driver_point",StrUtil.isNotEmpty(driver_point)? driver_point:"");
                if (!json.isEmpty()){
                    WzMsgPushServer.push(planId, json.toString());
                }
                return "success";
            }
        }
        return "faild";
    }

    //人脸图像接收
    public Object rlxxHandle(RLXXStruct rlxxStruct) {
        if (rlxxStruct != null) {
            if (StrUtil.isEmpty(rlxxStruct.getPhone()) || StrUtil.isEmpty(rlxxStruct.getPic())) {
                return "param error";
            }
            String face = rlxxStruct.getPic();

            String path = StrUtil.format("{}_{}.jpg", rlxxStruct.getPhone(), DateUtil.current());
            String pathSacle = StrUtil.format("{}_{}_scacle.jpg", rlxxStruct.getPhone(), DateUtil.current());
            if (face.indexOf(",") == -1) {
                String img = StrUtil.format("{}{}{}", environment.getProperty("local.facepath"), File.separator, path);
                String imgScacle = StrUtil.format("{}{}{}", environment.getProperty("local.facepath"), File.separator, pathSacle);
                FileUtil.writeBytes(Base64.decode(face), img);
                ImgUtil.scale(
                        FileUtil.file(img),
                        FileUtil.file(imgScacle),
                        0.5f//缩放比例
                );
            } else {
                String img = StrUtil.format("{}{}{}", environment.getProperty("local.facepath"), File.separator, path);
                String imgScacle = StrUtil.format("{}{}{}", environment.getProperty("local.facepath"), File.separator, pathSacle);
                FileUtil.writeBytes(Base64.decode(face.substring(face.indexOf(",") + 1)), img);
                ImgUtil.scale(
                        FileUtil.file(img),
                        FileUtil.file(imgScacle),
                        0.5f//缩放比例
                );
            }
            LOG.info("更新{}人脸图片数据", rlxxStruct.getPhone());
            int c = dbContext.qryIntValue("select count(1) from  bus_person where phone=?", par(rlxxStruct.getPhone()));
            if (c == 0) {
                BusPerson busPerson = new BusPerson();
                busPerson.setId(UUID32Generator.getUUID32());
                busPerson.setPname(rlxxStruct.getName());
                busPerson.setSex(rlxxStruct.getSex());
                busPerson.setPhone(rlxxStruct.getPhone());
                Map<String, Object> unitMap = dbContext.qryMap("select id from bus_unit where 1=1 and enterprise_name=?",
                        par(rlxxStruct.getUnit()));
                String id = unitMap.get("id") == null ? "" : unitMap.get("id").toString();
                busPerson.setUnit(id);
                busPerson.setDriverPoint(12);
                busPerson.setMajorType(rlxxStruct.getMajor_type());
                busPerson.setIssueDate(DateUtil.parseDate(rlxxStruct.getIssue_date()).toTimestamp());
                busPerson.setDriverNumber(rlxxStruct.getDriver_number());
                busPerson.setDriverPoint(rlxxStruct.getDriver_point());
                busPerson.setFacePath(path);
                busPerson.insert();
            } else {
                dbContext.exe("update bus_person set face_path=? where phone=?", par(path, rlxxStruct.getPhone()));
            }
            Map<String, Object> user = dbContext.qryMap("select * from bus_person where phone=? limit 1", par(rlxxStruct.getPhone()));
           /* String body = StrUtil.format("{\"msgType\":101,\"data\":{\"USERID\":\"{}\"," +
                    "\"NAME\":\"{}\",\"FACEIMAGE\":\"http://192.168.1.66:9999/face/{}\",\"STATE\":1}}", user.get("id"), user.get("pname"), pathSacle);*/
            String body = StrUtil.format("{\"msgType\":101,\"data\":{\"USERID\":\"{}\"," +
                    "\"NAME\":\"{}\",\"FACEIMAGE\":\"/face/{}\",\"STATE\":1}}", user.get("id"), user.get("pname"), pathSacle);
            LOG.info("推送人脸信息：{}", body);
            String rs = null;
            try {
                rs = HttpUtil.post(environment.getProperty("local.api"), body);
            } catch (Exception e) {
                return "行为识别接口服务无法访问";
            }
            LOG.info("推送人脸信息结果：{}", rs);
            return "upload success";
        }
        return "faild";
    }

    //人员闯入接收
    public Object rycrHandle(List<RYCRJSStruct> list){
        LOG.info("===人员闯入List："+list.toString());
        String camera_code=null;
        if (list!=null&&list.size()>0){
            TEventVideo dto=null;
            for (RYCRJSStruct struct:list){
                if (StrUtil.isEmpty(struct.getCAMERA_ID()) || StrUtil.isEmpty(struct.getALGORITHM_ID())
                        || StrUtil.isEmpty(struct.getVIOLATION_PIC()) || StrUtil.isEmpty(struct.getVIOLATION_TIME())) {
                    return "param error";
                }
                if (!isDate(struct.getVIOLATION_TIME())) {
                    return "time format error,please use yyyy-MM-dd HH:mm:ss";
                }
            }
            boolean islx = false;
            for (RYCRJSStruct struct:list){
                islx = false;
                camera_code = struct.getCAMERA_ID();
                if(camera_code!=null&&camera_code.startsWith("lx_")) {
                    camera_code = camera_code.substring(3);
                    islx = true;
                }
                if(StrUtil.isNotEmpty(struct.getVIOLATION_PIC()) && struct.getVIOLATION_PIC().endsWith(".jpg")) {
                    dto = new TEventVideo();
                    dto.setAlgorithmId("rycy");
                    dto.setCameraNum(camera_code);
                    dto.setImagePath(struct.getVIOLATION_PIC());
                    dto.setXcTime(Timestamp.valueOf(struct.getVIOLATION_TIME()));
                    dto.insert();
                    LOG.info("=========视频轮巡-人员闯入-入库成功！");
                }
                if(islx){
                    service.removeCoon(camera_code);
                    LOG.debug("摄像头[{}]识别完成.", camera_code);
                }
            }
            return "success";
        }
        return "faild";
    }

    //视频识别完成
    public Object jkwcHandle(JKWCStruct jkwcStruct) {
        if (jkwcStruct != null) {
            if (StrUtil.isEmpty(jkwcStruct.getCAMERA_CODE())) {
                return "param error";
            }
            String ccode = jkwcStruct.getCAMERA_CODE();
            if (!StrUtil.isEmpty(ccode)&&ccode.startsWith("offline")){
                String id = ccode.substring(ccode.indexOf("_")+1);
                dbContext.exe("update t_offline_info set status='2' where id = ? ", par(id));
                LOG.debug("=========离线视频识别完成！");
                return "success";
            }
        }
        return "faild";
    }

    public Object steamHandle(GETSTEAMStruct struct) {
        String steam="";
        if (struct!=null){
            if (StrUtil.isEmpty(struct.getCAMERA_CODE())){
                return "param error";
            }
            String code=struct.getCAMERA_CODE();
            if ("1001".equals(code)){
                steam = "rtsp://192.168.1.229:554/live/test2";
            }else{
                steam=service.play(code);
            }

            return steam;
        }
        return "faild";
    }


    private String getPlanIdByCamera(String cameraId, String time) {
        return dbContext.qryString("select a.ID from bus_operationplan a " +
                " left join bus_operationplan_camera b on a.id = b.operation_id " +
                "where " +
                " b.camera_num=? and a.START_TIME <= ? and a.END_TIME >= ? and a.STATUS = 1", par(cameraId, time, time));
    }

    private boolean isDate(String date) {
        DateTime val = null;
        try {
            val = DateUtil.parse(date.replace(" "," "),"yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
        }
        if (val != null)
            return true;
        return false;
    }
}
