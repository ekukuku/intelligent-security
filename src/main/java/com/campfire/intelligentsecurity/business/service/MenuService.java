package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class MenuService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Map<String, Object> menuList() {
        Map<String, Object> result = new HashMap<String, Object>();
        List<TSysMenu> menuList = dbContext.qryEntityList(TSysMenu.class, "select * from t_sys_menu where 1 = 1 order by sort");
        result.put("code", 0);
        result.put("data", menuList);
        return result;
    }

    public Object saveOrUpdate(TSysMenu sysMenu) {
        if (sysMenu.getId() != null) {
            sysMenu.update();
        } else {
            sysMenu.insert();
        }
        return true;
    }

    public Object loadInfo(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select id, title, replace(icon,'layui-icon ','') as icon, href, parent, isLeaf, sort, state, createtime from t_sys_menu where id=?", par(id));
        } else {
            return null;
        }
    }

    public List loadLevel1() {
        return dbContext.qryMapList("select id, title, isLeaf from t_sys_menu where parent=-1 order by sort");
    }

    public Object remove(String ids) {
        return dbContext.exe("delete from t_sys_menu where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object chengeState(String id, String state) {
        return dbContext.exe("update t_sys_menu set state = ? where id = ?", par(state, id));
    }
}
