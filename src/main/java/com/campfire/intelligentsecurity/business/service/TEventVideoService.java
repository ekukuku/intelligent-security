package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.pattern.CronPattern;
import com.campfire.intelligentsecurity.business.bean.TTaskCron;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class TEventVideoService extends BaseService {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object List() {
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT");
        sb.append(" t.id,");
        sb.append(" t.cameraNum,");
        sb.append(" IFNULL(b.camera_name,'未知设备') camera_name,");
        sb.append(" b.camera_type,");
        sb.append(" '人员闯入' AS algorithmId, ");
        sb.append(" t.imagePath, ");
        sb.append(" t.videoPath, ");
        sb.append(" t.state, ");
        sb.append(" t.xcTime, ");
        sb.append(" t.createTime");
        sb.append(" FROM");
        sb.append(" t_event_video t");
        sb.append(" LEFT JOIN bus_camera b on b.camera_num=t.cameraNum");
        sb.append(" where 1=1 ");
        System.out.println(sb.toString());
        return builder(sb.toString(), " order by t.xcTime desc");
    }

    //获取当前轮巡白名单、未在作业中监控设备
    public List<String> cameraOnlineListNoPlan() {
        /*String sql = "SELECT camera_num from bus_camera where camera_num not in (SELECT camera_num from bus_operationplan_camera where operation_id in ( SELECT\n" +
                " p.id \n" +
                " FROM\n" +
                " bus_operationplan p \n" +
                " WHERE\n" +
                " p.STATUS = '1' \n" +
                "  AND ( CURRENT_TIMESTAMP BETWEEN p.START_TIME AND p.END_TIME )))\n";*/
        StringBuffer sb=new StringBuffer();
        sb.append(" SELECT DISTINCT");
        sb.append(" b1.camera_num ");
        sb.append(" FROM");
        sb.append(" bus_camera b1 ");
        sb.append(" WHERE");
        sb.append(" b1.camera_num NOT IN (");
        sb.append(" SELECT");
        sb.append(" b2.camera_num ");
        sb.append(" FROM");
        sb.append(" bus_camera_block b2 ");
        sb.append(" WHERE");
        sb.append(" b2.block = 1 )");
        sb.append(" and b1.camera_num NOT IN (SELECT b3.camera_num  FROM bus_camera_plan b3 WHERE CURRENT_TIME BETWEEN `begin` AND `end` )");
        return dbContext.qryList(sb.toString());
    }

    public Object remove(String ids) {
        return dbContext.exe("delete from t_event_video where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object saveOrUpdate(TTaskCron taskCron) {
        if (taskCron.getId()!=null) {
            if ("hour".equals(taskCron.getPl())){
                taskCron.setCron(StrUtil.format("0 0 */{} * * ?",taskCron.getTime()));
            }else{
                taskCron.setCron(StrUtil.format("0 */{} * * * ?",taskCron.getTime()));
            }
            CronUtil.updatePattern("lunxun",new CronPattern(taskCron.getCron()));
            taskCron.update();
        }
        return true;
    }


    //获取轮巡
    public Map<String, Object> getTask(String task){
        return dbContext.qryMap("select * from t_task_cron where task=?",par(task));
    }


}
