package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusCamera;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.github.drinkjava2.jsqlbox.DB.*;

@Service
public class CameraService extends BaseService {

    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object cameraList() {
        return builder("SELECT id, camera_name, camera_num, camera_code, create_time, del_flag, camera_type, stream, remark from bus_camera where 1=1", " order by camera_num desc");
    }

    public Object loadInfo(String id) {
        if (StrUtil.isNotEmpty(id)) {
            return dbContext.qryMap("select id, camera_name, camera_num, camera_code, create_time, del_flag, camera_type, stream, remark, yyly, substation from bus_camera where id=?", par(id));
        } else {
            return null;
        }
    }

    public Object saveOrUpdate(BusCamera busCamera) {
        if (!StrUtil.isEmpty(busCamera.getId())) {
            dbContext.exe("update bus_camera set camera_name=?, camera_num=?, camera_code = ?, del_flag = ?, " +
                    "camera_type = ?, stream = ?, remark = ?, yyly = ?, substation = ? where id = ?", par(busCamera.getCameraName(), busCamera.getCameraNum(),
                    busCamera.getCameraCode(), busCamera.getDelFlag(), busCamera.getCameraType(), busCamera.getStream(), busCamera.getRemark(), busCamera.getYyly(), busCamera.getSubstation(), busCamera.getId()));
        } else {
            busCamera.insert();
        }
        return true;
    }

    public Object remove(String ids) {
        return dbContext.exe("delete from bus_camera where id in('" + ids.replaceAll(",", "','") + "')");
    }

    public Object chengeState(String id, String state) {
        return dbContext.exe("update bus_camera set enable = ? where id = ?", par(state, id));
    }

    public int isExist(String camera_num, String id) {
        return dbContext.qryIntValue("select count(*) as num from bus_camera where 1=1",
                noBlank(" and camera_num =?", camera_num),
                noBlank(" and id !=?", id));
    }

    public Object loadProfessionalType() {
        return dbContext.qryMapList("select LABEL as name, VALUE as id from t_sys_dict where TYPE = 'camera_yyly' order by SORT");
    }
}
