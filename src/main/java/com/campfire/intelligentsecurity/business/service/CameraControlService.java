package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.bean.CameraAlgorithm;
import com.campfire.intelligentsecurity.business.bean.camera.AuthStruct;
import com.campfire.intelligentsecurity.sys.kit.ComUtil;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jdialects.id.UUID32Generator;
import com.github.drinkjava2.jsqlbox.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class CameraControlService extends BaseService {

    @Value("${lunxun.uploadFolder}")
    private String uploadFolder;
    @Value("${lunxun.cookie}")
    private String cookie;
    @Value("${lunxun.checkMode}")
    private String checkMode;
    @Value("${lunxun.batchNum}")
    private String batchNum;

    @Autowired
    AuthStruct struct;

    @Autowired
    Environment environment;

    @Autowired
    CameraControlService cameraControlService;
    @Autowired
    TEventVideoService tEventVideoService;
    private int index = 0;
    private Map<String, String> picsMap = new HashMap<>();
    private List<String> sbList = new ArrayList<>();
    private List<String> connPool = new ArrayList<>();


    public void GetToken() {
        //struct.setToken("");
        try {
            String resultStr = HttpUtil.post(struct.getTokenurl(), StrUtil.format("{\"ak\":\"{}\",\"sk\":\"{}\"}", struct.getAk(), struct.getSk()), 10000);
            LOG.info("云台控制token：{}", resultStr);
            JSONObject resultJbj = JSONUtil.parseObj(resultStr);
            String resultCode = resultJbj.getStr("successful", "");
            if (resultCode.equals("true")) {
                JSONObject valObj = resultJbj.getJSONObject("resultValue");
                String ak = valObj.getStr("ak", "");
                if (!ak.equals(struct.getAk())) {
                    LOG.warn("返回ak与注册ak不一致");
                }
                struct.setToken(valObj.getStr("token", ""));
            } else if (resultCode.equals("401")) {
                LOG.warn("鉴权失败，AK或SK错误");
            } else {
                LOG.warn("未知的响应码");
            }
        } catch (Exception e) {
            LOG.error("云台控制token更新异常：{}", e.getMessage());
        }
    }

    public boolean Control(String cameraid, String eventCode, String stopCode, int rspeed, int cspeed) {
        if (StrUtil.isEmpty(struct.getToken())) {
            LOG.error("云台控制token为空");
            return false;
        }
        boolean flag = false;
        if (StrUtil.isEmpty(eventCode) || StrUtil.isEmpty(stopCode)) {
            if (!StrUtil.isEmpty(eventCode)) {
                flag = req(cameraid, eventCode, rspeed, cspeed);
            } else {
                flag = req(cameraid, stopCode, rspeed, cspeed);
            }
        } else {
            flag = req(cameraid, eventCode, rspeed, cspeed);
            ThreadUtil.sleep(500);
            flag = req(cameraid, stopCode, rspeed, cspeed);
        }
        return flag;
    }

    private boolean req(String cameraid, String event, int rspeed, int cspeed) {
        rspeed = 5;
        cspeed = 5;
        LOG.info("控制云台事件：{} 摄像头:{} 行速度：{} 列速度：{}", event, cameraid, rspeed, cspeed);
        Map<String, Object> formMap = new HashMap<>();
        formMap.put("ak", struct.getAk());
        formMap.put("token", struct.getToken());
        formMap.put("timestamp", DateUtil.current());
        formMap.put("nonce", UUID32Generator.getUUID32());
        JSONObject body = JSONUtil.createObj().set("sessionId", UUID32Generator.getUUID32()).set("code", cameraid).set("cmd", StrUtil.format("{}", event)).set("param1", StrUtil.format("{}", rspeed)).set("param2", StrUtil.format("{}", cspeed)).set("param3", "").set("param4", "");
        LOG.info("云台控制Body：{}", body);
        LOG.info("云台控制Form：{}", JSONUtil.toJsonStr(formMap));
        String rsBodyStr = HttpRequest.post(StrUtil.format("{}?{}", struct.getControlurl(), HttpUtil.toParams(formMap))).body(body.toString()).timeout(10000).execute().body();
        LOG.info("云台控制接口返回:{}", rsBodyStr);
        JSONObject rsBody = JSONUtil.parseObj(rsBodyStr);
        if (rsBody.getStr("resultCode", "").equals("200")) {
            return true;
        }
        return false;
    }

    public String play(String camera_num) {
        return play(camera_num, "mp4"); // 行为识别平台需要mp4格式
    }

    public String play(String camera_num, String format) {
        if (StrUtil.isEmpty(struct.getToken())) {
            LOG.error("云台控制token为空");
            return "";
        }
        Map<String, Object> formMap = new HashMap<>();
        formMap.put("code", camera_num);
        formMap.put("sessionId", UUID32Generator.getUUID32());
        formMap.put("format", format);
        formMap.put("rate", "sub"); // main 主码流 sub 辅码流
        formMap.put("redirect", false);
        formMap.put("ak", struct.getAk());
        formMap.put("token", struct.getToken());
        formMap.put("timestamp", DateUtil.current());
        formMap.put("nonce", UUID32Generator.getUUID32());
        LOG.info("视频播放Form：{}", JSONUtil.toJsonStr(formMap));
        String rsBodyStr = HttpUtil.get(struct.getPlayurl(), formMap);
        LOG.info("视频播放接口返回:{}", rsBodyStr);
        JSONObject rsBody = JSONUtil.parseObj(rsBodyStr);
        if (rsBody.getStr("resultCode", "").equals("301")) {
            return rsBody.getStr("location", "");
        }
        return "";
    }


    public void lunxun() {
        if(!StrUtil.isEmpty(checkMode)&&"1".equals(checkMode)){
            //开始抓图
            try {
                new getPicThread().start();
                LOG.info("Waiting 60 seconds");
                Thread.sleep(60000);
                //开始下载抓图推送
                new readPicThread().start();
            } catch (Exception e) {
                updFlag(0);
                LOG.error("轮巡进程异常:{}", e.getMessage());
            }
        } else {
            try {
                new videoCheckThread().start();
            } catch (Exception e) {
                updFlag(0);
                LOG.error("轮巡进程异常:{}", e.getMessage());
            }
        }
    }

    //重置轮巡状态
    public void updFlag(int flag){
        dbContext.exe("update t_task_cron set flag=? where task='lunxun'",par(flag));
    }

    public class videoCheckThread extends Thread {
        public void run() {
            long startTime = System.currentTimeMillis();
            try {
                updFlag(1);
                LOG.info("{}-摄像头巡检(视频抽帧)已开始", DateUtil.date());
                sbList = tEventVideoService.cameraOnlineListNoPlan();
                LOG.info("当前未在作业中的设备数量：{}", sbList.size());
                if (sbList.size() > 0 && sbList != null) {
                    //开始遍历设备，拉流抽帧
                    String camera_code, stream = null;
                    connPool = new ArrayList<>();
                    int batch, curIndex=0;
                    try {
                        batch = Integer.valueOf(batchNum);
                    }catch (Exception e){
                        batch = 3;
                    }
                    int maxWait = 5, times = 0;
                    while(curIndex < sbList.size()){
                        if(connPool.size()<batch) {
                            times=0; // 重置等待次数
                            LOG.info("摄像头巡检中...{}/{}", curIndex+1, sbList.size());
                            camera_code = sbList.get(curIndex);
                            LOG.info("视频巡检 设备编号: {}", camera_code);
                            stream = play(camera_code); // 根据设备编号获取播放地址
//                            stream = "rtsp://192.168.1.60:554/live/test/";
                            // TODO 推送视频流地址给智能识别平台
                            if(StrUtil.isNotEmpty(camera_code) && StrUtil.isNotEmpty(stream)) {
                                if(lunxunControl(camera_code, stream)) {
                                    connPool.add(camera_code);
                                    Thread.sleep(2000);
                                }
                            }
                            curIndex++;
                        }else{
                            times++;
                            LOG.info("视频轮巡缓冲池已满，等待中({}/{}).....", times, maxWait);
                            Thread.sleep(5000); // 扫描间隔
                            if(times >= maxWait){
                                LOG.info("强制结束(CODE:01)....");
                                updFlag(0); // 强制结束
                                break;
                            }
                        }
                    }
                    while(connPool.size()>0 && times < maxWait){
                        // 等待全部结束
                        times++;
                        LOG.info("遍历完成，等待结束({}/{}).....", times, maxWait);
                        Thread.sleep(1000);
                        if(times >= maxWait){
                            LOG.info("强制结束(CODE:02)....");
                            updFlag(0); // 强制结束
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                updFlag(0);
                LOG.info("摄像头巡检(视频抽帧)发生异常:{}", e.getMessage());
            } finally {
                updFlag(0);
                long endTime = System.currentTimeMillis();
                LOG.info("{}-摄像头巡检(视频抽帧)已结束,共计用时：{}秒", DateUtil.date(), (endTime - startTime) / 1000);
            }
        }
    }

    public void removeCoon(String camera_code){
        connPool.remove(camera_code);
        LOG.info("摄像头 {} 巡检完成, 缓冲池剩余: {}", camera_code, connPool.size());
    }

    public class getPicThread extends Thread {
        public void run() {
            long startTime = System.currentTimeMillis();
            try {
                updFlag(1);
                LOG.info("{}-轮巡抓图已开始", DateUtil.date());
                sbList = tEventVideoService.cameraOnlineListNoPlan();
                //测试一个设备
                /*sbList=new ArrayList<>();
                sbList.add("042010000003010715");
                sbList.add("042250000003070001");
                sbList.add("041710000003040015");*/
                LOG.info("当前未在作业中的设备数量：{}", sbList.size());
                if (sbList.size() > 0 && sbList != null) {
                    picsMap = new HashMap<>();
                    //开始循环抓图
                    String result = null;
                    for (String camera_code : sbList) {
                        result = getSnapByCamera(camera_code);
                        if ("-1".equals(result)) {
                            LOG.info("getPic获取token为null,轮巡抓图结束");
                            updFlag(0);
                            return;
                        }
                        if (StrUtil.isNotEmpty(result) && !"-1".equals(result)){
                            picsMap.put(camera_code, result);
                        }
                        Thread.sleep(3000);
                    }
                    LOG.info("getAllPic成功 list:{} picsMap count:{}", sbList.size(), picsMap.size());
                }
            } catch (Exception e) {
                updFlag(0);
                LOG.info("轮巡抓图异常:{}", e.getMessage());
            }finally {
                long endTime = System.currentTimeMillis();
                LOG.info("{}-轮巡抓图已结束,共计用时：{}秒", DateUtil.date(), (endTime - startTime) / 1000);
            }
        }
    }

    public class readPicThread extends Thread {
        public void run() {
            long startTime = 0;
            try {
                startTime = System.currentTimeMillis();
                LOG.info("{}-轮巡下载抓图已开始", DateUtil.date());
                List<List<String>> cameraCodeGroup = CollUtil.split(sbList, 50);
                LOG.info("Group: {}", cameraCodeGroup.size());
                for (List<String> cameraCodeList : cameraCodeGroup) {
                    LOG.info("List:{}", cameraCodeList);
                    //判断在线的设备
//                    List<String> onlineList=codeList;
//                    List<String> onlineList = getStatusByCamera(codeList);
//                    LOG.info("当前onlineList：" + onlineList.size());
                    //开始图片推送
                    JSONArray eventPics = JSONUtil.createArray();
                    for (String camera_code : cameraCodeList) {
                        Thread.sleep(3000);
                        String picUrl = picsMap.get(camera_code);
                        if (StrUtil.isNotEmpty(picUrl)){
                            LOG.info("图片下载地址 -> {} ", picUrl);
                            String picFile = uploadFolder + camera_code + "-" + System.currentTimeMillis() + ".jpg";
                            try {
                                HttpUtil.createGet(picUrl, true).cookie(cookie).execute().writeBody(picFile);
                                LOG.info("图片大小 -> length:{} ", FileUtil.file(picFile).length());
                            } catch (Exception e) {
                                LOG.info("cameraCode：{},download pic异常",camera_code);
                            }
                            if (FileUtil.file(picFile)!=null){
                                if (FileUtil.file(picFile).length() > 500) {
                                    LOG.info("AddPic -> picUrl:{} fileName：{}", picUrl, picFile);
                                    eventPics.add(JSONUtil.createObj().set("CAMERA_CODE", camera_code).set("STREAM", picFile.replace("D:/static", "")));
                                } else {
                                    LOG.info("DelPic -> picUrl:{} fileName：{}", picUrl, picFile);
                                    FileUtil.file(picFile).delete();
                                }
                            }
                        }
                    }
                    LOG.info("本组pic下载完成：{}", eventPics);
                    if (eventPics.size() > 0) {
                        String body = StrUtil.format("{\"msgType\":205,\"data\":{}}", eventPics);
                        LOG.info("本组人员闯入轮巡推送内容：{}", body);
                        String rs = HttpUtil.post(environment.getProperty("local.api"), body, 3000);
                        LOG.info("本组人员闯入轮巡推送返回：{}", rs);
                    } else {
                        LOG.info("==该组设备无可推送的pic==");
                    }
                }
            } catch (Exception e) {
                updFlag(0);
                LOG.info("==轮巡下载pic异常==");
            }finally {
                updFlag(0);
                long endTime = System.currentTimeMillis();
                LOG.info("{}-轮巡下载抓图推送已结束,共计用时：{}秒", DateUtil.date(), (endTime - startTime) / 1000);
            }

        }
    }


    /**
     * 获取设备在线状态
     *
     * @param cameraCode
     * @return
     */
    public List<String> getStatusByCamera(List<String> cameraCode) {
        LOG.info("获取状态==");
        List<String> list = new ArrayList<>();
        /*if (StrUtil.isEmpty(struct.getToken())) {
            LOG.info("统一视频平台token为空，请先获取token");
            return list;
        }*/
        Map<String, Object> formMap = new HashMap<>();
        formMap.put("ak", struct.getAk());
        formMap.put("token", struct.getToken());
        formMap.put("timestamp", DateUtil.current());
        formMap.put("nonce", IdUtil.simpleUUID());
        LOG.info("formMap：{}", formMap);
        JSONObject body = JSONUtil.createObj().set("devCodes", cameraCode.toArray());
        LOG.info("请求体：" + body.toString());
        try {
            LOG.info(struct.getStatusurl());
            String rsBodyStr = HttpRequest.post(StrUtil.format("{}?{}", struct.getStatusurl(), HttpUtil.toParams(formMap))).body(body.toString()).timeout(10000).execute().body();
            //String rsBodyStr="{\"successful\":true,\"resultValue\":[{\"id\":\"04201000000301037200000000000000\",\"devCode\":\"042010000003010372\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037300000000000000\",\"devCode\":\"042010000003010373\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037400000000000000\",\"devCode\":\"042010000003010374\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-3#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037500000000000000\",\"devCode\":\"042010000003010375\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-4#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037600000000000000\",\"devCode\":\"042010000003010376\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-电缆层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037700000000000000\",\"devCode\":\"042010000003010377\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-电缆层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037800000000000000\",\"devCode\":\"042010000003010378\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301037900000000000000\",\"devCode\":\"042010000003010379\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-设备层-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038000000000000000\",\"devCode\":\"042010000003010380\",\"devName\":\"河北-雄安-设备-10kV河西G3开关站-室外-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990024\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038300000000000000\",\"devCode\":\"042010000003010383\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038400000000000000\",\"devCode\":\"042010000003010384\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038500000000000000\",\"devCode\":\"042010000003010385\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-3#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038600000000000000\",\"devCode\":\"042010000003010386\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-4#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038700000000000000\",\"devCode\":\"042010000003010387\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-电缆层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038800000000000000\",\"devCode\":\"042010000003010388\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-电缆层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301038900000000000000\",\"devCode\":\"042010000003010389\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039000000000000000\",\"devCode\":\"042010000003010390\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039100000000000000\",\"devCode\":\"042010000003010391\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-室外-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039200000000000000\",\"devCode\":\"042010000003010392\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-室外-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039300000000000000\",\"devCode\":\"042010000003010393\",\"devName\":\"河北-雄安-设备-10kV河西G5开关站-设备层-1#全景\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039400000000000000\",\"devCode\":\"042010000003010394\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-室外-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039500000000000000\",\"devCode\":\"042010000003010395\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-室外-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039600000000000000\",\"devCode\":\"042010000003010396\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039700000000000000\",\"devCode\":\"042010000003010397\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039800000000000000\",\"devCode\":\"042010000003010398\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-1#全景\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301039900000000000000\",\"devCode\":\"042010000003010399\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-3#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040000000000000000\",\"devCode\":\"042010000003010400\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-4#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040100000000000000\",\"devCode\":\"042010000003010401\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-设备层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040200000000000000\",\"devCode\":\"042010000003010402\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-电缆层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040300000000000000\",\"devCode\":\"042010000003010403\",\"devName\":\"河北-雄安-设备-10kV剧河H1开关站-电缆层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040400000000000000\",\"devCode\":\"042010000003010404\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-室外-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040500000000000000\",\"devCode\":\"042010000003010405\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-室外-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040600000000000000\",\"devCode\":\"042010000003010406\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-1#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040700000000000000\",\"devCode\":\"042010000003010407\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-2#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040800000000000000\",\"devCode\":\"042010000003010408\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-1#全景\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301040900000000000000\",\"devCode\":\"042010000003010409\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-3#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301041000000000000000\",\"devCode\":\"042010000003010410\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-4#枪机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301041100000000000000\",\"devCode\":\"042010000003010411\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-设备层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301041200000000000000\",\"devCode\":\"042010000003010412\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-电缆层-1#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000301041300000000000000\",\"devCode\":\"042010000003010413\",\"devName\":\"河北-雄安-设备-10kV剧河H2开关站-电缆层-2#球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"25d7222ec76b4d55818c2e94ac16ceaf\",\"devCode\":\"042010000003010747\",\"devName\":\"雄安-河西站-西电梯-球机\",\"devType\":\"01\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":null,\"latLtude\":null,\"nodeId\":\"042010000002010001\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":\"雄安-河西站-西电梯-球机\",\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000352003600000000000000\",\"devCode\":\"042010000003520036\",\"devName\":\"10kV河西G5开关站\",\"devType\":\"52\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990025\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000352003700000000000000\",\"devCode\":\"042010000003520037\",\"devName\":\"10kV剧河H1开关站\",\"devType\":\"52\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990026\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null},{\"id\":\"04201000000352003800000000000000\",\"devCode\":\"042010000003520038\",\"devName\":\"10kV剧河H2开关站\",\"devType\":\"52\",\"devTypeName\":null,\"status\":1,\"decoderTag\":150,\"sysinfoCode\":\"040010002301000000\",\"longLtude\":0.0,\"latLtude\":0.0,\"nodeId\":\"042010000002990027\",\"fontTypeCode\":null,\"peerId\":null,\"sourceBusinessCode\":null,\"sourceBusinessName\":null,\"frontIp\":null,\"frontId\":null,\"isOuternet\":0,\"isShielding\":0,\"dvrIp\":null,\"adminuser\":null,\"adminpasswd\":null,\"tcpPort\":null,\"udpPort\":null,\"companyType\":null,\"spsDataType\":null,\"spsHead\":null,\"maxValue\":0,\"minValue\":0,\"frontName\":null,\"protocolType\":\"1\",\"gisPeerCode\":null,\"devShortName\":null,\"socre\":0,\"frontStatus\":null,\"businessName\":null,\"businessId\":null,\"isCollected\":null}],\"resultHint\":\"调用成功\",\"errorPage\":null,\"type\":null}";
            LOG.info(StrUtil.format("查询指定设备状态接口返回:{}", rsBodyStr));
            com.alibaba.fastjson.JSONObject rsBody = com.alibaba.fastjson.JSONObject.parseObject(rsBodyStr);
            if (rsBody != null) {
                if (rsBody.get("successful") != null) {
                    if ((boolean) rsBody.get("successful") == true) {
                        com.alibaba.fastjson.JSONArray array = (com.alibaba.fastjson.JSONArray) rsBody.get("resultValue");
                        com.alibaba.fastjson.JSONObject jsonObj = null;
                        for (int i = 0; i < array.size(); i++) {
                            jsonObj = (com.alibaba.fastjson.JSONObject) array.get(i);
                            if (jsonObj != null) {
                                if (jsonObj.get("status") != null) {
                                    if ("1".equals(jsonObj.getString("status"))) {
                                        if (jsonObj.get("devCode") != null) {
                                            list.add(jsonObj.getString("devCode"));
                                        } else {
                                            LOG.info("devCode为null");
                                        }
                                    }
                                } else {
                                    LOG.info("status为null");
                                }
                            } else {
                                LOG.info("jsonObj为null");
                            }
                        }
                        LOG.info("本组当前在线设备：" + list.toString());
                        return list;
                    } else {
                        LOG.info("successful为false");
                        LOG.info(rsBody.toString());
                    }
                } else {
                    LOG.info("successful为null");
                }
            } else {
                LOG.info("请求返回为null");
            }
        } catch (Exception e) {
            LOG.info(StrUtil.format("查询指定设备信息状态调用异常：{}", e.getMessage()));
        }
        return list;
    }

    /**
     * 单次监控设备抓图
     *
     * @param cameraCode
     * @return
     */
    public String getSnapByCamera(String cameraCode) {
        LOG.info("{}获取抓图==", cameraCode);
        if (StrUtil.isEmpty(struct.getToken())) {
            LOG.info("统一视频平台token为空，请先获取token");
            return "-1";
        }
        Map<String, Object> formMap = new HashMap<>();
        formMap.put("ak", struct.getAk());
        formMap.put("token", struct.getToken());
        formMap.put("timestamp", DateUtil.current());
        formMap.put("nonce", IdUtil.simpleUUID());
        JSONObject body = JSONUtil.createObj().set("sessionId", IdUtil.simpleUUID()).set("code", cameraCode);
        try {
            String rsBodyStr = HttpRequest.post(StrUtil.format("{}?{}", struct.getSnapurl(), HttpUtil.toParams(formMap))).body(body.toString()).timeout(10000).execute().body();
            LOG.info(StrUtil.format("单次抓图接口{} \n Body:{} \n Form:{} \n返回:{}", struct.getSnapurl(), body, formMap, rsBodyStr));
            if (StrUtil.isNotEmpty(rsBodyStr)) {
                JSONObject rsBody = JSONUtil.parseObj(rsBodyStr);
                if (rsBody != null) {
                    if (rsBody.containsKey("resultCode")) {
                        if (rsBody.getStr("resultCode", "").equals("200")) {
                            JSONObject snapResult = rsBody.getJSONObject("resultValue");
                            if (snapResult != null) {
                                if (snapResult.containsKey("fileUrl")) {
                                    if (StrUtil.isNotBlank(snapResult.getStr("fileUrl"))) {
                                        return struct.getUrl() + snapResult.getStr("fileUrl");
//                                    String picFile = uploadFolder + cameraCode + "-" + System.currentTimeMillis() + ".jpg";
//                                    Map map = new HashMap();
//                                    map.put("cameraCode", cameraCode);
//                                    map.put("picUrl", picUrl);
//                                    map.put("picFile", picFile);
//                                    PicList.add(map);
                                   /* while (true) {
                                        String picFile = uploadFolder + cameraCode + "-" + System.currentTimeMillis() + ".jpg";
                                        LOG.info("One -> picUrl:{} fileName：{}", picUrl, picFile);
                                        HttpUtil.createGet(picUrl, true).cookie(cookie).execute().writeBody(picFile);
                                        Thread.sleep(5000);
                                        String picFile2 = uploadFolder + cameraCode + "-" + System.currentTimeMillis() + ".jpg";
                                        LOG.info("Two -> picUrl:{} fileName：{}", picUrl, picFile2);
                                        HttpUtil.createGet(picUrl, true).cookie(cookie).execute().writeBody(picFile2);
                                        if (FileUtil.file(picFile2).length() > 500) {
                                            LOG.info("File:{} len:{}", picFile2, FileUtil.file(picFile2).length());
                                            String lasturl = picFile2.replace("D:/static", "");
                                            return lasturl;
                                        } else {
                                            LOG.info("File:{} len:{} CONTINUE", picFile2, FileUtil.file(picFile2).length());
                                        }
                                    }*/
                                    } else {
                                        LOG.error("fileUrl为空");
                                    }
                                } else {
                                    LOG.error("fileUrl为null");
                                }
                            } else {
                                LOG.error("resultValue为null");
                            }
                        } else {
                            LOG.error("resultCode不等于200");
                        }
                    } else {
                        LOG.error("resultCode为null");
                    }
                } else {
                    LOG.error("请求返回为null");
                }
            } else {
                LOG.error("单次抓图接口返回null");
            }
        } catch (Exception e) {
            LOG.error(StrUtil.format("单次抓图调用异常：{}", e.getMessage()));
        }
        return "";
    }

    /**
     * 推送轮巡摄像头信息
     * true: 推送成功 false: 推送失败
     * */
    public boolean lunxunControl(String camera_code, String stream) {
        LOG.debug("开始设备巡检，camera_code: {}, stream: {}", camera_code, stream);
        JSONObject camera = new JSONObject();
        camera.set("CAMERA_CODE", "lx_"+camera_code);
        camera.set("STREAM", stream);
        String body = StrUtil.format("{\"msgType\":206,\"data\":{}}", camera.toString());
        LOG.debug(body);
        String rs = null;
        try {
            rs = HttpUtil.post(environment.getProperty("local.api"), body, 3000);
            LOG.info("返回信息：{}", rs);
            return true;
        } catch (Exception e) {
            LOG.info("[添加摄像头巡检(拉流)]调用失败... {}", e.getMessage());
            return false;
        }
    }

    public Object watchControl(String add, String remove, String sfId, String planId) {
        JSONObject camera = new JSONObject();
        String camera_code = add;
        if (StrUtil.isNotEmpty(camera_code)) {
            String stream;
            // TODO 本处代码仅用于调试阶段
            if ("1001".equals(camera_code)) {
                //stream = "rtsp://192.168.1.229:554/live/test2";
                stream = "http://192.168.1.60:8080/hdl/live/test/.flv";
                /*if (index==0){
                    //stream = "rtsp://admin:qqqq1111@192.168.1.212:554/streaming/Channels/102";
                    stream = "rtsp://192.168.1.229:554/live/test1";
                }else if (index==1){
                    stream = "rtsp://192.168.1.229:554/live/test2";
                }
                else if (index==2){
                    stream = "rtsp://192.168.1.229:554/live/test3";
                }
                else if (index==3){
                    stream = "rtsp://192.168.1.229:554/live/test4";
                }
                else if (index==4){
                    stream = "rtsp://192.168.1.229:554/live/test5";
                }else{
                    stream = "rtsp://192.168.1.229:554/live/test5";
                }*/
            } else if ("1002".equals(camera_code)) {
                stream = "http://192.168.1.60:8080/hdl/live/test/.flv";
            }  else if ("041710000003040015".equals(camera_code)) {
                stream = "041710000003040015";
            } else {
                //stream = "rtsp://192.168.1.229:554/live/test2";
                stream = cameraControlService.play(camera_code);
            }
            LOG.debug(stream);
            if (!StrUtil.isEmpty(stream)) {
                camera.set("CAMERA_CODE", camera_code);
                camera.set("STREAM", stream);
                if (StrUtil.isEmpty(sfId)) {
                    if (StrUtil.isEmpty(planId)) {
                        sfId = dbContext.qryString("select GROUP_CONCAT(b.algorithm_id SEPARATOR ',') algorithm from bus_operationplan_algorithm b LEFT JOIN bus_operationplan p on p.ID=b.operation_id LEFT JOIN bus_operationplan_camera c on c.operation_id=b.operation_id where c.camera_num=? and p.`STATUS`='1' and (CURRENT_TIMESTAMP BETWEEN p.START_TIME and p.END_TIME) ", par(camera_code));
                        if (StrUtil.isEmpty(sfId)) {
                            sfId = dbContext.qryString(" select GROUP_CONCAT(DISTINCT algorithmKey SEPARATOR ',') from t_sys_event_name where enable=1");
                        }
                    } else {
                        sfId = dbContext.qryString("SELECT GROUP_CONCAT(algorithm_id SEPARATOR ',') algorithm from bus_operationplan_algorithm where operation_id=? ", par(planId));
                    }
                }
            } else {
                return "统一视频平台接口调用失败";
            }
            String[] sfIds = sfId.split(",");
            CameraAlgorithm bean = null;
            dbContext.exe("delete from camera_algorithm where camera_num=? ", par(camera_code));
            for (int k = 0; k < sfIds.length; k++) {
                bean = new CameraAlgorithm();
                bean.setCameraNum(camera_code);
                bean.setAlgorithm(sfIds[k]);
                if (bean != null) {
                    bean.insert();
                }
            }
        }
        String sf = dbContext.qryString("select GROUP_CONCAT(DISTINCT algorithmKey) algorithmKey from t_sys_event_name where algorithmMsg in('" + sfId.replaceAll(",", "','") + "')");
        String body = StrUtil.format("{\"msgType\":201,\"data\":{\"CAMERA_ADD\":{}," + "\"CAMERA_REMOVE\":\"{}\",\"ALGORITHM_LIST\":\"{}\"}}", camera, remove, sf);
        LOG.info("获取当前监测的视频流信息：{}", body);
        String rs = null;
        JSONObject json = null;
        try {
            rs = HttpUtil.post(environment.getProperty("local.api"), body, 3000);
            LOG.info("返回信息：{}", rs);
            if (JSONUtil.parseObj(rs).get("data") != null) {
                json = (JSONObject) JSONUtil.parseObj(rs).get("data");
                JSONArray jsonArray = (JSONArray) json.get("ONLINE_CAMERA");
                String jobContent = null;
                if (jsonArray.size() > 0) {
                    String date = DateUtil.formatDate(DateUtil.date(Calendar.getInstance()));
                    List<String> jobList = new ArrayList<>();
                    List<String> cameraNameList = new ArrayList<>();
                    StringBuffer sql = null;
                    JSONArray array = new JSONArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        sql = new StringBuffer();
                        sql.append("SELECT DISTINCT c.id id, ");
                        sql.append("IFNULL(c1.JOB_CONTENT,'其它') job, ");
                        sql.append("c.camera_name cname ");
                        sql.append("FROM ");
                        sql.append("bus_camera c ");
                        sql.append("left join( ");
                        sql.append("    select c1.camera_num, p1.JOB_CONTENT from bus_operationplan_camera c1 ");
                        sql.append("    left join bus_operationplan p1 on c1.operation_id = p1.id and p1.status!='2'");
                        sql.append("    where p1.START_TIME < ? AND p1.END_TIME > ? ");
                        sql.append(" )c1 on c.camera_num = c1.camera_num ");
                        sql.append("WHERE c.camera_num = ? limit 1");
                        Map<String, Object> map = dbContext.qryMap(sql.toString(), par(date, date, jsonArray.get(i).toString()));
                        if (map != null && map.size() > 0) {
                            jobList.add(map.get("job").toString());
                            cameraNameList.add(map.get("cname").toString());
                            array.add(map.get("id"));
                        }
                    }
                    json.put("job", jobList);
                    json.put("cameraName", cameraNameList);
                    json.put("camera_array", array);
                    LOG.info("返回信息：{}", json);
                }
            }
            return json;
        } catch (Exception e) {
            return "行为识别接口服务无法访问";
        }
    }

    public Object subscribe(String camera_code, String eventType, String isOriginal) {
        if (StrUtil.isEmpty(isOriginal)) {
            isOriginal = "0";
        }
        if ("1".equals(isOriginal)) {
            // 获取原始视频流
            String stream = "";
            // TODO 本处代码仅用于调试阶段
            if ("1001".equals(camera_code)) {
                stream = "rtsp://admin:qqqq1111@192.168.1.212:554/streaming/Channels/102";
            } else if ("1002".equals(camera_code)) {
                stream = "rtsp://admin:qqqq1111@192.168.1.212:554/streaming/Channels/102";
            } else {
                stream = cameraControlService.play(camera_code, "flv");
            }
            return stream;
        } else {
            String body = StrUtil.format("{\"msgType\":202,\"data\":{\"CAMERA_CODE\":\"{}\"," + "\"EVENT_TYPE\":{}}}", camera_code, eventType);
            LOG.info("获取当前监测的视频流信息：{}", body);
            String rs = null;
            try {
                rs = HttpUtil.post(environment.getProperty("local.api"), body, 3000);
                LOG.info("返回信息：{}", rs);
                return JSONUtil.parseObj(rs).get("data");
            } catch (Exception e) {
                return "行为识别接口服务无法访问";
            }
        }
    }
}
