package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.campfire.intelligentsecurity.business.bean.BusCameraAllow;
import com.campfire.intelligentsecurity.business.bean.BusCameraPlan;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class CameraAllowService extends BaseService {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object List() {
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT");
        sb.append(" b1.id id,");
        sb.append(" b1.camera_num cameraNum,");
        sb.append(" b2.camera_name cameraName,");
        sb.append(" b1.block block,");
        sb.append(" b1.t1 t1,");
        sb.append(" b1.t2 t2,");
        sb.append(" b1.t3 t3 ");
        sb.append(" FROM");
        sb.append(" bus_camera_block b1");
        sb.append(" LEFT JOIN bus_camera b2 ON b2.camera_num = b1.camera_num ");
        sb.append(" where 1=1 ");
        return builder(sb.toString());
    }

    public Object saveOrUpdate(BusCameraAllow allow) {
        if (allow.getId()!=null){
            allow.setCameraNum(allow.getSelect());
            allow.update();
        }else{
            dbContext.exe("delete from bus_camera_block where camera_num in('" + allow.getSelect().replaceAll(",", "','") + "')");
            String[] cameraArr=allow.getSelect().split(",");
            BusCameraAllow bean = allow;
            for (String cameraNum:cameraArr){
                bean.setCameraNum(cameraNum);
                bean.insert();
            }
        }
        return true;
    }

    public Object remove(String id) {
        return dbContext.exe("delete from bus_camera_block where id in('" + id.replaceAll(",", "','") + "')");
    }

    public Object load(String id) {
        if (StrUtil.isNotEmpty(id)) {
            StringBuffer sb = new StringBuffer();
            sb.append(" SELECT id, camera_num, block, t1, t2, t3 FROM bus_camera_block where id=?");
            return dbContext.qryMap(sb.toString(), par(id));
        } else {
            return null;
        }
    }

    public Object block(String id, String block) {
        return dbContext.exe("update bus_camera_block set block=? where id=? ",par(block,id));
    }
}
