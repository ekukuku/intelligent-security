package com.campfire.intelligentsecurity.business.service;

import com.campfire.intelligentsecurity.sys.service.BaseService;
import org.springframework.stereotype.Service;

import static com.github.drinkjava2.jsqlbox.DB.par;

@Service
public class ControlEventService extends BaseService {

    public boolean control(String relation_id, String relation_type, String control_type, String control_value) {
        try {
            dbContext.exe("update t_control_record set latest=0 where control_type=? and relation_type=? and relation_id=?",
                    par(control_type, relation_type, relation_id));
            dbContext.exe("insert into t_control_record (relation_id,relation_type, control_value, control_type,latest) values (?,?,?,?,1)",
                    par(relation_id, relation_type, control_value, control_type));
        } catch (Exception e) {
            LOG.error("插入管控记录失败:{}", e.getMessage());
            return false;
        }
        return true;
    }
}
