package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 作业计划和作业人员关系表
 */
@Table(name="bus_operationplan_person")
public class BusOperationplanPerson extends ActiveRecord<BusOperationplanPerson> {
	public static final String OPERATION_ID = "operation_id";

	public static final String PERSON_ID = "person_id";

	public static final String STATUS = "status";

	public static final String IMG_PATH = "img_path";


	/**
	 * 作业ID
	 */
	@Column(name="operation_id", length=192)
	private String operationId;
	/**
	 * 作业人员id
	 */
	@Column(name="person_id", length=192)
	private String personId;
	/**
	 * 0未签到 1签到成功
	 */
	@Column(name="status", length=765)
	private String status;
	/**
	 * 签到成功截图地址
	 */
	@Column(name="img_path", length=1500)
	private String imgPath;

	public String getOperationId(){
		return operationId;
	}

	public BusOperationplanPerson setOperationId(String operationId){
		this.operationId=operationId;
		return this;
	}

	public String getPersonId(){
		return personId;
	}

	public BusOperationplanPerson setPersonId(String personId){
		this.personId=personId;
		return this;
	}

	public String getStatus(){
		return status;
	}

	public BusOperationplanPerson setStatus(String status){
		this.status=status;
		return this;
	}

	public String getImgPath(){
		return imgPath;
	}

	public BusOperationplanPerson setImgPath(String imgPath){
		this.imgPath=imgPath;
		return this;
	}

}
