package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

@Table(name="t_person_record")
public class TPersonRecord extends ActiveRecord<TPersonRecord> {
	public static final String ID = "id";

	public static final String PLANID = "planid";

	public static final String FACEID = "faceId";

	public static final String USERID = "userid";

	public static final String USERKIND = "userKind";

	public static final String FACEIMAGE = "faceImage";

	public static final String CREATETIME = "createTime";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 作业计划ID
	 */
	@Column(name="planid", length=192)
	private String planid;
	/**
	 * 人脸库ID
	 */
	private Integer faceId;
	/**
	 * 人员id
	 */
	@Column(name="userid", length=192)
	private String userid;
	/**
	 * 人员类型 0非作业人员 1作业人员
	 */
	private Byte userKind;
	/**
	 * 人脸截图
	 */
	@Column(name="faceImage", length=765)
	private String faceImage;
	/**
	 * 发现时间
	 */
	private Timestamp createTime;

	public Long getId(){
		return id;
	}

	public TPersonRecord setId(Long id){
		this.id=id;
		return this;
	}

	public String getPlanid(){
		return planid;
	}

	public TPersonRecord setPlanid(String planid){
		this.planid=planid;
		return this;
	}

	public Integer getFaceId(){
		return faceId;
	}

	public TPersonRecord setFaceId(Integer faceId){
		this.faceId=faceId;
		return this;
	}

	public String getUserid(){
		return userid;
	}

	public TPersonRecord setUserid(String userid){
		this.userid=userid;
		return this;
	}

	public Byte getUserKind(){
		return userKind;
	}

	public TPersonRecord setUserKind(Byte userKind){
		this.userKind=userKind;
		return this;
	}

	public String getFaceImage(){
		return faceImage;
	}

	public TPersonRecord setFaceImage(String faceImage){
		this.faceImage=faceImage;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TPersonRecord setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

}
