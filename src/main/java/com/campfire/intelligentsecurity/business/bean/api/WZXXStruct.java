package com.campfire.intelligentsecurity.business.bean.api;

public class WZXXStruct implements ApiData {
    //算法id
    private String ALGORITHM_ID;
    //摄像头id
    private String CAMERA_ID;
    //违章时间
    private String VIOLATION_TIME;
    //违章图片
    private String VIOLATION_PIC;
    //视频片段
    private String VIOLATION_VIDEO;

    public String getALGORITHM_ID() {
        return ALGORITHM_ID;
    }

    public void setALGORITHM_ID(String VIOLATION_KIND) {
        this.ALGORITHM_ID = VIOLATION_KIND;
    }

    public String getCAMERA_ID() {
        return CAMERA_ID;
    }

    public void setCAMERA_ID(String CAMERA_ID) {
        this.CAMERA_ID = CAMERA_ID;
    }

    public String getVIOLATION_TIME() {
        return VIOLATION_TIME;
    }

    public void setVIOLATION_TIME(String VIOLATION_TIME) {
        this.VIOLATION_TIME = VIOLATION_TIME;
    }

    public String getVIOLATION_PIC() {
        return VIOLATION_PIC;
    }

    public void setVIOLATION_PIC(String VIOLATION_PIC) {
        this.VIOLATION_PIC = VIOLATION_PIC;
    }

    public String getVIOLATION_VIDEO() {
        return VIOLATION_VIDEO;
    }

    public void setVIOLATION_VIDEO(String VIOLATION_VIDEO) {
        this.VIOLATION_VIDEO = VIOLATION_VIDEO;
    }
}
