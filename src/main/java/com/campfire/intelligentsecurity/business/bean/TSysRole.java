package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

@Table(name="t_sys_role")
public class TSysRole extends ActiveRecord<TSysRole> {
	public static final String ID = "id";

	public static final String ROLENAME = "roleName";

	public static final String ROLECODE = "roleCode";

	public static final String ENABLE = "enable";

	public static final String DETAILS = "details";

	public static final String CREATETIME = "createTime";

	/**
	 * 主键
	 */
	@Id
	private Integer id;

	/**
	 * 管理员名字
	 */
	@Column(name="roleName", length=150)
	private String roleName;
	/**
	 * 管理员编号：列子：”admin“
	 */
	@Column(name="roleCode", length=150)
	private String roleCode;
	/**
	 * 是否启用：0:true，1是false，默认true
	 */
	private Integer enable;
	/**
	 * 说明
	 */
	@Column(name="details", length=150)
	private String details;
	/**
	 * 创建时间
	 */
	private Timestamp createTime;

	public Integer getId(){
		return id;
	}

	public TSysRole setId(Integer id){
		this.id=id;
		return this;
	}

	public String getRoleName(){
		return roleName;
	}

	public TSysRole setRoleName(String roleName){
		this.roleName=roleName;
		return this;
	}

	public String getRoleCode(){
		return roleCode;
	}

	public TSysRole setRoleCode(String roleCode){
		this.roleCode=roleCode;
		return this;
	}

	public Integer getEnable(){
		return enable;
	}

	public TSysRole setEnable(Integer enable){
		this.enable=enable;
		return this;
	}

	public String getDetails(){
		return details;
	}

	public TSysRole setDetails(String details){
		this.details=details;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TSysRole setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

}
