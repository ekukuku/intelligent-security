package com.campfire.intelligentsecurity.business.bean;

import java.sql.Timestamp;

public class ViolationPoint {
    private Timestamp violation_time;   //违章时间
    private String violation_name;      //违章人员
    private String violation_info;      //违章信息
    private String violation_score;     //违章扣分
    private String pic;                 //违章截图

    public Timestamp getViolation_time() {
        return violation_time;
    }

    public void setViolation_time(Timestamp violation_time) {
        this.violation_time = violation_time;
    }

    public String getViolation_name() {
        return violation_name;
    }

    public void setViolation_name(String violation_name) {
        this.violation_name = violation_name;
    }

    public String getViolation_info() {
        return violation_info;
    }

    public void setViolation_info(String violation_info) {
        this.violation_info = violation_info;
    }

    public String getViolation_score() {
        return violation_score;
    }

    public void setViolation_score(String violation_score) {
        this.violation_score = violation_score;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
