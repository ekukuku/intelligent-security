package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

@Table(name="t_sys_event_name")
public class TSysEventName extends ActiveRecord<TSysEventName> {

    @Id
    private Integer id;

    @Column(name="algorithmKey")
    private String algorithmKey;

    @Column(name="algorithmMsg")
    private String algorithmMsg;

    @Column(name="eventName")
    private String eventName;

    @Column(name="status")
    private Byte status;

    @Column(name="enable")
    private Byte enable;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlgorithmKey() {
        return algorithmKey;
    }

    public void setAlgorithmKey(String algorithmKey) {
        this.algorithmKey = algorithmKey;
    }

    public String getAlgorithmMsg() {
        return algorithmMsg;
    }

    public void setAlgorithmMsg(String algorithmMsg) {
        this.algorithmMsg = algorithmMsg;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getEnable() {
        return enable;
    }

    public void setEnable(Byte enable) {
        this.enable = enable;
    }
}
