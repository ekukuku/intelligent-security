package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="bus_wz_point")
public class BusWzPoint extends ActiveRecord<BusWzPoint> {
	public static final String ID = "id";

	public static final String INFO = "info";

	public static final String POINT = "point";

	public static final String CREATETIME = "createTime";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 人员id
	 */
	@Column(name="info", length=1500)
	private String info;
	/**
	 * 违章信息
	 */
	@Column(name="point", length=192)
	private String point;
	/**
	 * 创建时间
	 */
	private Timestamp createTime;

	public Long getId(){
		return id;
	}

	public BusWzPoint setId(Long id){
		this.id=id;
		return this;
	}

	public String getInfo(){
		return info;
	}

	public BusWzPoint setInfo(String info){
		this.info=info;
		return this;
	}

	public String getPoint(){
		return point;
	}

	public BusWzPoint setPoint(String point){
		this.point=point;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public BusWzPoint setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

}
