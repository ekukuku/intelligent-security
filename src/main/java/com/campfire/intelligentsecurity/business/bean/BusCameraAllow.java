package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 监控设备轮巡白名单
 */
@Table(name="bus_camera_block")
public class BusCameraAllow extends ActiveRecord<BusCameraAllow> {

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 摄像头编号
	 */
	@Column(name="camera_num", length=900)
	private String cameraNum;
	/**
	 * 是否为白名单
	 */
	private Integer block;
	/**
	 * 预留1
	 */
	@Column(name="t1", length=384)
	private String t1;
	/**
	 * 预留2
	 */
	@Column(name="t2", length=384)
	private String t2;
	/**
	 * 预留3
	 */
	@Column(name="t3", length=384)
	private String t3;
	@Column(insertable = false,updatable = false)
	private String select;

	public Long getId(){
		return id;
	}

	public BusCameraAllow setId(Long id){
		this.id=id;
		return this;
	}

	public String getCameraNum(){
		return cameraNum;
	}

	public BusCameraAllow setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public Integer getBlock(){
		return block;
	}

	public BusCameraAllow setBlock(Integer block){
		this.block=block;
		return this;
	}

	public String getT1(){
		return t1;
	}

	public BusCameraAllow setT1(String t1){
		this.t1=t1;
		return this;
	}

	public String getT2(){
		return t2;
	}

	public BusCameraAllow setT2(String t2){
		this.t2=t2;
		return this;
	}

	public String getT3(){
		return t3;
	}

	public BusCameraAllow setT3(String t3){
		this.t3=t3;
		return this;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}
}
