package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name = "t_test_student")
public class TTestStudent extends ActiveRecord<TTestStudent> {
    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String AGE = "age";

    public static final String SEX = "sex";

    public static final String ADDRESS = "address";

    public static final String CREATETIME = "createtime";

    @Id
    private Integer id;

    @Column(name = "name", length = 50)
    private String name;
    private Integer age;
    private Integer sex;
    @Column(name = "address", length = 50)
    private String address;
    @Column(insertable = false, updatable = false)
    private Timestamp createtime;

    public Integer getId() {
        return id;
    }

    public TTestStudent setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public TTestStudent setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public TTestStudent setAge(Integer age) {
        this.age = age;
        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public TTestStudent setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public TTestStudent setAddress(String address) {
        this.address = address;
        return this;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public TTestStudent setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
        return this;
    }

}
