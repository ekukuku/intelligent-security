package com.campfire.intelligentsecurity.business.bean.api;

public class RLXXStruct implements ApiData {
    private String Name;
    private String Sex;
    private String Phone;
    private String Unit;
    private String Major_type;
    private String Issue_date;
    private String Driver_number;
    private int Driver_point;
    private String Pic;
    private int Flag;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getMajor_type() {
        return Major_type;
    }

    public void setMajor_type(String major_type) {
        Major_type = major_type;
    }

    public String getIssue_date() {
        return Issue_date;
    }

    public void setIssue_date(String issue_date) {
        Issue_date = issue_date;
    }

    public String getDriver_number() {
        return Driver_number;
    }

    public void setDriver_number(String driver_number) {
        Driver_number = driver_number;
    }

    public int getDriver_point() {
        return Driver_point;
    }

    public void setDriver_point(int driver_point) {
        Driver_point = driver_point;
    }

    public String getPic() {
        return Pic;
    }

    public void setPic(String pic) {
        Pic = pic;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }
}
