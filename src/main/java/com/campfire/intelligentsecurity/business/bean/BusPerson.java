package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 作业人员信息
 */
@Table(name="bus_person")
public class BusPerson extends ActiveRecord<BusPerson> {
	public static final String ID = "id";

	public static final String PNAME = "pname";

	public static final String SEX = "sex";

	public static final String UNIT = "unit";

	public static final String PERSON_TYPE = "person_type";

	public static final String DRIVER__TYPE = "driver__type";

	public static final String MAJOR_TYPE = "major_type";

	public static final String ISSUE_DATE = "issue_date";

	public static final String DRIVER_NUMBER = "driver_number";

	public static final String FACE_ID = "face_id";

	public static final String FACE_PATH = "face_path";

	public static final String FACE_RELPATH = "face_relpath";

	public static final String FACE_ABSPATH = "face_abspath";

	public static final String DRIVER_PATH = "driver_path";

	public static final String CREATE_BY = "create_by";

	public static final String CREATE_TIME = "create_time";

	public static final String UPDATE_BY = "update_by";

	public static final String UPDATE_TIME = "update_time";

	public static final String DEL_FLAG = "del_flag";

	public static final String REMARK = "remark";

	public static final String DRIVER_POINT = "driver_point";

	public static final String TYPE = "type";

	public static final String PHONE = "phone";

	/**
	 * 人员id
	 */
	@Id
	@Column(name="id", length=192)
	private String id;

	/**
	 * 姓名
	 */
	@Column(name="pname", length=765)
	private String pname;
	/**
	 * 性别
	 */
	@Column(name="sex", length=765)
	private String sex;
	/**
	 * 单位
	 */
	@Column(name="unit", length=765)
	private String unit;
	/**
	 * 人员类别
	 */
	@Column(name="person_type", length=765)
	private String personType;
	/**
	 * 驾照类别
	 */
	@Column(name="driver_type", length=765)
	private String driverType;
	/**
	 * 专业类别
	 */
	@Column(name="major_type", length=765)
	private String majorType;
	/**
	 * 签发日期
	 */
	@Column(name="issue_date")
	private Timestamp issueDate;
	/**
	 * 驾照编号
	 */
	@Column(name="driver_number", length=765)
	private String driverNumber;
	/**
	 * 人脸信息id
	 */
	@Column(name="face_id", length=600)
	private String faceId;
	/**
	 * 人脸照片地址
	 */
	@Column(name="face_path", length=3000)
	private String facePath;
	/**
	 * 人脸照相对路径
	 */
	@Column(name="face_relpath", length=3000)
	private String faceRelpath;
	/**
	 * 人脸照绝对路径
	 */
	@Column(name="face_abspath", length=3000)
	private String faceAbspath;
	/**
	 * 驾照地址
	 */
	@Column(name="driver_path", length=3000)
	private String driverPath;
	@Column(name="create_by", length=192,updatable = false)
	private String createBy;
	@Column(name="create_time",updatable = false, insertable = false)
	private Timestamp createTime;
	@Column(name="update_by", length=192,insertable = false)
	private String updateBy;
	@Column(name="update_time",insertable = false, updatable = false)
	private Timestamp updateTime;
	/**
	 * 删除标识符（0：正常  1：删除）
	 */
	@Column(name="del_flag", updatable = false,insertable = false)
	private String delFlag;
	/**
	 * 备注
	 */
	@Column(name="remark", length=600)
	private String remark;
	/**
	 * 驾照分
	 */
	@Column(name="driver_point")
	private Integer driverPoint;
	/**
	 * 员工类型(0内部员工,1外包员工)
	 */
	@Column(name="type", length=3)
	private String type;
	/**
	 * 手机号
	 */
	@Column(name="phone", length=33)
	private String phone;

	@Column(name="access_status")
	private String accessStatus;


	public String getId(){
		return id;
	}

	public BusPerson setId(String id){
		this.id=id;
		return this;
	}

	public String getPname(){
		return pname;
	}

	public BusPerson setPname(String pname){
		this.pname=pname;
		return this;
	}

	public String getSex(){
		return sex;
	}

	public BusPerson setSex(String sex){
		this.sex=sex;
		return this;
	}

	public String getUnit(){
		return unit;
	}

	public BusPerson setUnit(String unit){
		this.unit=unit;
		return this;
	}

	public String getPersonType(){
		return personType;
	}

	public BusPerson setPersonType(String personType){
		this.personType=personType;
		return this;
	}

	public String getDriverType(){
		return driverType;
	}

	public BusPerson setDriverType(String driverType){
		this.driverType=driverType;
		return this;
	}

	public String getMajorType(){
		return majorType;
	}

	public BusPerson setMajorType(String majorType){
		this.majorType=majorType;
		return this;
	}

	public Timestamp getIssueDate(){
		return issueDate;
	}

	public BusPerson setIssueDate(Timestamp issueDate){
		this.issueDate=issueDate;
		return this;
	}

	public String getDriverNumber(){
		return driverNumber;
	}

	public BusPerson setDriverNumber(String driverNumber){
		this.driverNumber=driverNumber;
		return this;
	}

	public String getFaceId(){
		return faceId;
	}

	public BusPerson setFaceId(String faceId){
		this.faceId=faceId;
		return this;
	}

	public String getFacePath(){
		return facePath;
	}

	public BusPerson setFacePath(String facePath){
		this.facePath=facePath;
		return this;
	}

	public String getFaceRelpath(){
		return faceRelpath;
	}

	public BusPerson setFaceRelpath(String faceRelpath){
		this.faceRelpath=faceRelpath;
		return this;
	}

	public String getFaceAbspath(){
		return faceAbspath;
	}

	public BusPerson setFaceAbspath(String faceAbspath){
		this.faceAbspath=faceAbspath;
		return this;
	}

	public String getDriverPath(){
		return driverPath;
	}

	public BusPerson setDriverPath(String driverPath){
		this.driverPath=driverPath;
		return this;
	}

	public String getCreateBy(){
		return createBy;
	}

	public BusPerson setCreateBy(String createBy){
		this.createBy=createBy;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public BusPerson setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public String getUpdateBy(){
		return updateBy;
	}

	public BusPerson setUpdateBy(String updateBy){
		this.updateBy=updateBy;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public BusPerson setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public BusPerson setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getRemark(){
		return remark;
	}

	public BusPerson setRemark(String remark){
		this.remark=remark;
		return this;
	}

	public Integer getDriverPoint(){
		return driverPoint;
	}

	public BusPerson setDriverPoint(Integer driverPoint){
		this.driverPoint=driverPoint;
		return this;
	}

	public String getType(){
		return type;
	}

	public BusPerson setType(String type){
		this.type=type;
		return this;
	}

	public String getPhone(){
		return phone;
	}

	public BusPerson setPhone(String phone){
		this.phone=phone;
		return this;
	}

	public String getAccessStatus() {
		return accessStatus;
	}

	public void setAccessStatus(String accessStatus) {
		this.accessStatus = accessStatus;
	}
}
