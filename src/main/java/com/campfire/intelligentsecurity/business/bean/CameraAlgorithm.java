package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="camera_algorithm")
public class CameraAlgorithm extends ActiveRecord<CameraAlgorithm> {
	public static final String CAMERA_NUM = "camera_num";

	public static final String ALGORITHM = "algorithm";

	public static final String CREATETIME = "createtime";


	@Column(name="camera_num", length=900)
	private String cameraNum;
	@Column(name="algorithm", length=765)
	private String algorithm;

	@Column(name="createtime", insertable = false)
	private Timestamp createtime;

	public String getCameraNum(){
		return cameraNum;
	}

	public CameraAlgorithm setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public String getAlgorithm(){
		return algorithm;
	}

	public CameraAlgorithm setAlgorithm(String algorithm){
		this.algorithm=algorithm;
		return this;
	}

	public Timestamp getCreatetime(){
		return createtime;
	}

	public CameraAlgorithm setCreatetime(Timestamp createtime){
		this.createtime=createtime;
		return this;
	}

}
