package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

@Table(name="t_sys_event")
public class TSysEvent extends ActiveRecord<TSysEvent> {
	public static final String ID = "id";

	public static final String EVENTNAME = "eventName";

	public static final String EVENTTYPE = "eventType";

	public static final String ICON = "icon";

	public static final String ALGORITHMID = "algorithmId";

	public static final String REMARK = "remark";

	public static final String ENABLE = "enable";

	public static final String CREATETIME = "createTime";

	public static final String UPDATETIME = "updateTime";

	/**
	 * 主键
	 */
	@Id
	private Integer id;

	/**
	 * 事件名称
	 */
	@Column(name="eventName", length=765)
	private String eventName;
	/**
	 * 事件分类
	 */
	private Integer eventType;
	/**
	 * 事件图标
	 */
	@Column(name="icon", length=765)
	private String icon;
	/**
	 * 算法id
	 */
	private Integer algorithmId;
	/**
	 * 备注
	 */
	@Column(name="remark", length=765)
	private String remark;
	/**
	 * 启用状态 0禁用 1启用
	 */
	private Byte enable;
	/**
	 * 录入时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp createTime;
	/**
	 * 更新时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp updateTime;

	private String algorithm;
	private String algorithmKey;
	private Byte status;

	public Integer getId(){
		return id;
	}

	public TSysEvent setId(Integer id){
		this.id=id;
		return this;
	}

	public String getEventName(){
		return eventName;
	}

	public TSysEvent setEventName(String eventName){
		this.eventName=eventName;
		return this;
	}

	public Integer getEventType(){
		return eventType;
	}

	public TSysEvent setEventType(Integer eventType){
		this.eventType=eventType;
		return this;
	}

	public String getIcon(){
		return icon;
	}

	public TSysEvent setIcon(String icon){
		this.icon=icon;
		return this;
	}

	public Integer getAlgorithmId(){
		return algorithmId;
	}

	public TSysEvent setAlgorithmId(Integer algorithmId){
		this.algorithmId=algorithmId;
		return this;
	}

	public String getRemark(){
		return remark;
	}

	public TSysEvent setRemark(String remark){
		this.remark=remark;
		return this;
	}

	public Byte getEnable(){
		return enable;
	}

	public TSysEvent setEnable(Byte enable){
		this.enable=enable;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TSysEvent setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public TSysEvent setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

	public String getAlgorithm(){
		return algorithm;
	}

	public TSysEvent setAlgorithm(String algorithm){
		this.algorithm=algorithm;
		return this;
	}

	public String getAlgorithmKey(){
		return algorithmKey;
	}

	public TSysEvent setAlgorithmKey(String algorithmKey){
		this.algorithmKey=algorithmKey;
		return this;
	}


	public Byte getStatus(){
		return status;
	}

	public TSysEvent setStatus(Byte status){
		this.status=status;
		return this;
	}
}
