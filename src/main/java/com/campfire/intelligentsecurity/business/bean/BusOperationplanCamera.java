package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 作业计划关联摄像头
 */
@Table(name="bus_operationplan_camera")
public class BusOperationplanCamera extends ActiveRecord<BusOperationplanCamera> {
	public static final String ID = "id";

	public static final String OPERATION_ID = "operation_id";

	public static final String CAMERA_ID = "camera_id";

	public static final String CAMERA_TYPE = "camera_type";

	public static final String CAMERA_NUM = "camera_num";

	public static final String CAMERA_NAME = "camera_name";

	/**
	 * ID
	 */
	@Id
	@Column(name="id", length=192)
	private String id;

	/**
	 * 作业ID
	 */
	@Column(name="operation_id", length=192)
	private String operationId;
	/**
	 * 摄像头ID
	 */
	@Column(name="camera_id", length=192)
	private String cameraId;
	/**
	 * 摄像头0普通摄像头关联camera_config 1海康监控点编号获取rtsp
	 */
	@Column(name="camera_type", length=192)
	private String cameraType;
	/**
	 * 摄像头编号
	 */
	@Column(name="camera_num", length=600)
	private String cameraNum;
	/**
	 * 摄像头名称
	 */
	@Column(name="camera_name", length=765)
	private String cameraName;

	public String getId(){
		return id;
	}

	public BusOperationplanCamera setId(String id){
		this.id=id;
		return this;
	}

	public String getOperationId(){
		return operationId;
	}

	public BusOperationplanCamera setOperationId(String operationId){
		this.operationId=operationId;
		return this;
	}

	public String getCameraId(){
		return cameraId;
	}

	public BusOperationplanCamera setCameraId(String cameraId){
		this.cameraId=cameraId;
		return this;
	}

	public String getCameraType(){
		return cameraType;
	}

	public BusOperationplanCamera setCameraType(String cameraType){
		this.cameraType=cameraType;
		return this;
	}

	public String getCameraNum(){
		return cameraNum;
	}

	public BusOperationplanCamera setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public String getCameraName(){
		return cameraName;
	}

	public BusOperationplanCamera setCameraName(String cameraName){
		this.cameraName=cameraName;
		return this;
	}

}
