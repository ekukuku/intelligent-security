package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 监控设备轮巡免检计划
 */
@Table(name="bus_camera_plan")
public class BusCameraPlan extends ActiveRecord<BusCameraPlan> {
	public static final String ID = "id";

	public static final String CAMERA_NUM = "camera_num";

	public static final String BEGIN = "`begin`";

	public static final String END = "`end`";

	public static final String T1 = "t1";

	public static final String T2 = "t2";

	public static final String T3 = "t3";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 摄像头编号
	 */
	@Column(name="camera_num", length=900)
	private String cameraNum;
	/**
	 * 免检开始时间
	 */
	@Column(name="`begin`")
	private Timestamp begin;
	/**
	 * 免检结束时间
	 */
	@Column(name="`end`")
	private Timestamp end;
	/**
	 * 预留1
	 */
	@Column(name="t1", length=384)
	private String t1;
	/**
	 * 预留2
	 */
	@Column(name="t2", length=384)
	private String t2;
	/**
	 * 预留3
	 */
	@Column(name="t3", length=384)
	private String t3;

	@Column(insertable = false,updatable = false)
	private String select;

	public Long getId(){
		return id;
	}

	public BusCameraPlan setId(Long id){
		this.id=id;
		return this;
	}

	public String getCameraNum(){
		return cameraNum;
	}

	public BusCameraPlan setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public Timestamp getBegin(){
		return begin;
	}

	public BusCameraPlan setBegin(Timestamp begin){
		this.begin=begin;
		return this;
	}

	public Timestamp getEnd(){
		return end;
	}

	public BusCameraPlan setEnd(Timestamp end){
		this.end=end;
		return this;
	}

	public String getT1(){
		return t1;
	}

	public BusCameraPlan setT1(String t1){
		this.t1=t1;
		return this;
	}

	public String getT2(){
		return t2;
	}

	public BusCameraPlan setT2(String t2){
		this.t2=t2;
		return this;
	}

	public String getT3(){
		return t3;
	}

	public BusCameraPlan setT3(String t3){
		this.t3=t3;
		return this;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}
}
