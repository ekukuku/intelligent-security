package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="t_offline_info")
public class TOfflineInfo extends ActiveRecord<TOfflineInfo> {
	public static final String ID = "id";

	public static final String VNAME = "vname";

	public static final String EVENT = "event";

	public static final String URL = "url";

	public static final String STATUS = "status";

	public static final String CREATETIME = "createTime";

	public static final String SF = "sf";

	public static final String LEN = "len";

	public static final String SIZE = "`size`";

	/**
	 * 主键
	 */
	@Id
	private Integer id;

	/**
	 * 视频名称
	 */
	@Column(name="vname", length=765)
	private String vname;
	/**
	 * 违章事件
	 */
	@Column(name="event", length=765)
	private String event;
	/**
	 * 视频地址
	 */
	@Column(name="url", length=1500)
	private String url;
	/**
	 * 状态
	 */
	@Column(insertable = false)
	private Integer status;
	/**
	 * 录入时间
	 */
	@Column(insertable = false,updatable = false)
	private Timestamp createTime;
	/**
	 * 算法
	 */
	@Column(name="sf", length=765)
	private String sf;
	/**
	 * 视频长度（分钟）
	 */
	@Column(name="len", length=765)
	private String len;
	/**
	 * 视频大小（MB）
	 */
	@Column(name="`size`", length=765)
	private String size;

	public Integer getId(){
		return id;
	}

	public TOfflineInfo setId(Integer id){
		this.id=id;
		return this;
	}

	public String getVname(){
		return vname;
	}

	public TOfflineInfo setVname(String vname){
		this.vname=vname;
		return this;
	}

	public String getEvent(){
		return event;
	}

	public TOfflineInfo setEvent(String event){
		this.event=event;
		return this;
	}

	public String getUrl(){
		return url;
	}

	public TOfflineInfo setUrl(String url){
		this.url=url;
		return this;
	}

	public Integer getStatus(){
		return status;
	}

	public TOfflineInfo setStatus(Integer status){
		this.status=status;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TOfflineInfo setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public String getSf(){
		return sf;
	}

	public TOfflineInfo setSf(String sf){
		this.sf=sf;
		return this;
	}

	public String getLen(){
		return len;
	}

	public TOfflineInfo setLen(String len){
		this.len=len;
		return this;
	}

	public String getSize(){
		return size;
	}

	public TOfflineInfo setSize(String size){
		this.size=size;
		return this;
	}

}
