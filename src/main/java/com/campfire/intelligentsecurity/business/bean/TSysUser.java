package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

@Table(name="t_sys_user")
public class TSysUser extends ActiveRecord<TSysUser> {
	public static final String ID = "id";

	public static final String USERNAME = "username";

	public static final String USERCODE = "usercode";

	public static final String PASSWORD = "password";

	public static final String ENABLE = "enable";

	public static final String DELABLE = "delable";

	public static final String CREATETIME = "createTime";

	public static final String UPDATETIME = "updateTime";

	/**
	 * 主键
	 */
	@Id
	private Integer id;

	/**
	 * 用户名称
	 */
	@Column(name="username", length=300)
	private String username;
	/**
	 * 登录名
	 */
	@Column(name="usercode", length=300)
	private String usercode;
	/**
	 * 密码
	 */
	@Column(name="password", length=300)
	private String password;
	/**
	 * 状态 0禁用 1启用
	 */
	private Byte enable;
	/**
	 * 是否可删除 0不可删除 1可删除
	 */
	@Column(insertable = false, updatable = false)
	private Byte delable;
	/**
	 * 创建时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp createTime;
	/**
	 * 更新时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp updateTime;

	public Integer getId(){
		return id;
	}

	public TSysUser setId(Integer id){
		this.id=id;
		return this;
	}

	public String getUsername(){
		return username;
	}

	public TSysUser setUsername(String username){
		this.username=username;
		return this;
	}

	public String getUsercode(){
		return usercode;
	}

	public TSysUser setUsercode(String usercode){
		this.usercode=usercode;
		return this;
	}

	public String getPassword(){
		return password;
	}

	public TSysUser setPassword(String password){
		this.password=password;
		return this;
	}

	public Byte getEnable(){
		return enable;
	}

	public TSysUser setEnable(Byte enable){
		this.enable=enable;
		return this;
	}

	public Byte getDelable(){
		return delable;
	}

	public TSysUser setDelable(Byte delable){
		this.delable=delable;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TSysUser setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public TSysUser setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

}
