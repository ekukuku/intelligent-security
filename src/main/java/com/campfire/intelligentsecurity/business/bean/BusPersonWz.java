package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="bus_person_wz")
public class BusPersonWz extends ActiveRecord<BusPersonWz> {
	public static final String ID = "id";

	public static final String PERSONID = "personId";

	public static final String POINTINFO = "pointInfo";

	public static final String WZPOINT = "wzPoint";

	public static final String WZIMAGE = "wzImage";

	public static final String WZTIME = "wzTime";

	public static final String CREATETIME = "createTime";

	public static final String POINTID = "pointId";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 人员id
	 */
	@Column(name="personId")
	private String personId;
	/**
	 * 违章信息
	 */
	@Column(name="pointInfo", length=1500)
	private String pointInfo;
	/**
	 * 违章分数
	 */
	@Column(name="wzPoint", length=192)
	private String wzPoint;
	/**
	 * 违章截图
	 */
	@Column(name="wzImage", length=765)
	private String wzImage;
	/**
	 * 违章时间
	 */
	private Timestamp wzTime;
	/**
	 * 创建时间
	 */
	private Timestamp createTime;
	private Integer pointId;

	public Long getId(){
		return id;
	}

	public BusPersonWz setId(Long id){
		this.id=id;
		return this;
	}

	public String getPersonId(){
		return personId;
	}

	public BusPersonWz setPersonId(String personId){
		this.personId=personId;
		return this;
	}

	public String getPointInfo(){
		return pointInfo;
	}

	public BusPersonWz setPointInfo(String pointInfo){
		this.pointInfo=pointInfo;
		return this;
	}

	public String getWzPoint(){
		return wzPoint;
	}

	public BusPersonWz setWzPoint(String wzPoint){
		this.wzPoint=wzPoint;
		return this;
	}

	public String getWzImage(){
		return wzImage;
	}

	public BusPersonWz setWzImage(String wzImage){
		this.wzImage=wzImage;
		return this;
	}

	public Timestamp getWzTime(){
		return wzTime;
	}

	public BusPersonWz setWzTime(Timestamp wzTime){
		this.wzTime=wzTime;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public BusPersonWz setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public Integer getPointId(){
		return pointId;
	}

	public BusPersonWz setPointId(Integer pointId){
		this.pointId=pointId;
		return this;
	}
}
