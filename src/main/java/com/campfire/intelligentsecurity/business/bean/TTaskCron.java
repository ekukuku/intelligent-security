package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

@Table(name="t_task_cron")
public class TTaskCron extends ActiveRecord<TTaskCron> {
	public static final String ID = "id";

	public static final String TASK = "task";

	public static final String TIME = "`time`";

	public static final String STATUS = "status";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * task名称
	 */
	@Column(name="task", length=192)
	private String task;


	@Column(name = "pl")
	private String pl;
	/**
	 * task时间
	 */
	@Column(name="`time`")
	private Long time;

	@Column(name="cron", length=384)
	private String cron;
	/**
	 * task状态
	 */
	@Column(name="status", length=192)
	private String status;

	@Column(name="flag",updatable = false)
	private Integer flag;

	public Long getId(){
		return id;
	}

	public TTaskCron setId(Long id){
		this.id=id;
		return this;
	}

	public String getTask(){
		return task;
	}

	public TTaskCron setTask(String task){
		this.task=task;
		return this;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getStatus(){
		return status;
	}

	public TTaskCron setStatus(String status){
		this.status=status;
		return this;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
}
