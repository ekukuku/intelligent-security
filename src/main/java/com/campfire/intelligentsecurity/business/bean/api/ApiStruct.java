package com.campfire.intelligentsecurity.business.bean.api;

import cn.hutool.json.JSONUtil;

import java.util.List;

public class ApiStruct {
    //类型ID
    private int msgType;
    //消息内容json对象
    private String data;
    //自动构建消息结构体对象
    private ApiData dataBean;

    private List<RYCRJSStruct> dataList;

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ApiData getDataBean() {
        return dataBean;
    }

    public List<RYCRJSStruct> getDataList() {
        return dataList;
    }

    public void setDataList(List<RYCRJSStruct> dataList) {
        this.dataList = dataList;
    }

    public ApiType apiType() {
        switch (msgType) {
            case 101:
                return ApiType.RLLRTS;
            case 102:
                return ApiType.RLLRJGFH;
            case 103:
                this.dataBean = JSONUtil.toBean(this.data, RLXXStruct.class);
                return ApiType.RLXXJS;
            case 201:
                return ApiType.GZJHTS;
            case 203:
                this.dataBean = JSONUtil.toBean(this.data, JKWCStruct.class);
                return ApiType.SPSBWC;
            case 301:
                this.dataBean = JSONUtil.toBean(this.data, WZXXStruct.class);
                return ApiType.WZXXJS;
            case 302:
                this.dataBean = JSONUtil.toBean(this.data, RLSBXXJSStruct.class);
                return ApiType.RLSBXXJS;
            case 204:
                this.dataBean = JSONUtil.toBean(this.data, GETSTEAMStruct.class);
                return  ApiType.GETSTEAM;
            case 205:
                return  ApiType.RYCRTS;
            case 303:
                this.dataList = JSONUtil.toList(this.data,RYCRJSStruct.class);
                return  ApiType.RYCRJS;
            default:
                return ApiType.BAD;
        }
    }

    public enum ApiType {
        RLLRTS(101, "人脸录入推送"), RLLRJGFH(102, "人脸录入结果返回"), RLXXJS(103, "人脸信息接收"),
        GZJHTS(201, "工作计划推送"), WZXXJS(301, "违章消息接收"), RLSBXXJS(302, "人脸识别信息接收"),
        SPSBWC(203, "视频识别完成"), BAD(400, "错误的消息类型"),GETSTEAM(204,"获取视频流地址"),
        RYCRTS(205,"人员闯入推送"), RYCRJS(303,"人员闯入接收");

        private String name;
        private int id;

        ApiType(int id, String name) {
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
