package com.campfire.intelligentsecurity.business.bean.camera;

public enum CameraEvent {
    ZOOM_IN_START(770, "缩小"), ZOOM_IN_STOP(769, "缩小停止"),
    ZOOM_OUT_START(772, "放大"), ZOOM_OUT_STOP(771, "放大停止"),
    AV_ADD_START(259,"光圈开"), AV_ADD_STOP(260,"光圈开停止"),
    AV_SUB_START(258,"光圈关"), AV_SUB_STOP(257,"光圈关停止"),
    FOCUS_ADD_START(514,"近聚焦"), FOCUS_ADD_STOP(515,"远聚焦停止"),
    FOCUS_SUB_START(516,"远聚焦"), FOCUS_SUB_STOP(513,"近聚焦停止"),
    TOP_START(1026, "向上"), TOP_STOP(1025, "向上停止"),
    BOT_START(1028, "向下"), BOT_STOP(1027, "向下停止"),
    LEFT_START(1284, "左转"), LEFT_STOP(1283, "左转停止"),
    RIGHT_START(1282, "右转"), RIGHT_STOP(1281, "右转停止"),
    STOP_ALL(2035, "停止动作");
    private int eventid;
    private String name;

    CameraEvent(int eventid, String name) {
        this.eventid = eventid;
        this.name = name;
    }

    public int getEventid() {
        return eventid;
    }

    public void setEventid(int eventid) {
        this.eventid = eventid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
