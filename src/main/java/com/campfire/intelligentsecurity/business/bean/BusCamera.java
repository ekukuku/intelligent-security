package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

/**
 * 海康威视监控点摄像头配置
 */
@Table(name="bus_camera")
public class BusCamera extends ActiveRecord<BusCamera> {
	public static final String ID = "id";

	public static final String CAMERA_NAME = "camera_name";

	public static final String CAMERA_NUM = "camera_num";

	public static final String CAMERA_CODE = "camera_code";

	public static final String CREATE_TIME = "create_time";

	public static final String UPDATE_TIME = "update_time";

	public static final String DEL_FLAG = "del_flag";

	public static final String CAMERA_TYPE = "camera_type";

	public static final String REMARK = "remark";

	public static final String STREAM = "stream";

	public static final String YYLY = "yyly";

	public static final String SUBSTATION = "substation";

	/**
	 * 摄像头ID
	 */
	@Id
	@Column(name="id", insertable = false, updatable = false)
	private String id;

	/**
	 * 摄像头设备名称
	 */
	@Column(name="camera_name", length=900)
	private String cameraName;
	/**
	 * 摄像头编号
	 */
	@Column(name="camera_num", length=900)
	private String cameraNum;
	/**
	 * 监控点编号
	 */
	@Column(name="camera_code", length=765)
	private String cameraCode;
	/**
	 * 创建日期
	 */
	@Column(name="create_time", insertable = false, updatable = false)
	private Timestamp createTime;
	/**
	 * 更新日期
	 */
	@Column(name="update_time", insertable = false, updatable = false)
	private Timestamp updateTime;
	/**
	 * 删除标记（0：正常；1：删除；）
	 */
	@Column(name="del_flag", length=3)
	private String delFlag;
	/**
	 * 摄像头类型 1--安全帽/移动布控球，3--公网摄像头
	 */
	@Column(name="camera_type", length=3)
	private String cameraType;
	@Column(name="stream", length=1000)
	private String stream;
	@Column(name="remark", length=765)
	private String remark;

	@Column(name="yyly")
	private String yyly;

	@Column(name="substation")
	private String substation;

	public String getId(){
		return id;
	}

	public BusCamera setId(String id){
		this.id=id;
		return this;
	}

	public String getCameraName(){
		return cameraName;
	}

	public BusCamera setCameraName(String cameraName){
		this.cameraName=cameraName;
		return this;
	}

	public String getCameraNum(){
		return cameraNum;
	}

	public BusCamera setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public String getCameraCode(){
		return cameraCode;
	}

	public BusCamera setCameraCode(String cameraCode){
		this.cameraCode=cameraCode;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public BusCamera setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public String getStream(){
		return stream;
	}

	public BusCamera setStream(String stream){
		this.stream=stream;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public BusCamera setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public BusCamera setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getCameraType(){
		return cameraType;
	}

	public BusCamera setCameraType(String cameraType){
		this.cameraType=cameraType;
		return this;
	}

	public String getRemark(){
		return remark;
	}

	public BusCamera setRemark(String remark){
		this.remark=remark;
		return this;
	}

	public String getYyly() {
		return yyly;
	}

	public void setYyly(String yyly) {
		this.yyly = yyly;
	}

	public String getSubstation() {
		return substation;
	}

	public void setSubstation(String substation) {
		this.substation = substation;
	}
}
