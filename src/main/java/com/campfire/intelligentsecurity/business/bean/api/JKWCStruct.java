package com.campfire.intelligentsecurity.business.bean.api;

public class JKWCStruct implements ApiData {

    //摄像头编号
    private String CAMERA_CODE;

    public String getCAMERA_CODE() {
        return CAMERA_CODE;
    }

    public void setCAMERA_CODE(String CAMERA_CODE) {
        this.CAMERA_CODE = CAMERA_CODE;
    }

}
