package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name = "t_ticket_info")
public class TicketInfo extends ActiveRecord<TicketInfo> {
    public static final String ID = "id";

    public static final String JOBCONTENT = "jobContent";

    public static final String JOBLOCATION = "jobLocation";

    public static final String STARTTIME = "startTime";

    public static final String ENDTIME = "endTime";

    public static final String CREATETIME = "createTime";

    public static final String RESULT = "result";

    public static final String NAME = "name";

    public static final String URL = "url";

    public static final String MASTER = "master";

    public static final String WORKERS = "workers";

    public static final String OPERATIONPLAN = "operationplan";


    public TicketInfo() {
    }


    public TicketInfo(String result) {
        this.result = result;
    }

    public TicketInfo(String result, String name, String url) {
        this.result = result;
        this.name = name;
        this.url = url;
    }

    public TicketInfo(String result, String name, String url, String master, String workers) {
        this.result = result;
        this.name = name;
        this.url = url;
        this.master = master;
        this.workers = workers;
    }

    public TicketInfo(String result, String name, String url, String master, String workers, String operationplan) {
        this.result = result;
        this.name = name;
        this.url = url;
        this.master = master;
        this.workers = workers;
        this.operationplan = operationplan;
    }

    /**
     * 主键
     */
    @Id
    @Column(name = "id", insertable = false, updatable = false)
    private String id;

    /**
     * 工作内容
     */
    @Column(name = "jobContent")
    private String jobContent;

    /**
     * 工作地点
     */
    @Column(name = "jobLocation")
    private String jobLocation;

    /**
     * 计划开始时间
     */
    private Timestamp startTime;

    /**
     * 计划结束时间
     */
    private Timestamp endTime;

    /**
     * 识别结果
     */
    @Column(name = "result")
    private String result;

    /**
     * 图片名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 图片路径
     */
    @Column(name = "url")
    private String url;

    @Column(name = "master")
    private String master;

    @Column(name = "workers")
    private String workers;

    @Column(name = "operationplan")
    private String operationplan;

    public static String getID() {
        return ID;
    }

    public static String getJOBCONTENT() {
        return JOBCONTENT;
    }

    public static String getJOBLOCATION() {
        return JOBLOCATION;
    }

    public static String getSTARTTIME() {
        return STARTTIME;
    }

    public static String getENDTIME() {
        return ENDTIME;
    }

    public static String getCREATETIME() {
        return CREATETIME;
    }

    public static String getRESULT() {
        return RESULT;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getURL() {
        return URL;
    }

    public static String getMASTER() {
        return MASTER;
    }

    public static String getWORKERS() {
        return WORKERS;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobContent() {
        return jobContent;
    }

    public void setJobContent(String jobContent) {
        this.jobContent = jobContent;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getWorkers() {
        return workers;
    }

    public void setWorkers(String workers) {
        this.workers = workers;
    }

    public static String getOPERATIONPLAN() {
        return OPERATIONPLAN;
    }

    public String getOperationplan() {
        return operationplan;
    }

    public void setOperationplan(String operationplan) {
        this.operationplan = operationplan;
    }
}
