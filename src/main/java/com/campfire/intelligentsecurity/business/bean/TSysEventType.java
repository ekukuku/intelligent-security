package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;
import java.sql.Timestamp;

@Table(name="t_sys_event_type")
public class TSysEventType extends ActiveRecord<TSysEventType> {
	public static final String ID = "id";

	public static final String TYPENAME = "typeName";

	public static final String REMARK = "remark";

	public static final String ENABLE = "enable";

	public static final String CREATETIME = "createTime";

	public static final String UPDATETIME = "updateTime";

	/**
	 * 主键
	 */
	@Id
	private Integer id;

	/**
	 * 分类名称
	 */
	@Column(name="typeName", length=765)
	private String typeName;
	/**
	 * 备注
	 */
	@Column(name="remark", length=765)
	private String remark;
	/**
	 * 启用状态 0禁用 1启用
	 */
	private Byte enable;
	/**
	 * 录入时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp createTime;
	/**
	 * 更新时间
	 */
	@Column(insertable = false, updatable = false)
	private Timestamp updateTime;

	public Integer getId(){
		return id;
	}

	public TSysEventType setId(Integer id){
		this.id=id;
		return this;
	}

	public String getTypeName(){
		return typeName;
	}

	public TSysEventType setTypeName(String typeName){
		this.typeName=typeName;
		return this;
	}

	public String getRemark(){
		return remark;
	}

	public TSysEventType setRemark(String remark){
		this.remark=remark;
		return this;
	}

	public Byte getEnable(){
		return enable;
	}

	public TSysEventType setEnable(Byte enable){
		this.enable=enable;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TSysEventType setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public TSysEventType setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

}
