package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 作业计划
 */
@Table(name="bus_operationplan")
public class BusOperationplan extends ActiveRecord<BusOperationplan> {
	public static final String ID = "ID";

	public static final String TEAM = "TEAM";

	public static final String COORDINATE = "COORDINATE";

	public static final String PEOPLE_NUM = "PEOPLE_NUM";

	public static final String IS_ELECTRIFIED = "IS_ELECTRIFIED";

	public static final String JOB_CONTENT = "JOB_CONTENT";

	public static final String WORKING_DAYS = "WORKING_DAYS";

	public static final String START_TIME = "START_TIME";

	public static final String END_TIME = "END_TIME";

	public static final String WORKING_TYPE = "WORKING_TYPE";

	public static final String MAJOR_TYPE = "MAJOR_TYPE";

	public static final String OPERATIONAL_RISK_LEVEL = "OPERATIONAL_RISK_LEVEL";

	public static final String POWER_RISK_LEVEL = "POWER_RISK_LEVEL";

	public static final String VOLTAGE_LEVEL = "VOLTAGE_LEVEL";

	public static final String OPERATION_CHARGE_PERSON = "OPERATION_CHARGE_PERSON";

	public static final String OPERATION_CHARGE_PERSON_ID = "OPERATION_CHARGE_PERSON_ID";

	public static final String OPERATION_CHARGE_PERSON_PHONE = "OPERATION_CHARGE_PERSON_PHONE";

	public static final String STATUS = "STATUS";

	public static final String CREATE_BY = "CREATE_BY";

	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String UPDATE_BY = "UPDATE_BY";

	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String DEL_FLAG = "DEL_FLAG";

	public static final String REMARKS = "REMARKS";

	public static final String OPERATION_LOCATION = "OPERATION_LOCATION";

	public static final String CONSTRUCTION_MANAGEMENT_UNIT = "CONSTRUCTION_MANAGEMENT_UNIT";

	public static final String CONSTRUCTION_UNIT = "CONSTRUCTION_UNIT";

	public static final String CONSTRUCTION_UNIT_TYPE = "CONSTRUCTION_UNIT_TYPE";

	public static final String MAIN_UNIT_NUM = "MAIN_UNIT_NUM";

	public static final String OUTSOURCING_UNIT_NUM = "OUTSOURCING_UNIT_NUM";

	public static final String SUPERVISE_UNIT_NUM = "SUPERVISE_UNIT_NUM";

	public static final String SAFETY_CHARGE_PERSON = "SAFETY_CHARGE_PERSON";

	public static final String SAFETY_CHARGE_PERSON_ID = "SAFETY_CHARGE_PERSON_ID";

	public static final String SAFETY_CHARGE_PERSON_PHONE = "SAFETY_CHARGE_PERSON_PHONE";

	public static final String VIOLATION = "VIOLATION";

	public static final String INTEGRITY = "INTEGRITY";

	public static final String ERR_MSG = "ERR_MSG";

	@Id
	@Column(name="ID", length=192)
	private String id;

	/**
	 * 班组
	 */
	@Column(name="TEAM", length=765)
	private String team;
	/**
	 * 工作地点坐标
	 */
	@Column(name="COORDINATE", length=765)
	private String coordinate;
	/**
	 * 工作总人数
	 */
	@Column(name="PEOPLE_NUM", length=765)
	private String peopleNum;
	/**
	 * 是否带电
	 */
	@Column(name="IS_ELECTRIFIED", length=765)
	private String isElectrified;
	/**
	 * 工作内容
	 */
	@Column(name="JOB_CONTENT", length=765)
	private String jobContent;
	/**
	 * 工作天数
	 */
	@Column(name="WORKING_DAYS", length=765)
	private String workingDays;
	/**
	 * 工作开始时间
	 */
	@Column(name="START_TIME")
	private Timestamp startTime;
	/**
	 * 工作结束时间
	 */
	@Column(name="END_TIME")
	private Timestamp endTime;
	/**
	 * 工作类型
	 */
	@Column(name="WORKING_TYPE", length=765)
	private String workingType;
	/**
	 * 专业类型
	 */
	@Column(name="MAJOR_TYPE", length=765)
	private String majorType;
	/**
	 * 作业风险等级
	 */
	@Column(name="OPERATIONAL_RISK_LEVEL", length=765)
	private String operationalRiskLevel;
	/**
	 * 电网风险等级
	 */
	@Column(name="POWER_RISK_LEVEL", length=765)
	private String powerRiskLevel;
	/**
	 * 电压等级
	 */
	@Column(name="VOLTAGE_LEVEL", length=765)
	private String voltageLevel;
	/**
	 * 工作负责人
	 */
	@Column(name="OPERATION_CHARGE_PERSON", length=765)
	private String operationChargePerson;
	/**
	 * 工作负责人ID
	 */
	@Column(name="OPERATION_CHARGE_PERSON_ID", length=765)
	private String operationChargePersonId;
	/**
	 * 工作负责人手机号
	 */
	@Column(name="OPERATION_CHARGE_PERSON_PHONE", length=765)
	private String operationChargePersonPhone;
	/**
	 * 作业状态 0未开始 1进行中 2已结束
	 */
	@Column(name="STATUS", length=765)
	private String status;
	@Column(name="CREATE_BY", length=192,updatable = false)
	private String createBy;
	@Column(name="CREATE_DATE", insertable = false,updatable = false)
	private Timestamp createDate;
	@Column(name="UPDATE_BY", length=192)
	private String updateBy;
	@Column(name="UPDATE_DATE",insertable = false,updatable = true)
	private Timestamp updateDate;
	/**
	 * 删除标识符（0：正常  1：删除）
	 */
	@Column(name="DEL_FLAG")
	private String delFlag;
	/**
	 * 备注
	 */
	@Column(name="REMARKS", length=600)
	private String remarks;
	/**
	 * 作业地点
	 */
	@Column(name="OPERATION_LOCATION", length=765)
	private String operationLocation;
	/**
	 * 建设管理单位
	 */
	@Column(name="CONSTRUCTION_MANAGEMENT_UNIT", length=765)
	private String constructionManagementUnit;
	/**
	 * 施工单位
	 */
	@Column(name="CONSTRUCTION_UNIT", length=765)
	private String constructionUnit;
	/**
	 * 施工单位类别
	 */
	@Column(name="CONSTRUCTION_UNIT_TYPE", length=765)
	private String constructionUnitType;
	/**
	 * 主业单位人数
	 */
	@Column(name="MAIN_UNIT_NUM", length=765)
	private String mainUnitNum;
	/**
	 * 外包单位人数
	 */
	@Column(name="OUTSOURCING_UNIT_NUM", length=765)
	private String outsourcingUnitNum;
	/**
	 * 直属和省管单位人数
	 */
	@Column(name="SUPERVISE_UNIT_NUM", length=765)
	private String superviseUnitNum;
	/**
	 * 安全监护人
	 */
	@Column(name="SAFETY_CHARGE_PERSON", length=765)
	private String safetyChargePerson;
	/**
	 * 安全监护人ID
	 */
	@Column(name="SAFETY_CHARGE_PERSON_ID", length=765)
	private String safetyChargePersonId;
	/**
	 * 安全监护人手机号
	 */
	@Column(name="SAFETY_CHARGE_PERSON_PHONE", length=765)
	private String safetyChargePersonPhone;

	/**
	 * 专责监护人
	 */
	@Column(name="SPEICAL_PERSON", length=765)
	private String speicalPerson;
	/**
	 * 专责监护人ID
	 */
	@Column(name="SPEICAL_ID", length=765)
	private String speicalId;
	/**
	 * 专责监护人手机号
	 */
	@Column(name="SPEICAL_PERSON_PHONE", length=765)
	private String speicalPersonPhone;

	/**
	 * 关联违章
	 */
	@Column(name="VIOLATION", length=765)
	private String violation;
	/**
	 * 批量导入数据完整性
	 */
	@Column(name="INTEGRITY", length=765)
	private String integrity;
	/**
	 * 批量导入异常数据提示
	 */
	@Column(name="ERR_MSG", length=3000)
	private String errMsg;

	@Column(insertable = false,updatable = false)
	private String sfdd;
	@Column(insertable = false,updatable = false)
	private String zyryid;
	@Column(insertable = false,updatable = false)
	private String aqjdr;
	@Column(insertable = false,updatable = false)
	private String zzjhr;
	@Column(insertable = false,updatable = false)
	private String gzfzr;
	@Column(insertable = false,updatable = false)
	private String zyfxdj;
	@Column(insertable = false,updatable = false)
	private String dwfxdj;
	@Column(insertable = false,updatable = false)
	private String zyzt;
	@Column(insertable = false,updatable = false)
	private String sxtsl;
	@Column(insertable = false,updatable = false)
	private String zysbjk;

	@Column(insertable = false,updatable = false)
	private String wgCount;
	@Column(insertable = false,updatable = false)
	private String dqrwgCount;

	@Column(insertable = false,updatable = false)
	private String cameras;
	@Column(insertable = false,updatable = false)
	private String cameras_name;
	@Column(insertable = false,updatable = false)
	private String algorithms;

	public String getWgCount() {
		return wgCount;
	}

	public void setWgCount(String wgCount) {
		this.wgCount = wgCount;
	}

	public String getDqrwgCount() {
		return dqrwgCount;
	}

	public void setDqrwgCount(String dqrwgCount) {
		this.dqrwgCount = dqrwgCount;
	}

	public String getZysbjk() {
		return zysbjk;
	}

	public void setZysbjk(String zysbjk) {
		this.zysbjk = zysbjk;
	}

	public String getSfdd() {
		return sfdd;
	}

	public void setSfdd(String sfdd) {
		this.sfdd = sfdd;
	}

	public String getZyryid() {
		return zyryid;
	}

	public void setZyryid(String zyryid) {
		this.zyryid = zyryid;
	}

	public String getAqjdr() {
		return aqjdr;
	}

	public void setAqjdr(String aqjdr) {
		this.aqjdr = aqjdr;
	}

	public String getZzjhr() {
		return zzjhr;
	}

	public void setZzjhr(String zzjhr) {
		this.zzjhr = zzjhr;
	}

	public String getGzfzr() {
		return gzfzr;
	}

	public void setGzfzr(String gzfzr) {
		this.gzfzr = gzfzr;
	}

	public String getZyfxdj() {
		return zyfxdj;
	}

	public void setZyfxdj(String zyfxdj) {
		this.zyfxdj = zyfxdj;
	}

	public String getDwfxdj() {
		return dwfxdj;
	}

	public void setDwfxdj(String dwfxdj) {
		this.dwfxdj = dwfxdj;
	}

	public String getZyzt() {
		return zyzt;
	}

	public void setZyzt(String zyzt) {
		this.zyzt = zyzt;
	}

	public String getSxtsl() {
		return sxtsl;
	}

	public void setSxtsl(String sxtsl) {
		this.sxtsl = sxtsl;
	}

	public String getId(){
		return id;
	}

	public BusOperationplan setId(String id){
		this.id=id;
		return this;
	}

	public String getTeam(){
		return team;
	}

	public BusOperationplan setTeam(String team){
		this.team=team;
		return this;
	}

	public String getCoordinate(){
		return coordinate;
	}

	public BusOperationplan setCoordinate(String coordinate){
		this.coordinate=coordinate;
		return this;
	}

	public String getPeopleNum(){
		return peopleNum;
	}

	public BusOperationplan setPeopleNum(String peopleNum){
		this.peopleNum=peopleNum;
		return this;
	}

	public String getIsElectrified(){
		return isElectrified;
	}

	public BusOperationplan setIsElectrified(String isElectrified){
		this.isElectrified=isElectrified;
		return this;
	}

	public String getJobContent(){
		return jobContent;
	}

	public BusOperationplan setJobContent(String jobContent){
		this.jobContent=jobContent;
		return this;
	}

	public String getWorkingDays(){
		return workingDays;
	}

	public BusOperationplan setWorkingDays(String workingDays){
		this.workingDays=workingDays;
		return this;
	}

	public Timestamp getStartTime(){
		return startTime;
	}

	public BusOperationplan setStartTime(Timestamp startTime){
		this.startTime=startTime;
		return this;
	}

	public Timestamp getEndTime(){
		return endTime;
	}

	public BusOperationplan setEndTime(Timestamp endTime){
		this.endTime=endTime;
		return this;
	}

	public String getWorkingType(){
		return workingType;
	}

	public BusOperationplan setWorkingType(String workingType){
		this.workingType=workingType;
		return this;
	}

	public String getMajorType(){
		return majorType;
	}

	public BusOperationplan setMajorType(String majorType){
		this.majorType=majorType;
		return this;
	}

	public String getOperationalRiskLevel(){
		return operationalRiskLevel;
	}

	public BusOperationplan setOperationalRiskLevel(String operationalRiskLevel){
		this.operationalRiskLevel=operationalRiskLevel;
		return this;
	}

	public String getPowerRiskLevel(){
		return powerRiskLevel;
	}

	public BusOperationplan setPowerRiskLevel(String powerRiskLevel){
		this.powerRiskLevel=powerRiskLevel;
		return this;
	}

	public String getVoltageLevel(){
		return voltageLevel;
	}

	public BusOperationplan setVoltageLevel(String voltageLevel){
		this.voltageLevel=voltageLevel;
		return this;
	}

	public String getOperationChargePerson(){
		return operationChargePerson;
	}

	public BusOperationplan setOperationChargePerson(String operationChargePerson){
		this.operationChargePerson=operationChargePerson;
		return this;
	}

	public String getOperationChargePersonId(){
		return operationChargePersonId;
	}

	public BusOperationplan setOperationChargePersonId(String operationChargePersonId){
		this.operationChargePersonId=operationChargePersonId;
		return this;
	}

	public String getOperationChargePersonPhone(){
		return operationChargePersonPhone;
	}

	public BusOperationplan setOperationChargePersonPhone(String operationChargePersonPhone){
		this.operationChargePersonPhone=operationChargePersonPhone;
		return this;
	}

	public String getStatus(){
		return status;
	}

	public BusOperationplan setStatus(String status){
		this.status=status;
		return this;
	}

	public String getCreateBy(){
		return createBy;
	}

	public BusOperationplan setCreateBy(String createBy){
		this.createBy=createBy;
		return this;
	}

	public Timestamp getCreateDate(){
		return createDate;
	}

	public BusOperationplan setCreateDate(Timestamp createDate){
		this.createDate=createDate;
		return this;
	}

	public String getUpdateBy(){
		return updateBy;
	}

	public BusOperationplan setUpdateBy(String updateBy){
		this.updateBy=updateBy;
		return this;
	}

	public Timestamp getUpdateDate(){
		return updateDate;
	}

	public BusOperationplan setUpdateDate(Timestamp updateDate){
		this.updateDate=updateDate;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public BusOperationplan setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getRemarks(){
		return remarks;
	}

	public BusOperationplan setRemarks(String remarks){
		this.remarks=remarks;
		return this;
	}

	public String getOperationLocation(){
		return operationLocation;
	}

	public BusOperationplan setOperationLocation(String operationLocation){
		this.operationLocation=operationLocation;
		return this;
	}

	public String getConstructionManagementUnit(){
		return constructionManagementUnit;
	}

	public BusOperationplan setConstructionManagementUnit(String constructionManagementUnit){
		this.constructionManagementUnit=constructionManagementUnit;
		return this;
	}

	public String getConstructionUnit(){
		return constructionUnit;
	}

	public BusOperationplan setConstructionUnit(String constructionUnit){
		this.constructionUnit=constructionUnit;
		return this;
	}

	public String getConstructionUnitType(){
		return constructionUnitType;
	}

	public BusOperationplan setConstructionUnitType(String constructionUnitType){
		this.constructionUnitType=constructionUnitType;
		return this;
	}

	public String getMainUnitNum(){
		return mainUnitNum;
	}

	public BusOperationplan setMainUnitNum(String mainUnitNum){
		this.mainUnitNum=mainUnitNum;
		return this;
	}

	public String getOutsourcingUnitNum(){
		return outsourcingUnitNum;
	}

	public BusOperationplan setOutsourcingUnitNum(String outsourcingUnitNum){
		this.outsourcingUnitNum=outsourcingUnitNum;
		return this;
	}

	public String getSuperviseUnitNum(){
		return superviseUnitNum;
	}

	public BusOperationplan setSuperviseUnitNum(String superviseUnitNum){
		this.superviseUnitNum=superviseUnitNum;
		return this;
	}

	public String getSafetyChargePerson(){
		return safetyChargePerson;
	}

	public BusOperationplan setSafetyChargePerson(String safetyChargePerson){
		this.safetyChargePerson=safetyChargePerson;
		return this;
	}

	public String getSafetyChargePersonId(){
		return safetyChargePersonId;
	}

	public BusOperationplan setSafetyChargePersonId(String safetyChargePersonId){
		this.safetyChargePersonId=safetyChargePersonId;
		return this;
	}

	public String getSafetyChargePersonPhone(){
		return safetyChargePersonPhone;
	}

	public BusOperationplan setSafetyChargePersonPhone(String safetyChargePersonPhone){
		this.safetyChargePersonPhone=safetyChargePersonPhone;
		return this;
	}

	public String getSpeicalPerson() {
		return speicalPerson;
	}

	public void setSpeicalPerson(String speicalPerson) {
		this.speicalPerson = speicalPerson;
	}

	public String getSpeicalId() {
		return speicalId;
	}

	public void setSpeicalId(String speicalId) {
		this.speicalId = speicalId;
	}

	public String getSpeicalPersonPhone() {
		return speicalPersonPhone;
	}

	public void setSpeicalPersonPhone(String speicalPersonPhone) {
		this.speicalPersonPhone = speicalPersonPhone;
	}

	public String getViolation(){
		return violation;
	}

	public BusOperationplan setViolation(String violation){
		this.violation=violation;
		return this;
	}

	public String getIntegrity(){
		return integrity;
	}

	public BusOperationplan setIntegrity(String integrity){
		this.integrity=integrity;
		return this;
	}

	public String getCameras() {
		return cameras;
	}

	public void setCameras(String cameras) {
		this.cameras = cameras;
	}

	public String getCameras_name() {
		return cameras_name;
	}

	public void setCameras_name(String cameras_name) {
		this.cameras_name = cameras_name;
	}

	public String getAlgorithms() {
		return algorithms;
	}

	public void setAlgorithms(String algorithms) {
		this.algorithms = algorithms;
	}

	public String getErrMsg(){
		return errMsg;
	}

	public BusOperationplan setErrMsg(String errMsg){
		this.errMsg=errMsg;
		return this;
	}

}
