package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

/**
 * 事件分类
 */
@Table(name="violation_algorithm")
public class ViolationAlgorithm extends ActiveRecord<ViolationAlgorithm> {
	public static final String ID = "ID";

	public static final String NAME = "NAME";

	public static final String VIOLATION_TYPE_ID = "VIOLATION_TYPE_ID";

	public static final String NUM = "NUM";

	public static final String CREATE_BY = "CREATE_BY";

	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String UPDATE_BY = "UPDATE_BY";

	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String DEL_FLAG = "DEL_FLAG";

	public static final String REMARKS = "REMARKS";

	public static final String ICON = "ICON";

	@Id
	@Column(name="ID", length=192)
	private String id;

	/**
	 * 事件名
	 */
	@Column(name="NAME", length=150)
	private String name;
	/**
	 * 关联事件分类 VIOLATION_TYPE表
	 */
	@Column(name="VIOLATION_TYPE_ID", length=192)
	private String violationTypeId;
	/**
	 * 算法编号
	 */
	@Column(name="NUM", length=30)
	private String num;
	@Column(name="CREATE_BY", length=192)
	private String createBy;
	@Column(name="CREATE_DATE")
	private Timestamp createDate;
	@Column(name="UPDATE_BY", length=192)
	private String updateBy;
	@Column(name="UPDATE_DATE")
	private Timestamp updateDate;
	/**
	 * 删除标识符（0：正常  1：删除）
	 */
	@Column(name="DEL_FLAG", length=3)
	private String delFlag;
	/**
	 * 备注
	 */
	@Column(name="REMARKS", length=600)
	private String remarks;
	/**
	 * 图标
	 */
	@Column(name="ICON", length=765)
	private String icon;

	public String getId(){
		return id;
	}

	public ViolationAlgorithm setId(String id){
		this.id=id;
		return this;
	}

	public String getName(){
		return name;
	}

	public ViolationAlgorithm setName(String name){
		this.name=name;
		return this;
	}

	public String getViolationTypeId(){
		return violationTypeId;
	}

	public ViolationAlgorithm setViolationTypeId(String violationTypeId){
		this.violationTypeId=violationTypeId;
		return this;
	}

	public String getNum(){
		return num;
	}

	public ViolationAlgorithm setNum(String num){
		this.num=num;
		return this;
	}

	public String getCreateBy(){
		return createBy;
	}

	public ViolationAlgorithm setCreateBy(String createBy){
		this.createBy=createBy;
		return this;
	}

	public Timestamp getCreateDate(){
		return createDate;
	}

	public ViolationAlgorithm setCreateDate(Timestamp createDate){
		this.createDate=createDate;
		return this;
	}

	public String getUpdateBy(){
		return updateBy;
	}

	public ViolationAlgorithm setUpdateBy(String updateBy){
		this.updateBy=updateBy;
		return this;
	}

	public Timestamp getUpdateDate(){
		return updateDate;
	}

	public ViolationAlgorithm setUpdateDate(Timestamp updateDate){
		this.updateDate=updateDate;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public ViolationAlgorithm setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getRemarks(){
		return remarks;
	}

	public ViolationAlgorithm setRemarks(String remarks){
		this.remarks=remarks;
		return this;
	}

	public String getIcon(){
		return icon;
	}

	public ViolationAlgorithm setIcon(String icon){
		this.icon=icon;
		return this;
	}

}
