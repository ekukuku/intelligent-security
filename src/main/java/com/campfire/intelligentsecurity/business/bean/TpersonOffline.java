package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="t_person_offline")
public class TpersonOffline extends ActiveRecord<TpersonOffline> {
    public static final String ID = "id";

    public static final String OFFLINE = "offline";

    public static final String USERID = "userid";

    public static final String NAME = "name";

    public static final String PHONE = "phone";

    public static final String UNIT = "unit";

    public static final String DRIVER_POINT = "driver_point";

    public static final String FACEIMAGE = "faceImage";

    public static final String CREATETIME = "createTime";

    public static final String FACEID = "faceid";

    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 离线视频id
     */
    @Column(name="offline", length=64)
    private String offline;
    /**
     * 人员id
     */
    private String userid;
    /**
     * 姓名
     */
    private String name;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 单位
     */
    private String unit;
    /**
     * 驾照分
     */
    @Column(name="driver_point")
    private String driverPoint;
    /**
     * 违章图片地址
     */
    private String faceImage;
    /**
     * 违章时间
     */
    private Timestamp createTime;
    private String faceid;

    public Long getId(){
        return id;
    }

    public TpersonOffline setId(Long id){
        this.id=id;
        return this;
    }

    public String getOffline(){
        return offline;
    }

    public TpersonOffline setOffline(String offline){
        this.offline=offline;
        return this;
    }

    public String getUserid(){
        return userid;
    }

    public TpersonOffline setUserid(String userid){
        this.userid=userid;
        return this;
    }

    public String getName(){
        return name;
    }

    public TpersonOffline setName(String name){
        this.name=name;
        return this;
    }

    public String getPhone(){
        return phone;
    }

    public TpersonOffline setPhone(String phone){
        this.phone=phone;
        return this;
    }

    public String getUnit(){
        return unit;
    }

    public TpersonOffline setUnit(String unit){
        this.unit=unit;
        return this;
    }

    public String getDriverPoint(){
        return driverPoint;
    }

    public TpersonOffline setDriverPoint(String driverPoint){
        this.driverPoint=driverPoint;
        return this;
    }

    public String getFaceImage(){
        return faceImage;
    }

    public TpersonOffline setFaceImage(String faceImage){
        this.faceImage=faceImage;
        return this;
    }

    public Timestamp getCreateTime(){
        return createTime;
    }

    public TpersonOffline setCreateTime(Timestamp createTime){
        this.createTime=createTime;
        return this;
    }

    public String getFaceid(){
        return faceid;
    }

    public TpersonOffline setFaceid(String faceid){
        this.faceid=faceid;
        return this;
    }
}