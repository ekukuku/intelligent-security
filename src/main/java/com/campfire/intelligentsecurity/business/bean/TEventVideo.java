package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="t_event_video")
public class TEventVideo extends ActiveRecord<TEventVideo> {
	public static final String ID = "id";

	public static final String CAMERANUM = "cameraNum";

	public static final String ALGORITHMID = "algorithmId";

	public static final String IMAGEPATH = "imagePath";

	public static final String VIDEOPATH = "videoPath";

	public static final String STATE = "`state`";

	public static final String XCTIME = "xcTime";

	public static final String CREATETIME = "createTime";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 摄像头编号
	 */
	@Column(name="cameraNum", length=300)
	private String cameraNum;
	/**
	 * 算法编号
	 */
	@Column(name="algorithmId", length=64)
	private String algorithmId;
	/**
	 * 事件图片地址
	 */
	@Column(name="imagePath")
	private String imagePath;
	/**
	 * 事件视频地址
	 */
	@Column(name="videoPath")
	private String videoPath;
	/**
	 * 0待确认 1已确认 2已消警
	 */
	@Column(name="`state`",insertable = false)
	private Byte state;
	/**
	 * 巡查时间
	 */
	@Column(name = "xcTime")
	private Timestamp xcTime;
	/**
	 * 创建时间
	 */
	@Column(insertable = false)
	private Timestamp createTime;

	public Long getId(){
		return id;
	}

	public TEventVideo setId(Long id){
		this.id=id;
		return this;
	}

	public String getCameraNum(){
		return cameraNum;
	}

	public TEventVideo setCameraNum(String cameraNum){
		this.cameraNum=cameraNum;
		return this;
	}

	public String getAlgorithmId(){
		return algorithmId;
	}

	public TEventVideo setAlgorithmId(String algorithmId){
		this.algorithmId=algorithmId;
		return this;
	}

	public String getImagePath(){
		return imagePath;
	}

	public TEventVideo setImagePath(String imagePath){
		this.imagePath=imagePath;
		return this;
	}

	public String getVideoPath(){
		return videoPath;
	}

	public TEventVideo setVideoPath(String videoPath){
		this.videoPath=videoPath;
		return this;
	}

	public Byte getState(){
		return state;
	}

	public TEventVideo setState(Byte state){
		this.state=state;
		return this;
	}

	public Timestamp getXcTime(){
		return xcTime;
	}

	public TEventVideo setXcTime(Timestamp xcTime){
		this.xcTime=xcTime;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TEventVideo setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

}
