package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="t_sys_dict")
public class TSysDict extends ActiveRecord<TSysDict> {
	public static final String ID = "ID";

	public static final String LABEL = "LABEL";

	public static final String VALUE = "`VALUE`";

	public static final String TYPE = "TYPE";

	public static final String SORT = "SORT";

	public static final String CREATE_BY = "CREATE_BY";

	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String UPDATE_BY = "UPDATE_BY";

	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String DEL_FLAG = "DEL_FLAG";

	public static final String REMARKS = "REMARKS";

	public static final String VALUE_REMARKS = "VALUE_REMARKS";

	/**
	 * ID唯一主键
	 */
	@Id
	private Integer id;

	/**
	 * 码
	 */
	@Column(name="LABEL", length=192)
	private String label;
	/**
	 * 值
	 */
	@Column(name="`VALUE`", length=192)
	private String value;
	/**
	 * 类型
	 */
	@Column(name="TYPE", length=192)
	private String type;
	/**
	 * 排序
	 */
	private Integer sort;
	@Column(name="CREATE_BY", length=192)
	private String createBy;
	@Column(name="CREATE_DATE")
	private Timestamp createDate;
	@Column(name="UPDATE_BY", length=192)
	private String updateBy;
	@Column(name="UPDATE_DATE")
	private Timestamp updateDate;
	/**
	 * 删除标识符（0：正常  1：删除）
	 */
	@Column(name="DEL_FLAG", length=3)
	private String delFlag;
	/**
	 * 备注
	 */
	@Column(name="REMARKS", length=600)
	private String remarks;
	/**
	 * 值说明
	 */
	@Column(name="VALUE_REMARKS", length=1500)
	private String valueRemarks;

	public Integer getId(){
		return id;
	}

	public TSysDict setId(Integer id){
		this.id=id;
		return this;
	}

	public String getLabel(){
		return label;
	}

	public TSysDict setLabel(String label){
		this.label=label;
		return this;
	}

	public String getValue(){
		return value;
	}

	public TSysDict setValue(String value){
		this.value=value;
		return this;
	}

	public String getType(){
		return type;
	}

	public TSysDict setType(String type){
		this.type=type;
		return this;
	}

	public Integer getSort(){
		return sort;
	}

	public TSysDict setSort(Integer sort){
		this.sort=sort;
		return this;
	}

	public String getCreateBy(){
		return createBy;
	}

	public TSysDict setCreateBy(String createBy){
		this.createBy=createBy;
		return this;
	}

	public Timestamp getCreateDate(){
		return createDate;
	}

	public TSysDict setCreateDate(Timestamp createDate){
		this.createDate=createDate;
		return this;
	}

	public String getUpdateBy(){
		return updateBy;
	}

	public TSysDict setUpdateBy(String updateBy){
		this.updateBy=updateBy;
		return this;
	}

	public Timestamp getUpdateDate(){
		return updateDate;
	}

	public TSysDict setUpdateDate(Timestamp updateDate){
		this.updateDate=updateDate;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public TSysDict setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getRemarks(){
		return remarks;
	}

	public TSysDict setRemarks(String remarks){
		this.remarks=remarks;
		return this;
	}

	public String getValueRemarks(){
		return valueRemarks;
	}

	public TSysDict setValueRemarks(String valueRemarks){
		this.valueRemarks=valueRemarks;
		return this;
	}

}
