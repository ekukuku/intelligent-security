package com.campfire.intelligentsecurity.business.bean.api;

public class GETSTEAMStruct implements ApiData{
    private String CAMERA_CODE;

    public String getCAMERA_CODE() {
        return CAMERA_CODE;
    }

    public void setCAMERA_CODE(String CAMERA_CODE) {
        this.CAMERA_CODE = CAMERA_CODE;
    }
}
