package com.campfire.intelligentsecurity.business.bean.api;

public class RLSBXXJSStruct implements ApiData {

    private String FACE_ID;
    //工作计划id
    private String CAMERA_ID;
    //识别事件
    private String FIND_TIME;
    //识别截图
    private String USER_PIC;

    public String getFACE_ID() {
        return FACE_ID;
    }

    public void setFACE_ID(String FACE_ID) {
        this.FACE_ID = FACE_ID;
    }

    public String getCAMERA_ID() {
        return CAMERA_ID;
    }

    public void setCAMERA_ID(String CAMERA_ID) {
        this.CAMERA_ID = CAMERA_ID;
    }

    public String getFIND_TIME() {
        return FIND_TIME;
    }

    public void setFIND_TIME(String FIND_TIME) {
        this.FIND_TIME = FIND_TIME;
    }

    public String getUSER_PIC() {
        return USER_PIC;
    }

    public void setUSER_PIC(String USER_PIC) {
        this.USER_PIC = USER_PIC;
    }
}
