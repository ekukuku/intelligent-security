package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="bus_unit")
public class BusUnit extends ActiveRecord<BusUnit> {
	public static final String ID = "id";

	public static final String CREATE_BY = "create_by";

	public static final String CREATE_TIME = "create_time";

	public static final String UPDATE_BY = "update_by";

	public static final String UPDATE_TIME = "update_time";

	public static final String DEL_FLAG = "del_flag";

	public static final String REMARK = "remark";

	public static final String ENTERPRISE_NAME = "enterprise_name";

	public static final String CREDIT_CODE = "credit_code";

	public static final String LEGAL_NAME = "legal_name";

	public static final String LEGAL_PHONE = "legal_phone";

	public static final String ACCESS_STATUS = "access_status";

	public static final String ACCEPTANCE_UNIT = "acceptance_unit";

	public static final String SAFETY_POINTS = "safety_points";

	public static final String NEGATIVE_STATE = "negative_state";

	public static final String BLACK_LIST = "black_list";

	/**
	 * 人员id
	 */
	@Id
	@Column(name="id", length=192)
	private String id;

	@Column(name="create_by", length=192,updatable = false)
	private String createBy;
	@Column(name="create_time",updatable = false, insertable = false)
	private Timestamp createTime;
	@Column(name="update_by", length=192,insertable = false)
	private String updateBy;
	@Column(name="update_time",insertable = false, updatable = false)
	private Timestamp updateTime;
	/**
	 * 删除标识符（0：正常  1：删除）
	 */
	@Column(name="del_flag", updatable = false)
	private String delFlag;
	/**
	 * 备注
	 */
	@Column(name="remark", length=600)
	private String remark;
	/**
	 * 企业名称
	 */
	@Column(name="enterprise_name", length=765)
	private String enterpriseName;
	/**
	 * 社会统一信用代码
	 */
	@Column(name="credit_code", length=765)
	private String creditCode;
	/**
	 * 法人姓名
	 */
	@Column(name="legal_name", length=765)
	private String legalName;
	/**
	 * 法人电话
	 */
	@Column(name="legal_phone", length=765)
	private String legalPhone;
	/**
	 * 准入状态
	 */
	@Column(name="access_status", length=765)
	private String accessStatus;
	/**
	 * 受理单位
	 */
	@Column(name="acceptance_unit", length=765)
	private String acceptanceUnit;
	/**
	 * 安全积分
	 */
	@Column(name="safety_points")
	private Double safetyPoints;
	/**
	 * 是否为负面清单
	 */
	@Column(name="negative_state", length=765)
	private String negativeState;
	/**
	 * 是否为黑名单
	 */
	@Column(name="black_list", length=765)
	private String blackList;

	public String getId(){
		return id;
	}

	public BusUnit setId(String id){
		this.id=id;
		return this;
	}

	public String getCreateBy(){
		return createBy;
	}

	public BusUnit setCreateBy(String createBy){
		this.createBy=createBy;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public BusUnit setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

	public String getUpdateBy(){
		return updateBy;
	}

	public BusUnit setUpdateBy(String updateBy){
		this.updateBy=updateBy;
		return this;
	}

	public Timestamp getUpdateTime(){
		return updateTime;
	}

	public BusUnit setUpdateTime(Timestamp updateTime){
		this.updateTime=updateTime;
		return this;
	}

	public String getDelFlag(){
		return delFlag;
	}

	public BusUnit setDelFlag(String delFlag){
		this.delFlag=delFlag;
		return this;
	}

	public String getRemark(){
		return remark;
	}

	public BusUnit setRemark(String remark){
		this.remark=remark;
		return this;
	}

	public String getEnterpriseName(){
		return enterpriseName;
	}

	public BusUnit setEnterpriseName(String enterpriseName){
		this.enterpriseName=enterpriseName;
		return this;
	}

	public String getCreditCode(){
		return creditCode;
	}

	public BusUnit setCreditCode(String creditCode){
		this.creditCode=creditCode;
		return this;
	}

	public String getLegalName(){
		return legalName;
	}

	public BusUnit setLegalName(String legalName){
		this.legalName=legalName;
		return this;
	}

	public String getLegalPhone(){
		return legalPhone;
	}

	public BusUnit setLegalPhone(String legalPhone){
		this.legalPhone=legalPhone;
		return this;
	}

	public String getAccessStatus(){
		return accessStatus;
	}

	public BusUnit setAccessStatus(String accessStatus){
		this.accessStatus=accessStatus;
		return this;
	}

	public String getAcceptanceUnit(){
		return acceptanceUnit;
	}

	public BusUnit setAcceptanceUnit(String acceptanceUnit){
		this.acceptanceUnit=acceptanceUnit;
		return this;
	}

	public Double getSafetyPoints(){
		return safetyPoints;
	}

	public BusUnit setSafetyPoints(Double safetyPoints){
		this.safetyPoints=safetyPoints;
		return this;
	}

	public String getNegativeState(){
		return negativeState;
	}

	public BusUnit setNegativeState(String negativeState){
		this.negativeState=negativeState;
		return this;
	}

	public String getBlackList(){
		return blackList;
	}

	public BusUnit setBlackList(String blackList){
		this.blackList=blackList;
		return this;
	}


}
