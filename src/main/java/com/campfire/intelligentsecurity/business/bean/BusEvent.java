package com.campfire.intelligentsecurity.business.bean;


import java.util.List;

public class BusEvent {
    private String id;
    private String typeName;
    private List<EventList> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<EventList> getList() {
        return list;
    }

    public void setList(List<EventList> list) {
        this.list = list;
    }
}
