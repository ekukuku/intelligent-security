package com.campfire.intelligentsecurity.business.bean;

import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;

@Table(name="t_event_record")
public class TEventRecord extends ActiveRecord<TEventRecord> {
	public static final String ID = "id";

	public static final String PLANID = "planid";

	public static final String EVENTID = "eventid";

	public static final String ALGORITHMID = "algorithmId";

	public static final String IMAGEPATH = "imagePath";

	public static final String VIDEOPATH = "videoPath";

	public static final String STATE = "`state`";

	public static final String CREATETIME = "createTime";

	/**
	 * 主键
	 */
	@Id
	private Long id;

	/**
	 * 作业计划ID
	 */
	@Column(name="planid", length=192)
	private String planid;
	/**
	 * 违章分类
	 */
	@Column(name="eventid")
	private Integer eventid;
	/**
	 * 算法编号
	 */
	@Column(name="algorithmId")
	private Integer algorithmId;
	/**
	 * 违章图片地址
	 */
	@Column(name="imagePath", length=765)
	private String imagePath;
	/**
	 * 违章视频地址
	 */
	@Column(name="videoPath", length=765)
	private String videoPath;
	/**
	 * 0待确认 1已确认 2已消警
	 */
	@Column(name="`state`")
	private Byte state;
	/**
	 * 违章时间
	 */
	@Column(name="`createTime`")
	private Timestamp createTime;

	public Long getId(){
		return id;
	}

	public TEventRecord setId(Long id){
		this.id=id;
		return this;
	}

	public String getPlanid(){
		return planid;
	}

	public TEventRecord setPlanid(String planid){
		this.planid=planid;
		return this;
	}

	public Integer getEventid(){
		return eventid;
	}

	public TEventRecord setEventid(Integer eventid){
		this.eventid=eventid;
		return this;
	}

	public Integer getAlgorithmId(){
		return algorithmId;
	}

	public TEventRecord setAlgorithmId(Integer algorithmId){
		this.algorithmId=algorithmId;
		return this;
	}

	public String getImagePath(){
		return imagePath;
	}

	public TEventRecord setImagePath(String imagePath){
		this.imagePath=imagePath;
		return this;
	}

	public String getVideoPath(){
		return videoPath;
	}

	public TEventRecord setVideoPath(String videoPath){
		this.videoPath=videoPath;
		return this;
	}

	public Byte getState(){
		return state;
	}

	public TEventRecord setState(Byte state){
		this.state=state;
		return this;
	}

	public Timestamp getCreateTime(){
		return createTime;
	}

	public TEventRecord setCreateTime(Timestamp createTime){
		this.createTime=createTime;
		return this;
	}

}
