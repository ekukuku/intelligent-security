package com.campfire.intelligentsecurity.business.bean.camera;

public class AuthStruct {
    private String ak;
    private String sk;
    private String token;
    private String url;
    private String tokenurl;
    private String controlurl;
    private String statusurl;
    private String snapurl;
    private String playurl;

    public String getTokenurl() {
        return tokenurl;
    }

    public void setTokenurl(String tokenurl) {
        this.tokenurl = tokenurl;
    }

    public String getControlurl() {
        return controlurl;
    }

    public void setControlurl(String controlurl) {
        this.controlurl = controlurl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getPlayurl() {
        return playurl;
    }

    public void setPlayurl(String playurl) {
        this.playurl = playurl;
    }

    public String getStatusurl() {
        return statusurl;
    }

    public void setStatusurl(String statusurl) {
        this.statusurl = statusurl;
    }

    public String getSnapurl() {
        return snapurl;
    }

    public void setSnapurl(String snapurl) {
        this.snapurl = snapurl;
    }
}
