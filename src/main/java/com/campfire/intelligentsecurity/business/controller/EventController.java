package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.TSysEvent;
import com.campfire.intelligentsecurity.business.bean.TSysEventName;
import com.campfire.intelligentsecurity.business.service.EventService;
import com.campfire.intelligentsecurity.business.service.MenuService;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/event")
public class EventController extends BaseController {

    @Autowired
    EventService eventService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.addObject("eventType", eventService.loadEventType());
        mv.setViewName("pages/event/list.html");
        return mv;
    }

    @RequestMapping("/view/eventlist")
    public ModelAndView eventlistView() {
        mv.setViewName("pages/event/eventlist.html");
        return mv;
    }


    @RequestMapping("/data/addEvent")
    public ModelAndView addEventView() {
        mv.addObject("keyList", eventService.keyList());
        mv.addObject("eventInfo", null);
        mv.setViewName("pages/event/eventname.html");
        return mv;
    }


    @RequestMapping("/data/add")
    public ModelAndView add() {
        mv.addObject("eventType", eventService.loadEventType());
        mv.addObject("eventInfo", null);
        mv.setViewName("pages/event/eventinfo.html");
        return mv;
    }

    @RequestMapping("/data/edit")
    public ModelAndView edit(String id) {
        mv.addObject("eventType", eventService.loadEventType());
        mv.addObject("eventInfo", eventService.loadInfo(id));
        mv.setViewName("pages/event/eventinfo.html");
        return mv;
    }

    @RequestMapping("/data/editEvent")
    public ModelAndView editEvent(String id) {
        mv.addObject("keyList", eventService.keyList());
        mv.addObject("eventInfo", eventService.loadInfoEvent(id));
        mv.setViewName("pages/event/eventname.html");
        return mv;
    }

    @PostMapping("/data/saveEvent")
    @ResponseBody
    public Object saveOrUpdateEvent(@RequestBody TSysEventName eventName){
        return success(eventService.saveOrUpdateEvent(eventName));
    }

    @DeleteMapping("/data/removeEvent")
    @ResponseBody
    public Object removeEvent(String ids) {
        return success(eventService.removeEvent(ids));
    }

    @PostMapping("/data/save")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TSysEvent sysEvent){
        return success(eventService.saveOrUpdate(sysEvent));
    }

    @DeleteMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(eventService.remove(ids));
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return eventService.menuList();
    }

    @RequestMapping("/data/eventlist")
    @ResponseBody
    public Object eventlist() {
        return eventService.eventlist();
    }

    @PostMapping("/data/changeState")
    @ResponseBody
    public Object changeState(String id, String state){
        return success(eventService.chengeState(id, state));
    }


    @PostMapping("/data/changeStatus")
    @ResponseBody
    public Object changeStatus(String id, String state){
        return success(eventService.changeStatus(id, state));
    }

    @PostMapping("/data/changeStateEvent")
    @ResponseBody
    public Object changeStateEvent(String id, String state){
        return success(eventService.changeStateEvent(id, state));
    }


    @PostMapping("/data/changeStatusEvent")
    @ResponseBody
    public Object changeStatusEvent(String id, String state){
        return success(eventService.changeStatusEvent(id, state));
    }


    @GetMapping("/loadEvent")
    @ResponseBody
    public Object loadSf() {
        return success(eventService.loadEvent());
    }

    @GetMapping("/loadEventName")
    @ResponseBody
    public Object loadEventName(String id) {
        return success(eventService.loadEventName(id));
    }

    @RequestMapping("/view/loadsf")
    public ModelAndView loadsf(String cameraNum) {
        mv.addObject("cameraNum", cameraNum);
        mv.addObject("EventList", eventService.loadCameraAlgorithm(cameraNum));
        mv.setViewName("pages/monitor/cgsf.html");
        return mv;
    }

    @RequestMapping("/data/loadCameraAlgorithm")
    @ResponseBody
    public Object loadCameraAlgorithm(String cameraNum) {
        return success(eventService.loadCameraAlgorithm(cameraNum));
    }
}
