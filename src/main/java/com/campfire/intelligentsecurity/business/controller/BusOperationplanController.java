package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.campfire.intelligentsecurity.business.bean.BusOperationplan;
import com.campfire.intelligentsecurity.business.bean.TEventRecord;
import com.campfire.intelligentsecurity.business.service.BusOperationplanService;
import com.campfire.intelligentsecurity.business.service.DictService;
import com.campfire.intelligentsecurity.business.service.EventService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/busOperationplan")
public class BusOperationplanController extends BaseController {


    @Autowired
    BusOperationplanService busOperationplanService;
    @Autowired
    DictService dictService;
    @Autowired
    EventService eventService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/busOperationplan/list.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView input(String id, String type) {
        mv.addObject("gzxzList", dictService.loadCodeBook("dict_gzxz"));
        mv.addObject("dydjList", dictService.loadCodeBook("dict_dydj"));
        mv.addObject("zyfxdjList", dictService.loadCodeBook("dict_zyfxdj"));
        mv.addObject("dwfxdjList", dictService.loadCodeBook("dict_dwfxdj"));
        mv.addObject("dwList", dictService.loadDw());
        if (StrUtil.isNotEmpty(id)) {
            mv.addObject("task", busOperationplanService.load(id));
            mv.addObject("taskZyry", busOperationplanService.loadZyry(id));
            mv.addObject("taskZysb", busOperationplanService.loadZysb(id));
            mv.addObject("taskSgdw", busOperationplanService.loadSgdw(id));
        } else {
            mv.addObject("task", null);
            mv.addObject("taskZyry", null);
            mv.addObject("taskZysb", null);
            mv.addObject("taskSgdw", null);
        }
        mv.addObject("type", type);
        mv.setViewName("pages/busOperationplan/edit.html");
        return mv;
    }

    @RequestMapping("/view/sf")
    public ModelAndView sf(String ids,String type) {
        mv.addObject("operationId", ids);
        if ("1".equals(type)){
            mv.addObject("type", type);
            mv.addObject("EventList", eventService.loadPalnAlgorithm(ids));
        }else{
            mv.addObject("type", 2);
            mv.addObject("EventList", eventService.loadEvent());
        }
        mv.setViewName("pages/busOperationplan/sf.html");
        return mv;
    }

    @RequestMapping("/view/selectGis")
    public ModelAndView selectGis() {
        mv.setViewName("pages/busOperationplan/gis.html");
        return mv;
    }

    @RequestMapping("/view/uploadplan")
    public ModelAndView uploadplan() {
        mv.setViewName("pages/busOperationplan/uploadplan.html");
        return mv;
    }

    @RequestMapping("/view/upload")
    public ModelAndView upload(String planid) {
        mv.addObject("planid", planid);
        mv.setViewName("pages/busOperationplan/upload.html");
        return mv;
    }

    public final static String UPLOAD_FILE_PATH = "D:\\workspaceIDEA\\intelligent-security\\target\\classes\\static\\admin\\video";

    @RequestMapping(value = "uploadVideo")
    @ResponseBody
    public JSONObject uploadVideo(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String planid = request.getParameter("planid");
        if (!file.isEmpty()) {
            JSONObject json = new JSONObject();
            try {
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(UPLOAD_FILE_PATH, file.getOriginalFilename())));
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (Exception e) {
                json.put("msg", "error");
                json.put("code", "1");
                return json;
            }
            json.put("msg", "ok");
            json.put("code", "0");
            return json;
        } else {
            return null;
        }
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list(BusOperationplan busOperationplan) {
        return busOperationplanService.List(busOperationplan);
    }

    @RequestMapping("/data/listplan")
    @ResponseBody
    public Object listPlan(BusOperationplan busOperationplan) {
        return busOperationplanService.ListPlan(busOperationplan);
    }

    @RequestMapping("/data/OperationplanList")
    @ResponseBody
    public Object OperationplanList() {
        return busOperationplanService.OperationplanList();
    }

    @RequestMapping("/data/loadAllzyCoordinates")
    @ResponseBody
    public Object loadAllzyCoordinates(String status) {
        return busOperationplanService.loadAllzyCoordinates(status);
    }

    @RequestMapping("/data/echartsAllplan")
    @ResponseBody
    public Object echartsAllplan() {
        return busOperationplanService.echartsAllplan();
    }

    @RequestMapping("/data/wzList")
    @ResponseBody
    public Object wzList(TEventRecord tEventRecord) {
        return busOperationplanService.wzList(tEventRecord);
    }

    @RequestMapping("/data/wzCount")
    @ResponseBody
    public Object wzCount(String planid) {
        return busOperationplanService.wzCount(planid);
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusOperationplan busOperationplan) {
        LOG.debug("新增/保存....");
        return success(busOperationplanService.saveOrUpdate(busOperationplan));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除计划....");
        return success(busOperationplanService.remove(ids));
    }

    @PostMapping("/data/wzListDelete")
    @ResponseBody
    public Object wzListDelete(String ids) {
        LOG.debug("删除违章....");
        return success(busOperationplanService.wzListDelete(ids));
    }


    @PostMapping("/data/over")
    @ResponseBody
    public Object over(String ids) {
        LOG.debug("结束....");
        return success(busOperationplanService.startORover(ids, "2", null));
    }

    @PostMapping("/data/start")
    @ResponseBody
    public Object start(String ids, String sfId) {
        LOG.debug("开始....");
        Object obj = busOperationplanService.startORover(ids, "1", sfId);
        if (obj instanceof String && "行为识别接口服务无法访问".equals(obj)) {
            return failure(-1, String.valueOf(obj));
        }
        return success(obj);
    }

    @GetMapping("/data/cameraCheck")
    @ResponseBody
    public Object cameraCheck(String cameraid, String startTime, String endTime, String operationid) {
        return success(busOperationplanService.cameraCheck(cameraid, startTime, endTime, operationid));
    }

    @PostMapping("/data/wzqr")
    @ResponseBody
    public Object wzqr(String id, String state, String type) {
        LOG.debug("确认违章....");
        return success(busOperationplanService.wzqr(id, state, type));
    }

    @ResponseBody
    @RequestMapping(value = "/data/importData", method = {RequestMethod.GET, RequestMethod.POST})
    public Object importData(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //    System.out.println("进来了");
        Map<String, String> map = new HashMap<>();
        return success(busOperationplanService.importData(file, request, response));
    }
}
