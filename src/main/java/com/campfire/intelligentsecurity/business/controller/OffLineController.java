package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.campfire.intelligentsecurity.business.bean.BusOperationplan;
import com.campfire.intelligentsecurity.business.bean.TOfflineInfo;
import com.campfire.intelligentsecurity.business.service.CameraControlService;
import com.campfire.intelligentsecurity.business.service.EventService;
import com.campfire.intelligentsecurity.business.service.OffLineService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/offline")
public class OffLineController extends BaseController {

    @Value("${offline.uploadFolder}")
    private String uploadFolder;
    @Value("${local.offlineApi}")
    private String offlineApi;
    @Autowired
    OffLineService service;
    @Autowired
    EventService eventService;
    @Autowired
    CameraControlService controlService;

    @RequestMapping("/view/list")
    public ModelAndView selectGis() {
        mv.setViewName("pages/offline/list.html");
        return mv;
    }

    @RequestMapping("/view/upload")
    public ModelAndView upload() {
        mv.setViewName("pages/offline/upload.html");
        return mv;
    }

    @RequestMapping("/view/sf")
    public ModelAndView sf(String id) {
        mv.addObject("id",id);
        mv.addObject("EventList",eventService.loadEvent());
        mv.setViewName("pages/offline/sf.html");
        return mv;
    }

    @RequestMapping("/view/showEvent")
    public ModelAndView showEvent(String id) {
        mv.addObject("id",id);
        mv.setViewName("pages/offline/showEvent.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return service.List();
    }

    @RequestMapping("/data/offlineList")
    @ResponseBody
    public Object offlineList() {
        return success(service.offlineList());
    }

    @RequestMapping("/data/eventList")
    @ResponseBody
    public Object eventList(String id) {
        return service.eventList();
    }


    @RequestMapping("/data/getPersonOffline")
    @ResponseBody
    public Object getPersonOffline(String offlineId) {
        return service.getPersonOffline(offlineId);
    }

    @RequestMapping(value = "uploadVideo")
    @ResponseBody
    public JSONObject uploadVideo(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String size=request.getParameter("size");
        if (!file.isEmpty()) {
            JSONObject json=new JSONObject();
            File filePath=null;
            File file1=null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
                String date=sdf.format(new Date());
                String name=file.getOriginalFilename();

                String prefix=name.substring(name.lastIndexOf(".")+1);
                String fileName=sdf2.format(DateUtil.date())+"."+prefix;
                filePath=new File(uploadFolder+date);
                if (!filePath.exists()){
                    filePath.mkdirs();
                }
                file1=new File(uploadFolder+date, fileName);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file1));
                out.write(file.getBytes());
                out.flush();
                out.close();
                TOfflineInfo info =new TOfflineInfo();
                info.setSize(size);
                info.setVname(file.getOriginalFilename());
                info.setUrl("/offlineVideo/"+date+"/"+fileName);
                info.setEvent("offline");
                service.saveOrUpdate(info);
            } catch (Exception e) {
                json.put("msg", "error");
                json.put("code", "1");
                return json;
            }
            json.put("msg", "ok");
            json.put("code", "0");
            return json;
        } else {
            return null;
        }
    }

    /**
     * 开启离线视频解析 违章识别
     * @return
     */
    @PostMapping("/startJx")
    @ResponseBody
    public Object startJx(String event, String id,String sfId){
        return success(service.offlineJx(event,id,sfId));
    }
    /**
     * 离线视频解析 更新状态
     * @return
     */
    @PostMapping("/updStatus")
    @ResponseBody
    public Object updStatus(String ids){
        return success(service.updStatus(ids));
    }

    @PostMapping("/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(service.remove(ids));
    }


    @PostMapping("/data/wzqr")
    @ResponseBody
    public Object wzqr(String id,String state) {
        LOG.debug("确认违章....");
        return success(service.wzqr(id,state));
    }

}
