package com.campfire.intelligentsecurity.business.controller;


import com.campfire.intelligentsecurity.business.bean.BusPersonWz;
import com.campfire.intelligentsecurity.business.service.BusOperationplanService;
import com.campfire.intelligentsecurity.business.service.BusPersonService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/wzgl")
public class WzglController extends BaseController {

    @Autowired
    BusOperationplanService busOperationplanService;
    @Autowired
    BusPersonService busPersonService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.addObject("OperationplanList",busOperationplanService.OperationplanList());
        mv.setViewName("pages/wzgl/list.html");
        return mv;
    }

    @RequestMapping("/view/video")
    public ModelAndView videoView(String plan_id) {
        mv.addObject("planInfo", busOperationplanService.loadPlanInfo(plan_id));
        mv.addObject("camList", busOperationplanService.loadCamera(plan_id));
        mv.setViewName("pages/wzgl/video.html");
        return mv;
    }

    @RequestMapping("/view/wzPoint")
    public ModelAndView wzPoint(String sj,String wzid,String type) {
        mv.addObject("wzPoint", busPersonService.loadWzPoint());
        mv.addObject("ryList", loadRy());
        mv.addObject("sj", sj);
        mv.addObject("wzid", wzid);
        mv.addObject("type", type);
        mv.setViewName("pages/wzgl/kf.html");
        return mv;
    }

    @GetMapping("/data/loadRyxx")
    @ResponseBody
    public Object loadRxxx(String plan_id, String lastTime) {
        return busOperationplanService.loadRyxx(plan_id, lastTime);
    }

    @GetMapping("/data/loadWzxx")
    @ResponseBody
    public Object loadWzxx(String plan_id, String lastTime) {
        return busOperationplanService.loadWzxx(plan_id, lastTime);
    }

    @GetMapping("/data/loadRy")
    @ResponseBody
    public Object loadRy() {
        return busOperationplanService.loadRy();
    }
}

