package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.BusUnit;
import com.campfire.intelligentsecurity.business.service.BusUnitService;
import com.campfire.intelligentsecurity.business.service.ControlEventService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * 企业准入管理
 */

@Controller
@RequestMapping("/busUnit")
public class BusUnitController extends BaseController {

    @Autowired
    BusUnitService busUnitService;
    @Autowired
    ControlEventService controlEventService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/busUnit/list.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView input(String id) {
        mv.addObject("busUnit", busUnitService.load(id));
        mv.setViewName("pages/busUnit/input.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return busUnitService.List();
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusUnit busUnit) {
        LOG.debug("新增/保存....");
        return success(busUnitService.saveOrUpdate(busUnit));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(busUnitService.remove(ids));
    }

    @PostMapping("/data/control")
    @ResponseBody
    public Object control(String unitid, String control_value, String control_type) {
        return success(controlEventService.control(unitid, "1", control_type, control_value));
    }
}
