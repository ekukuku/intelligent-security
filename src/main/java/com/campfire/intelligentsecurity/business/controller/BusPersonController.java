package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusPerson;
import com.campfire.intelligentsecurity.business.bean.BusPersonWz;
import com.campfire.intelligentsecurity.business.service.BusPersonService;
import com.campfire.intelligentsecurity.business.service.ControlEventService;
import com.campfire.intelligentsecurity.business.service.DictService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * 作业人员管理
 */

@Controller
@RequestMapping("/busPerson")
public class BusPersonController extends BaseController {

    @Autowired
    BusPersonService busPersonService;
    @Autowired
    DictService dictService;

    @Autowired
    ControlEventService controlEventService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        //mv.addObject("zylbList", dictService.loadZylb("dict_zylb"));
        mv.addObject("dwList", dictService.loadDw());
        mv.setViewName("pages/busPerson/list.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView input(String id) {
        mv.addObject("jzlbList", dictService.loadCodeBook("dict_jzlb"));
        mv.addObject("dwList", dictService.loadDw());
        if (StrUtil.isNotEmpty(id)) {
            mv.addObject("zylbList", busPersonService.loadZylb(id));
            mv.addObject("busPerson", busPersonService.load(id));
        } else {
            mv.addObject("zylbList", null);
            mv.addObject("busPerson", null);
        }
        mv.setViewName("pages/busPerson/input.html");
        return mv;
    }

    @RequestMapping("/view/wzPoint")
    public ModelAndView wzPoint(String id) {
        mv.addObject("wzPoint", busPersonService.loadWzPoint());
        mv.addObject("personId", id);
        mv.setViewName("pages/busPerson/koufen.html");
        return mv;
    }

    @RequestMapping("/view/showkf")
    public ModelAndView showkf(String personId) {
        mv.addObject("personId", personId);
        mv.setViewName("pages/busPerson/showkf.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return busPersonService.List();
    }

    @RequestMapping("/data/kfList")
    @ResponseBody
    public Object kfList() {
        return busPersonService.kfList();
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusPerson busPerson) {
        LOG.debug("新增/保存....");
        return success(busPersonService.saveOrUpdate(busPerson));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(busPersonService.remove(ids));
    }

    @PostMapping("/data/koufen")
    @ResponseBody
    public Object koufen(@RequestBody BusPersonWz busPersonWz) {
        LOG.debug("扣分....");
        return success(busPersonService.koufen(busPersonWz));
    }

    @PostMapping("/data/control")
    @ResponseBody
    public Object control(String userid, String control_value, String control_type) {
        return success(controlEventService.control(userid, "0", control_type, control_value));
    }
}
