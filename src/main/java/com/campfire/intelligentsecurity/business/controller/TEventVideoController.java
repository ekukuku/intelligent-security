package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.TTaskCron;
import com.campfire.intelligentsecurity.business.service.CameraControlService;
import com.campfire.intelligentsecurity.business.service.TEventVideoService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/video")
public class TEventVideoController extends BaseController {

    @Autowired
    private TEventVideoService tEventVideoService;
    @Autowired
    CameraControlService cameraControlService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/lunxun/list.html");
        return mv;
    }

    @RequestMapping("/view/edit")
    public ModelAndView editView() {
        mv.addObject("taskCron",tEventVideoService.getTask("lunxun"));
        mv.setViewName("pages/lunxun/edit.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return tEventVideoService.List();
    }


    @RequestMapping("/data/lunxun")
    @ResponseBody
    public Object lunxun() {
        try {
            cameraControlService.lunxun();
        } catch (Exception e) {
            return failure();
        }
        return success();
    }

    @RequestMapping("/data/getTask")
    @ResponseBody
    public Object getTask() {
        try {
            Map<String, Object> map = tEventVideoService.getTask("lunxun");
            if (map!=null){
                if ((Integer) map.get("flag")==0){
                    return success();
                }else{
                    return failure(402,"当前轮巡未结束，请等待轮巡结束再执行！");
                }
            }
        } catch (Exception e) {
            return failure();
        }
        return success();
    }

    @RequestMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(tEventVideoService.remove(ids));
    }




    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TTaskCron taskCron) {
        LOG.debug("新增/保存....");
        return success(tEventVideoService.saveOrUpdate(taskCron));
    }

}
