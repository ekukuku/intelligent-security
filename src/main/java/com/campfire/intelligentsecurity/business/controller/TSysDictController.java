package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.service.DictService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.github.drinkjava2.jsqlbox.DB.par;

@RestController
@RequestMapping("/dict")
public class TSysDictController extends BaseController {

    @Autowired
    DictService dictService;

    /**
     * 数据字典
     * @return
     */
    @GetMapping("/loadCodeBook")
    public Object loadCodeBook(String type) {
        return success(dictService.loadCodeBook(type));
    }
    @GetMapping("/loadCodeBook2")
    public Object loadCodeBook2(String type) {
        return success(dictService.loadCodeBook2(type));
    }

    /**
     * 作业人员
     * @return
     */
    @GetMapping("/loadZyry")
    public Object loadZyry(String unit) {
        return success(dictService.loadZyry(unit));
    }

    /**
     * 作业设备检测
     * @return
     */
    @GetMapping("/loadCamera")
    public Object loadCamera() {
        return success(dictService.loadCamera());
    }

    /**
     * 违章算法
     * @return
     */
    @GetMapping("/loadSf")
    public Object loadSf() {
        return success(dictService.loadSf());
    }


    /**
     * 单位
     * @return
     */
    @GetMapping("/loadDw")
    public Object loadDw() {
        return success(dictService.loadDw());
    }

    /**
     * 单位
     * @return
     */
    @GetMapping("/loadDw2")
    public Object loadDw2() {
        return success(dictService.loadDw2());
    }
}
