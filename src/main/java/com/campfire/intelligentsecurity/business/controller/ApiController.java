package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.http.HttpStatus;
import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.bean.api.ApiStruct;
import com.campfire.intelligentsecurity.business.service.ApiService;
import com.campfire.intelligentsecurity.sys.bean.Result;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController extends BaseController {

    @Autowired
    ApiService apiService;

    @PostMapping({"", "/"})
    public Object api(@RequestBody ApiStruct apiStruct) {
        Object rs = apiService.api(apiStruct);
        if (rs == null) {
            Result out = failure(HttpStatus.HTTP_BAD_REQUEST, "param error or null");
            LOG.info("{}", JSONUtil.toJsonStr(out));
            return out;
        }
        if (apiStruct.apiType().getId()==204){
            return  rs;
        }
        Result out = success(rs);
        LOG.info("{}", JSONUtil.toJsonStr(out));
        return out;
    }
}
