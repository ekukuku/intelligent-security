package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.TTestStudent;
import com.campfire.intelligentsecurity.business.service.StudentService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/student")
public class StudentController extends BaseController {

    @Autowired
    StudentService studentService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/student/list.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView input(String id) {
        mv.addObject("student", studentService.load(id));
        mv.setViewName("pages/student/input.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return studentService.studentList();
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TTestStudent tTestStudent) {
        LOG.debug("新增/保存....");
        return success(studentService.saveOrUpdate(tTestStudent));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(studentService.remove(ids));
    }
}
