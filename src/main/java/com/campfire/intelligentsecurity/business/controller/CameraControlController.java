package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.service.CameraControlService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import com.github.drinkjava2.jdialects.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/camera")
public class CameraControlController extends BaseController {
    @Autowired
    CameraControlService cameraControlService;

    @GetMapping("/control")
    public Object control(String cameraid, String eventCode, String stopCode, int rspeed, int cspeed) {
        boolean flag = cameraControlService.Control(cameraid, eventCode, stopCode, rspeed, cspeed);
        if (flag) {
            return success();
        } else {
            return failure();
        }
    }

    @GetMapping("/getWatchList")
    @ResponseBody
    public Object getWatchList() {
        Object result = cameraControlService.watchControl("", "", "","");
        if(result instanceof String && "行为识别接口服务无法访问".equals(result)){
            return failure(-1, String.valueOf(result));
        }
        return success(result);
    }

    @GetMapping("/watchControl")
    @ResponseBody
    public Object watchControl(String add, String remove, String sfId,String planId) {
        Object result = cameraControlService.watchControl(add, remove, sfId,planId);
        if(result instanceof String && "行为识别接口服务无法访问".equals(result)){
            return failure(-1, String.valueOf(result));
        }
        if (result instanceof String && "统一视频平台接口调用失败".equals(result)){
            return failure(-1, String.valueOf(result));
        }
        return success(result);
    }

    @GetMapping("/subscribe")
    @ResponseBody
    public Object subscribe(String cameraCode, String eventType, String isOriginal) {
        if(StrUtils.isEmpty(cameraCode)){
            return failure(-1, "设备编号为空，调用失败");
        }
        if(StrUtils.isEmpty(eventType)){
            eventType="1";
        }
        Object result = cameraControlService.subscribe(cameraCode, eventType, isOriginal);
        if(result instanceof String && "行为识别接口服务无法访问".equals(result)){
            return failure(-1, String.valueOf(result));
        }
        return success(result);
    }
}
