package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/track")
public class TrackController extends BaseController {

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/track/list.html");
        return mv;
    }
}
