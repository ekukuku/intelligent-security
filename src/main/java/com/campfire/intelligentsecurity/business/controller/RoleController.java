package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.TSysRole;
import com.campfire.intelligentsecurity.business.service.RoleService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    RoleService roleService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/role/list.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return roleService.roleList();
    }

    @RequestMapping("/data/add")
    public ModelAndView add() {
        mv.addObject("roleInfo", null);
        mv.setViewName("pages/role/roleInfo.html");
        return mv;
    }

    @RequestMapping("/data/edit")
    public ModelAndView edit(String id) {
        mv.addObject("roleInfo", roleService.loadInfo(id));
        mv.setViewName("pages/role/roleInfo.html");
        return mv;
    }

    @PostMapping("/data/save")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TSysRole sysRole){
        return success(roleService.saveOrUpdate(sysRole));
    }

    @DeleteMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(roleService.remove(ids));
    }

    @PostMapping("/data/changeState")
    @ResponseBody
    public Object saveOrUpdate(String id, String state){
        return success(roleService.chengeState(id, state));
    }

    @PostMapping("/data/changeUsers")
    @ResponseBody
    public Object changeUsers(String roleid, String users){
        System.out.println("roleid: "+roleid+"\tusers: "+users);
        return success(roleService.changeUsers(roleid, users));
    }

    @RequestMapping("/data/user")
    public ModelAndView userList(String roleid){
        mv.addObject("roleid", roleid);
        mv.addObject("curUsers", roleService.curUserList(roleid));
        mv.setViewName("pages/role/user.html");
        return mv;
    }

    @RequestMapping("/data/menu")
    public ModelAndView menuList(String roleid){
        mv.addObject("roleid", roleid);
        mv.addObject("curMenu", roleService.curMenuList(roleid));
        mv.setViewName("pages/role/menu.html");
        return mv;
    }

    @RequestMapping("/data/userlist")
    @ResponseBody
    public Object userlist() {
        return roleService.userList();
    }

    @RequestMapping("/data/menulist")
    @ResponseBody
    public Object menulist() {
        return roleService.menuList();
    }

    @PostMapping("/data/changeMenu")
    @ResponseBody
    public Object changeMenu(String roleid, String menu){
        return success(roleService.changeMenu(roleid, menu));
    }
}
