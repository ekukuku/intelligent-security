package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.TSysUser;
import com.campfire.intelligentsecurity.business.service.UserService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/user/list.html");
        return mv;
    }

    @RequestMapping("/userinfo/add")
    public ModelAndView userinfo() {
        mv.addObject("userInfo", new TSysUser());
        mv.setViewName("pages/user/add.html");
        return mv;
    }

    @RequestMapping("/userinfo/edit")
    public ModelAndView edit(String id) {
        mv.addObject("userInfo", userService.loadInfo(id));
        mv.setViewName("pages/user/add.html");
        return mv;
    }

    @RequestMapping("/data/isExist")
    @ResponseBody
    public Object isExist(String usercode, String userid) {
        return userService.isExist(usercode, userid);
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return userService.userList();
    }

    @PostMapping("/data/save")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TSysUser sysUser){
        return success(userService.saveOrUpdate(sysUser));
    }

    @DeleteMapping("/userinfo/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(userService.remove(ids));
    }

    @PostMapping("/userinfo/changeState")
    @ResponseBody
    public Object saveOrUpdate(String id, String state){
        return success(userService.chengeState(id, state));
    }

}
