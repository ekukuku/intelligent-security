package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.json.JSONUtil;
import com.campfire.intelligentsecurity.business.service.MonitorService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/monitor")
public class MonitorController extends BaseController {


    @Autowired
    MonitorService monitorService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.addObject("tree", JSONUtil.parseArray(monitorService.tree()));
        mv.setViewName("pages/monitor/list.html");
        return mv;
    }

    @RequestMapping("/data/tree")
    @ResponseBody
    public Object tree(){
        return success(monitorService.tree());
    }

    @GetMapping("/data/stream")
    @ResponseBody
    public Object loadStreamUrl(String camera_num){
        return success(monitorService.loadStreamUrl(camera_num));
    }
}
