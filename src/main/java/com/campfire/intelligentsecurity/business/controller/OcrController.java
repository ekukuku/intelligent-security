package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.campfire.intelligentsecurity.business.bean.TicketInfo;
import com.campfire.intelligentsecurity.business.service.BusPersonService;
import com.campfire.intelligentsecurity.business.service.DictService;
import com.campfire.intelligentsecurity.business.service.OcrService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Base64;
import java.util.Map;

@Controller
@RequestMapping("/ocr")
public class OcrController extends BaseController {

    @Autowired
    OcrService ocrService;

    @Autowired
    BusPersonService busPersonService;
    @Autowired
    DictService dictService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/ocr/list.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView input(String name) {
        mv.addObject("jzlbList", dictService.loadCodeBook("dict_jzlb"));
        mv.addObject("dwList", dictService.loadDw());
        Map<String, Object> personIdByName = ocrService.getPersonIdByName(name);
        if (StrUtil.isNotEmpty(name) && !personIdByName.isEmpty()) {
            String id = personIdByName.get("id").toString();
            mv.addObject("zylbList", busPersonService.loadZylb(id));
            mv.addObject("busPerson", busPersonService.load(id));
        } else {
            mv.addObject("zylbList", null);
            mv.addObject("busPerson", null);
        }
        mv.setViewName("pages/ocr/show.html");
        return mv;
    }

    @RequestMapping("/upload")
    public ModelAndView upload(String id) {
        mv.setViewName("pages/ocr/upload.html");
        mv.addObject("id", id);
        return mv;
    }

    @RequestMapping("/ocrupload")
    public ModelAndView ocrupload() {
        mv.setViewName("pages/ocr/ocrupload.html");
        return mv;
    }


    @RequestMapping("/result")
    public ModelAndView result(String id) {
        mv.setViewName("pages/ocr/result.html");
        mv.addObject("result", ocrService.ticket(id));
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return ocrService.ticketList();
    }

    @ResponseBody
    @RequestMapping("/uploadImage")
    public Object uploadImage(MultipartFile file, String id) {
        JSONObject jsonObject = new JSONObject();
        String name = file.getOriginalFilename();//上传文件的真实名称

        //
        try {
            String pic = Base64.getEncoder().encodeToString(file.getBytes());
            synchronized (this) {
                JSONObject ocr = ocrService.ocr(pic);
                String result = ocr.getStr("result", "");
                String url = ocr.getStr("url", "");
                String master = ocr.getStr("master", "");
                String workers = ocr.getStr("workers", "");
                if (StrUtil.isNotEmpty(id)) {
                    String charge = ocrService.getCharge(id).get("OPERATION_CHARGE_PERSON").toString();    //工作负责人
                    if (!"".equals(master) && !master.equals(charge) && !result.contains("工作负责人未签名")) {
                        if ("工作票无异常".equals(result)) {
                            result = "工作票负责人签名不匹配";
                        } else {
                            result += ",工作票负责人签名不匹配";
                        }
                    }
                }
                ocrService.saveOrUpdate(new TicketInfo(result, name, url, master, workers, id));
            }
        } catch (Exception e) {
            LOG.info("OCR服务异常");
        }
        //

        jsonObject.append("code", 0);    //因为layui原生代码中，只有返回该json字符串才会进行成功回调
        return jsonObject;
    }

    @ResponseBody
    @RequestMapping("/getImage")
    public String getImage(String url) {
        try {
            byte[] bytes = FileUtil.readBytes(url);
            String pic = Base64.getEncoder().encodeToString(bytes);
            return "data:image/jpeg;base64," + pic;
        } catch (IORuntimeException e) {
            e.printStackTrace();
        }
        return "";
    }

    @PostMapping("/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(ocrService.remove(ids));
    }
}
