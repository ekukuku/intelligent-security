package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.service.FirstService;
import com.campfire.intelligentsecurity.business.service.MenuService;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/first")
public class FirstController extends BaseController {

    @Autowired
    FirstService firstService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.addObject("planStatistics", firstService.planStatistics());
        mv.addObject("personStatistics", firstService.personStatistics());
        mv.addObject("admittanceInfo", firstService.loadAdmittanceInfo());
        mv.addObject("jqwzInfo", firstService.loadJqwzInfo());
        mv.addObject("teamInfo", firstService.loadTeamInfo());
        mv.addObject("teamJobInfo", firstService.loadTeamJobInfo());
        mv.addObject("blInfo", firstService.loadBlackListInfo());
        mv.addObject("camInfo", firstService.loadCamInfo());
        mv.addObject("wzInfo", firstService.loadWzInfo());
        mv.setViewName("pages/first/list.html");
        return mv;
    }
}
