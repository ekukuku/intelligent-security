package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.ViolationPoint;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import com.campfire.intelligentsecurity.sys.service.BaseService;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.github.drinkjava2.jsqlbox.DB.par;

@RestController
@RequestMapping("/violation")

public class ViolationController extends BaseController {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;
    /**
     * 违章扣分
     */
    @PostMapping("/deductPoints")       //TODO
    public Object deductPoints(
//            @RequestBody ViolationPoint violationPoint
    ) {
        //违章时间  违章人员  违章信息  违章扣分  违章截图
        LOG.debug("违章扣分");
        dbContext.exe("update t_test_student set name = ? where id = ?",par("test2"),par("10"));
        return null;
    }
}
