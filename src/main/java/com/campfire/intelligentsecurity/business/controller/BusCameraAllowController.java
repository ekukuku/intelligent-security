package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusCameraAllow;
import com.campfire.intelligentsecurity.business.service.CameraAllowService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/allow")
public class BusCameraAllowController extends BaseController {
    @Autowired
    CameraAllowService cameraAllowService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/lunxun/listblock.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView saveOrUpdate(String id) {
        mv.addObject("bean", cameraAllowService.load(id));
        mv.setViewName("pages/lunxun/editblock.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return cameraAllowService.List();
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusCameraAllow allow) {
        LOG.debug("新增/保存....");
        return success(cameraAllowService.saveOrUpdate(allow));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(cameraAllowService.remove(ids));
    }

    @PostMapping("/data/block")
    @ResponseBody
    public Object block(String id,String block) {
        return success(cameraAllowService.block(id,block));
    }

}
