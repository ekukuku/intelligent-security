package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.service.MenuService;
import com.campfire.intelligentsecurity.business.service.RoleService;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Autowired
    MenuService menuService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/menu/list.html");
        return mv;
    }

    @RequestMapping("/data/add")
    public ModelAndView add() {
        mv.addObject("level1", menuService.loadLevel1());
        mv.addObject("menuInfo", null);
        mv.setViewName("pages/menu/menuInfo.html");
        return mv;
    }

    @RequestMapping("/data/edit")
    public ModelAndView edit(String id) {
        mv.addObject("level1", menuService.loadLevel1());
        mv.addObject("menuInfo", menuService.loadInfo(id));
        mv.setViewName("pages/menu/menuInfo.html");
        return mv;
    }

    @PostMapping("/data/save")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody TSysMenu sysMenu){
        return success(menuService.saveOrUpdate(sysMenu));
    }

    @DeleteMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(menuService.remove(ids));
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return menuService.menuList();
    }

    @PostMapping("/data/changeState")
    @ResponseBody
    public Object saveOrUpdate(String id, String state){
        return success(menuService.chengeState(id, state));
    }
}
