package com.campfire.intelligentsecurity.business.controller;

import com.campfire.intelligentsecurity.business.bean.BusCameraAllow;
import com.campfire.intelligentsecurity.business.bean.BusCameraPlan;
import com.campfire.intelligentsecurity.business.service.CameraAllowService;
import com.campfire.intelligentsecurity.business.service.CameraPlanService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/camera/plan")
public class BusCameraPlanController extends BaseController {
    @Autowired
    CameraPlanService cameraPlanService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/lunxun/listplan.html");
        return mv;
    }

    @RequestMapping("/view/saveOrUpdate")
    public ModelAndView saveOrUpdate(String id) {
        mv.addObject("bean", cameraPlanService.load(id));
        mv.setViewName("pages/lunxun/editplan.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return cameraPlanService.List();
    }

    @PostMapping("/data/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusCameraPlan bean) {
        LOG.debug("新增/保存....");
        return success(cameraPlanService.saveOrUpdate(bean));
    }

    @PostMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        LOG.debug("删除....");
        return success(cameraPlanService.remove(ids));
    }
}
