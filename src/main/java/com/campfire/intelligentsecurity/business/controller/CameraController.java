package com.campfire.intelligentsecurity.business.controller;

import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.business.bean.BusCamera;
import com.campfire.intelligentsecurity.business.service.CameraService;
import com.campfire.intelligentsecurity.sys.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/camera")
public class CameraController extends BaseController {

    @Autowired
    CameraService cameraService;

    @RequestMapping("/view/list")
    public ModelAndView listView() {
        mv.setViewName("pages/camera/list.html");
        return mv;
    }

    @RequestMapping("/data/add")
    public ModelAndView userinfo() {
        mv.addObject("cameraInfo", null);
        mv.addObject("professionalTypeList", cameraService.loadProfessionalType());

        mv.setViewName("pages/camera/add.html");
        return mv;
    }

    @RequestMapping("/data/edit")
    public ModelAndView edit(String id) {
        mv.addObject("cameraInfo", cameraService.loadInfo(id));
        mv.setViewName("pages/camera/add.html");
        return mv;
    }

    @RequestMapping("/data/list")
    @ResponseBody
    public Object list() {
        return cameraService.cameraList();
    }

    @PostMapping("/data/save")
    @ResponseBody
    public Object saveOrUpdate(@RequestBody BusCamera busCamera){
        return success(cameraService.saveOrUpdate(busCamera));
    }

    @DeleteMapping("/data/remove")
    @ResponseBody
    public Object remove(String ids) {
        return success(cameraService.remove(ids));
    }

    @PostMapping("/data/changeState")
    @ResponseBody
    public Object saveOrUpdate(String id, String state){
        return success(cameraService.chengeState(id, state));
    }

    @RequestMapping("/data/isExist")
    @ResponseBody
    public Object isExist(String camera_num, String id) {
        return cameraService.isExist(camera_num, id);
    }

}
