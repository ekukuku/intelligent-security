package com.campfire.intelligentsecurity.sys.controller;


import cn.hutool.http.HttpStatus;
import com.campfire.intelligentsecurity.sys.bean.Result;
import com.campfire.intelligentsecurity.sys.service.SysService;
import com.campfire.intelligentsecurity.sys.service.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

@Controller
@RequestMapping
public class SysController extends BaseController {

    @Autowired
    SysService sysService;


    @RequestMapping({"/index"})
    public ModelAndView index(HttpSession session) {
        mv.addObject("user", session.getAttribute("user"));
        mv.setViewName("index.html");
        return mv;
    }

    @RequestMapping({"/version"})
    public ModelAndView version(HttpSession session) {
        mv.setViewName("version.html");
        return mv;
    }

    @RequestMapping({"", "/", "/login"})
    public ModelAndView login(HttpSession session) {
        session.removeAttribute("usercode");
        session.removeAttribute("user");
        mv.setViewName("login.html");
        return mv;
    }

    @RequestMapping("/pwd")
    public ModelAndView pwd(HttpSession session) {
        mv.addObject("username", session.getAttribute("usercode"));
        mv.setViewName("updpwd.html");
        return mv;
    }


    @ResponseBody
    @RequestMapping("/loginSecurity")
    public Result loginSecurity(String username, String password, HttpSession session, HttpServletResponse response) {
        Map<String, Object> map = sysService.login(username, password);
        Result result = new Result();
        String Ip = "";
        try {
            Ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        if (map != null && map.size() > 0) {
            session.setAttribute("user", map);
            session.setAttribute("usercode", username);
            result.setCode(HttpStatus.HTTP_OK);
            //保存登录日志
            sysService.SaveLoginLog(map.get("id").toString(), map.get("usercode").toString(), Ip, "0", null);
        } else {
            result.setCode(HttpStatus.HTTP_BAD_REQUEST);
            sysService.SaveLoginLog(null, username, Ip, "1", "账号/密码有误");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/updpwd")
    public Result updpwd(@RequestBody Map<String, String> map, HttpSession session, HttpServletResponse response) {
        Result result = new Result();
        long code = 0;
        Map<String, Object> map1 = sysService.login(map.get("username"), map.get("oldpwd"));
        if (map1 != null && map1.size() > 0) {
            code = sysService.upd(map.get("username"), map.get("password"));
            if (code == 1) {
                result.setCode(HttpStatus.HTTP_OK);
                result.setMessage("修改成功,请重新登录");
                session.removeAttribute("usercode");
                session.removeAttribute("user");
            } else {
                result.setCode(HttpStatus.HTTP_INTERNAL_ERROR);
                result.setMessage("修改失败");
            }
        } else {
            result.setCode(HttpStatus.HTTP_BAD_REQUEST);
            result.setMessage("旧密码校验失败");
        }
        return result;
    }


    @RequestMapping("/epgis/view/list")
    public ModelAndView epgis() {
        mv.setViewName("pages/epgis/index.html");
        return mv;
    }

    @RequestMapping("/sys/data/menulist")
    @ResponseBody
    public Object menulist(HttpSession session) {
        Map<String, Object> user = (Map<String, Object>) session.getAttribute("user");
        String usercode = (String) user.get("usercode");
        Integer userid = (Integer) user.get("id");
        if ("superadmin".equals(usercode)) {
            return sysService.allMenuList();
        }
        return sysService.menuList(userid);
    }

    @PostMapping("/dataInterface")
    @ResponseBody
    public Object dataInterface(String message, String title, int type, int position){
        WebSocketServer.sendMessage(message, title, type, position);
        return "success";
    }
}
