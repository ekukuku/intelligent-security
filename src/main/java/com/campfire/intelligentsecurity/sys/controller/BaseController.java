package com.campfire.intelligentsecurity.sys.controller;

import cn.hutool.http.HttpStatus;
import com.campfire.intelligentsecurity.sys.bean.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    @Autowired
    public HttpServletRequest request;
    public ModelAndView mv = new ModelAndView();
    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    public Result success() {
        return success("操作成功");
    }

    public Result success(String message) {
        return success(HttpStatus.HTTP_OK,message, null);
    }

    public Result success(Object data) {
        return success(HttpStatus.HTTP_OK,"操作成功", data);
    }

    public Result success(int code,String message, Object data) {
        Result result = new Result();
        result.setCode(0);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public Result failure() {
        return failure(HttpStatus.HTTP_INTERNAL_ERROR, "操作失败");
    }

    public Result failure(Integer code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }
}
