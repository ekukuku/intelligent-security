package com.campfire.intelligentsecurity.sys.bean;


import com.github.drinkjava2.jdialects.annotation.jpa.Column;
import com.github.drinkjava2.jdialects.annotation.jpa.Id;
import com.github.drinkjava2.jdialects.annotation.jpa.Table;
import com.github.drinkjava2.jdialects.annotation.jpa.Transient;
import com.github.drinkjava2.jsqlbox.ActiveRecord;

import java.sql.Timestamp;
import java.util.List;

@Table(name = "t_sys_menu")
public class TSysMenu extends ActiveRecord<TSysMenu> {
    public static final String ID = "id";

    public static final String TITLE = "title";

    public static final String ICON = "icon";

    public static final String TYPE = "type";

    public static final String OPENTYPE = "openType";

    public static final String HREF = "href";

    public static final String PARENT = "parent";

    public static final String STATE = "state";

    public static final String SORT = "sort";

    public static final String CREATETIME = "createtime";

    @Id
    private Integer id;

    @Column(name = "title", length = 150)
    private String title;
    @Column(name = "icon", length = 200)
    private String icon;
    private Integer type;
    private Integer openType;
    private String href;
    private Integer parent;
    private Integer isLeaf;
    private Integer sort;
    private Integer state;
    @Column(insertable = false)
    private Timestamp createtime;
    @Transient
    private List<TSysMenu> children;

    public Integer getId() {
        return id;
    }

    public TSysMenu setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public TSysMenu setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public TSysMenu setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public TSysMenu setType(Integer type) {
        this.type = type;
        return this;
    }

    public Integer getOpenType() {
        return openType;
    }

    public TSysMenu setOpenType(Integer openType) {
        this.openType = openType;
        return this;
    }

    public String getHref() {
        return href;
    }

    public TSysMenu setHref(String href) {
        this.href = href;
        return this;
    }

    public Integer getParent() {
        return parent;
    }

    public TSysMenu setParent(Integer parent) {
        this.parent = parent;
        return this;
    }

    public Integer getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public TSysMenu setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
        return this;
    }

    public List<TSysMenu> getChildren() {
        return children;
    }

    public void setChildren(List<TSysMenu> children) {
        this.children = children;
    }
}
