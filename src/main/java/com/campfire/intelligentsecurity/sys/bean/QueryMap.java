package com.campfire.intelligentsecurity.sys.bean;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

import java.util.Map;
import java.util.Set;

public class QueryMap {
    private Map<String, QueryParam> map;

    public QueryMap(Map<String, QueryParam> map) {
        this.map = map;
    }

    public Object get(String key) {
        QueryParam queryParam = map.get(key);
        if (queryParam != null) {
            String[] values = queryParam.getValues();
            if (ObjectUtil.isNotNull(values)) {
                if (StrUtil.isNotEmpty(values[0])) {
                    return values[0];
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    public Set<String> getKeys() {
        return map.keySet();
    }

    public String getStr(String key) {
        Object obj = get(key);
        if (obj != null) {
            return StrUtil.toString(obj);
        }
        return null;
    }

    public void remove(String key) {
        map.remove(key);
    }

    public Integer getInt(String key) {
        String obj = getStr(key);
        if (obj != null) {
            return Integer.parseInt(obj);
        }
        return null;
    }
}
