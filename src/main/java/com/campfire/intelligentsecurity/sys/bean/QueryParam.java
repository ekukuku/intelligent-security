package com.campfire.intelligentsecurity.sys.bean;

import cn.hutool.core.util.ObjectUtil;

public class QueryParam {
    private String key;
    private String[] values;

    public QueryParam(String key, String[] values) {
        this.key = key;
        this.values = values;
    }

    public QueryParam() {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public String getStrSingle() {
        if (ObjectUtil.isNotNull(values)) {
            return values[0];
        }
        return null;
    }

    public Integer getIntSingle() {
        if (ObjectUtil.isNotNull(values)) {
            return Integer.parseInt(values[0]);
        }
        return null;
    }
}
