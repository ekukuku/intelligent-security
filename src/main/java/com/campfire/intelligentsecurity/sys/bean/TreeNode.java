package com.campfire.intelligentsecurity.sys.bean;

import java.util.List;

public class TreeNode {

    private String title;
    private String id;
    private Object children;
    private String spread;
    private String disabled;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getChildren() {
        return children;
    }

    public void setChildren(Object children) {
        this.children = children;
    }

    public String getSpread() { return spread; }

    public void setSpread(String spread) { this.spread = spread; }

    public String getDisabled() { return disabled; }

    public void setDisabled(String disabled) { this.disabled = disabled; }
}
