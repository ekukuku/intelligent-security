package com.campfire.intelligentsecurity.sys.kit;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.campfire.intelligentsecurity.sys.bean.ListPager;
import com.campfire.intelligentsecurity.sys.bean.QueryMap;
import com.github.drinkjava2.jdbpro.SqlItem;
import com.github.drinkjava2.jsqlbox.DbContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.github.drinkjava2.jsqlbox.DB.*;

public class PagerKit {
    private String sql;
    private DbContext dbContext;
    private int pageSize;
    private int currentPage;
    private boolean auto;
    private List<Object> SqlParams = new ArrayList<>();
    private List<Object> SqlItemParams = new ArrayList<>();

    public PagerKit(String sql, DbContext dbContext, QueryMap userParams, Object... params) {
        this.sql = sql;
        this.dbContext = dbContext;
        this.pageSize = userParams.getInt("limit");
        this.currentPage = userParams.getInt("page");
        initParams(userParams, params);
    }

    public PagerKit(String sql, boolean auto, QueryMap userParams, Object... params) {
        this.sql = sql;
        this.auto = auto;
        this.pageSize = userParams.getInt("limit");
        this.currentPage = userParams.getInt("page");
        initParams(userParams, params);
    }

    public ListPager builder() {
        if (dbContext == null) {
            dbContext = DbContext.getGlobalDbContext();
        }
        ListPager pager = new ListPager();
        pager.setCount(getCount());
        pager.setData(list());
        return pager;
    }

    private long getCount() {
        return this.dbContext.qryLongValue("select count(1) from (", sql, SqlItemParams.toArray(), CollectionUtil.join(SqlParams, " "), ") a");
    }

    private List<Map<String, Object>> list() {
        return this.dbContext.qryMapList(pagin(currentPage, pageSize), sql, SqlItemParams.toArray(), CollectionUtil.join(SqlParams, " "));
    }

    private void initParams(QueryMap userParams, Object... params) {
        for (String key : userParams.getKeys()
        ) {
            if (key.equals("limit") || key.equals("page") || key.startsWith("cus_")) {
                continue;
            }
            if (key.endsWith("@like")) {
                this.SqlItemParams.add(noNull(" and " + key.substring(0, key.length() - 5) + " like ?", "%", userParams.get(key), "%"));
            } else if(key.endsWith("_date_le")) {
                this.SqlItemParams.add(noNull(" and " + key.substring(0, key.length() - 8) + " <= ?", userParams.get(key)));
            } else if(key.endsWith("_date_ge")) {
                this.SqlItemParams.add(noNull(" and " + key.substring(0, key.length() - 8) + " >= ?", userParams.get(key)));
            } else {
                this.SqlItemParams.add(notNull(StrUtil.format(" and {}=?", key), userParams.get(key)));
            }
        }
        if (!auto) {
            this.SqlItemParams.clear();
        }
        for (Object object : params
        ) {
            if (object instanceof SqlItem) {
                this.SqlItemParams.add(object);
            } else {
                this.SqlParams.add(object);
            }
        }
    }
}
