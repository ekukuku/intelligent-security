package com.campfire.intelligentsecurity.sys.kit;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;

public class AESUtil {
    public static final String SECRET="ANGK1369abcdAQGK";
    public static final String IV="2244668811337799";

    private static AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, SECRET.getBytes(), IV.getBytes());

    public static String encode(String str) {
        return aes.encryptHex(str);
    }

    public static String decode(String str) {
        return aes.decryptStr(str);
    }
}
