package com.campfire.intelligentsecurity.sys.kit;

import com.github.drinkjava2.jdialects.springsrc.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ComUtil {
    //分组方法、集合分成N个对象
    public static List splitToPieces(Collection data, int eachPieceSize) {
        if (CollectionUtils.isEmpty(data)) {
            return new ArrayList<>(0);
        }

        if (eachPieceSize <= 0) {
            throw new IllegalArgumentException("参数错误");
        }

        List result = new ArrayList<>();
        for (int index = 0; index< data.size();index=index+eachPieceSize){
            result.add(data.stream().skip(index).limit(eachPieceSize).collect(Collectors.toList()));
        }
        return result;
    }
}
