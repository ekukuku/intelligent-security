package com.campfire.intelligentsecurity.sys.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalException {
    private final Logger LOG = LoggerFactory.getLogger(getClass());


    @ExceptionHandler(value = Exception.class)
    Object handleException(Exception e, HttpServletRequest request) {
        LOG.error("url {}, msg {}", request.getRequestURL(), e.getMessage());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error/500.html");
        return modelAndView;
    }
}
