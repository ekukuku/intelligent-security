package com.campfire.intelligentsecurity.sys.service;


import com.campfire.intelligentsecurity.business.bean.TSysUser;
import com.campfire.intelligentsecurity.sys.bean.TSysMenu;
import com.campfire.intelligentsecurity.sys.kit.AESUtil;
import com.github.drinkjava2.jsqlbox.DbContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.drinkjava2.jsqlbox.DB.notBlank;
import static com.github.drinkjava2.jsqlbox.DB.par;
import static com.github.drinkjava2.jsqlbox.DB.param;

@Service
public class SysService {
    @Autowired
    @Qualifier("main")
    DbContext dbContext;

    public Object menuList(Integer userid) {
        List<TSysMenu> menuList = dbContext.qryEntityList(TSysMenu.class, "select " +
                " distinct d.id, d.title, d.icon, d.type, d.openType, d.href, d.parent, d.isLeaf, d.sort " +
                "from " +
                " t_sys_user a " +
                " left join t_role_user b on a.id = b.userid " +
                " left join t_role_menu c on b.roleid = c.roleid " +
                " inner join t_sys_menu d on c.menuid = d.id " +
                "where " +
                " d.state = 1 " +
                " and a.id = ? " +
                "order by sort,id", par(userid));
        List<TSysMenu> parentList = menuList.stream().filter(tSysMenu -> tSysMenu.getParent() == -1).collect(Collectors.toList());
        for (TSysMenu menu : parentList
        ) {
            menu.setChildren(menuList.stream().filter(tSysMenu -> tSysMenu.getParent() == menu.getId()).collect(Collectors.toList()));
        }
        return parentList;
    }

    public Object allMenuList() {
        List<TSysMenu> menuList = dbContext.qryEntityList(TSysMenu.class, "select id, title, icon, type, openType, href, parent, isLeaf, sort " +
                "from " +
                " t_sys_menu " +
                "where " +
                " state = 1 " +
                "order by sort,id");
        List<TSysMenu> parentList = menuList.stream().filter(tSysMenu -> tSysMenu.getParent() == -1).collect(Collectors.toList());
        for (TSysMenu menu : parentList
        ) {
            menu.setChildren(menuList.stream().filter(tSysMenu -> tSysMenu.getParent() == menu.getId()).collect(Collectors.toList()));
        }
        return parentList;
    }

    public Map<String, Object> login(String username, String password) {
        password = AESUtil.encode(password);
        Map<String, Object> map = dbContext.qryMap("select * from t_sys_user where 1=1 and enable=1 ",
                notBlank(" and usercode=?", username),
                notBlank(" and password=?", password));
        return map;
    }

    public long upd(String username, String password) {
        long count = 0;
        try {
            password = AESUtil.encode(password);
            count = dbContext.execute("update t_sys_user  set password=? where usercode=?", password, username);
        } finally {
            return count;
        }
    }


    public long SaveLoginLog(String userid, String usercode, String loginIP, String result, String errMsg) {
        long count = 0;
        try {
            count = dbContext.execute("INSERT INTO t_sys_login_log(userid, usercode,loginIP, result, errMsg) VALUES (?,?,?,?,?)", userid, usercode, loginIP, result, errMsg);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
