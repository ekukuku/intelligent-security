package com.campfire.intelligentsecurity.business.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.github.drinkjava2.jdialects.springsrc.utils.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TEventVideoServiceTest {

    @Autowired
    Environment environment;
    @Autowired
    TEventVideoService service;
    @Test
    public void test(){
        List<String> list = service.cameraOnlineListNoPlan();
        for (int i=0;i<91;i++){
            list.add("1234"+i);
        }
        if (list != null && list.size() > 0) {
            List group = splitToPieces(list, 100);
            List codeList=null;
            String cameraCode=null;
            try {
                JSONObject json=null;
                JSONArray array=null;
                for (int i=0;i<group.size();i++){
                    codeList= (List) group.get(i);
                    array=new JSONArray();
                    for (int k=0;k<codeList.size();k++){
                        cameraCode= (String) codeList.get(k);
                        if (1==1){
                            //判断是否在线
                        }
                        json = new JSONObject();
                        json.set("CAMERA_CODE", cameraCode);
                        //请求接口获取10张图片，然后发送解析。违章管理rycr
                        if ("1001".equals(cameraCode)) {
                            json.set("STREAM", "http://192.168.1.60:8765/20220422/1001_wdjz_1650619753908.jpg");
                        } else if ("1002".equals(cameraCode)) {
                            json.set("STREAM", "http://192.168.1.60:8765/20220422/1001_wdjz_1650619753908.jpg");
                        } else {
                            json.set("STREAM", "http://192.168.1.60:8765/20220422/1001_wdjz_1650619753908.jpg");
                            //通过接口获取抓拍图像
                        }
                        array.add(json);
                    }
                    String body = StrUtil.format("{\"msgType\":205,\"data\":{}}", array);
                    System.out.println("人员闯入轮巡开始推送："+body);
                    String rs = null;
                    rs = HttpUtil.post(environment.getProperty("local.api"), body, 3000);
                    //System.out.println(rs);
                    System.out.println("==该组推送完成==");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {

                    }

                }

            } catch (Exception e) {
                System.out.println("人员闯入轮巡推送异常");
            }
        }
    }

    public static  List splitToPieces(Collection data, int eachPieceSize) {
        if (CollectionUtils.isEmpty(data)) {
            return new ArrayList<>(0);
        }

        if (eachPieceSize <= 0) {
            throw new IllegalArgumentException("参数错误");
        }

        List result = new ArrayList<>();
        for (int index = 0; index< data.size();index=index+eachPieceSize){
            result.add(data.stream().skip(index).limit(eachPieceSize).collect(Collectors.toList()));
        }
        return result;
    }
}